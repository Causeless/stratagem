using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditToGameStateClearer : MonoBehaviour
{
    private void ClearGameState()
    {
        // Clear subunit/entity containers
        // Massive hack, but agh, I'm exhausted
        foreach (Transform t in GameObject.FindWithTag("Entity Collection").transform)
        {
            Destroy(t.gameObject);
        }

        foreach (Transform t in GameObject.FindWithTag("SubUnit Collection").transform)
        {
            Destroy(t.gameObject);
        }
    }

    private void Awake()
    {
        ClearGameState();
    }

    private void OnApplicationQuit()
    {
        ClearGameState();
    }
}
