using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OfficerNames", menuName = "Stratagem/Officer Names", order = 1)]
public class OfficerNames : ScriptableObject
{
    public List<string> officerNameList = new List<string>
    {

    };
}