using UnityEngine;

[CreateAssetMenu(fileName = "NewUnitIconsConfig", menuName = "Stratagem/Unit Icons", order = 2)]
public class UnitIconsConfig : ScriptableObject
{
    public Sprite romanIcon;
    public Sprite infantryIcon;
    public Sprite infantrySkirmisherIcon;
    public Sprite cavalryIcon;
    public Sprite cavalrySkirmisherIcon;
}
