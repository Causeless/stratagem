using UnityEngine;

[CreateAssetMenu(fileName = "NewUnitStatsConfig", menuName = "Stratagem/Unit Stats")]
public class UnitStatsConfig : ScriptableObject
{
    [Header("Unit Info")]
    public string unitName;
    public enum UnitType
    {
        Cavalry,
        Infantry,
        InfantrySkirmisher,
        CavalrySkirmisher,
        InfantryCommander,
        CavalryCommander,
    }

    public UnitType Type;
    public GameObject subUnitPrefab;
    [Space]
    [Header("Health and Entities")]
    public int BaseHealthPoints;
    public int BaseHealthPointsPerSubUnit;
    public int MinimumHealthPointsPerSubUnit;
    public int HealthPointsPerEntity;
    [Header("Unit Psychology Stats")]
    [Range(0f,100f)]
    public float BaseMorale;
    [Range(0f,100f)]
    public float BaseDiscipline;
    public float BaseFatigue = 100f;
    [Range(0f,100f)]
    public float BaseConsitution;
    [Header("Unit Melee Stats")]
    public float BaseAttackSpeed;
    public float MaxAttackSpeed;
    [Range(0f,100f)]
    public float BaseHitChance;
    [Range(0f,100f)]
    public float BaseDefenceChance;
    [Range(0f,100f)]
    public float BaseCriticalHitChance;
    [Header("Unit Missile Stats")]
    [Range(0f,100f)]
    public float BaseMissileHitChance;
    [Range(0f,100f)]
    public float BaseMissileDefenceChance;
    [Range(0f,100f)]
    public float BaseMissileCriticalHitChance;
    public int BaseUnitAmmo = 2;
    public float BaseReloadTime = 15f;
    public float BaseMissileAttackInterval;
    public float ShootRange;
    [Header("Unit Formation Stats")]
    public float FormationMeleeDefence;
    public float FormationMissileDefence;
    [Header("Unit Movement Stats")]
    public float BaseWalkSpeed;
    public float BaseRunSpeed;
    public float BaseChargeSpeed;
    public float BaseSprintTime;
    [Header("Unit Charge Behaviour")]
    public float ChargeRange;
    public float CommitmentRange;
}
