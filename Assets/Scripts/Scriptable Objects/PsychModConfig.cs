using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

[CreateAssetMenu(fileName = "NewStatModConfig", menuName = "Stratagem/Stat Modifier Over Time")]
public class StatModConfig : ScriptableObject
{
    public string StatModifierName;
    public bool isAffectedByMoraleMultiplier;
    public UnitEventManager.ModifiableStats ModifiedStat;
    public List<UnitEventManager.UnitEventTriggers> eventTriggerList = new List<UnitEventManager.UnitEventTriggers>();
    public float startValue = 0f;
    public float endValue = 100f;
    public float duration = 5f;
    public float cooldown = 0f;
    public AnimationCurve curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
}
