using UnityEngine;

[CreateAssetMenu(fileName = "NewAggressionLevelConfig", menuName = "Stratagem/AggressionLevelConfig")]
public class AggressionLevelConfig : ScriptableObject
{
    public int ID;
    public Unit.UnitAggressionLevel aggressionLevel;
    public float maxMorale;
    public float minMorale;
}
