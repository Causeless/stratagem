using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FactionSpawnList", menuName = "Stratagem/Faction Spawn List", order = 4)]
public class FactionSpawnList : ScriptableObject
{
    public Faction faction;
    public GameObject infantryPrefab;
    public GameObject cavalryPrefab;
    public GameObject skirmisherPrefab;
}