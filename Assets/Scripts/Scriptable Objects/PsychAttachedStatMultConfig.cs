using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStatMultConfig", menuName = "Stratagem/Stat Multiplier Against Tracked Stat")]
public class StatMultConfig : ScriptableObject
{
    public string StatMultiplierName;
    public bool isAffectedByMoraleMultiplier;
    public UnitEventManager.ModifiableStats ModifiedStat;
    public UnitEventManager.ModifiableStats TrackedStat;
    public List<UnitEventManager.UnitEventTriggers> eventTriggerList = new List<UnitEventManager.UnitEventTriggers>();
    public float startValue = 0f;
    public float endValue = 100f;
    public AnimationCurve curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
}
