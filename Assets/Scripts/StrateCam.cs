﻿using System.Collections;
using UnityEngine;
using UnityEngine.Extensions;
using UnityEngine.EventSystems;
using MyBox;

public class StrateCam : MonoBehaviour
{
    // Public fields

    //added for cursor change when rotating
    
    public Texture2D CursorRotateLeft;
    public Texture2D CursorRotateRight;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public Vector2 hotSpotRight = new Vector2(33, 0);

    //end added

    public int initialCameraRotationX = 35;
    public int initialCameraRotationY = -35;

    public Terrain terrain;

    public float panSpeed = 15.0f;
    public float zoomSpeed = 100.0f;
    public float rotationSpeed = 50.0f;

    public float mousePanMultiplier = 0.1f;
    public float mouseRotationMultiplier = 0.2f;
    public float mouseZoomMultiplier = 5.0f;

    public float minZoomDistance = 20.0f;
    public float maxZoomDistance = 200.0f;
    public float smoothingFactor = 0.1f;
    public float goToSpeed = 0.1f;

    public bool useKeyboardInput = true;
    public bool useMouseInput = true;
    public bool adaptToTerrainHeight = true;
    public bool increaseSpeedWhenZoomedOut = true;
    public bool correctZoomingOutRatio = true;
    public bool smoothing = true;
    public bool allowDoubleClickMovement = false;

    public bool allowScreenEdgeMovement = true;
    public int screenEdgeSize = 10;
    public float screenEdgeSpeed = 1.0f;
    public float edgeMargin = 0f;

    public GameObject objectToFollow;
    public Vector3 cameraTarget;

    public float cameraHeightOffset = 1f;

    public LayerMask layerMask;

    public bool autoFocus;

    [ConditionalField(nameof(autoFocus))]
    public float autoFocusDistanceOffset = 0f;
    [ConditionalField(nameof(autoFocus))]
    public float autoFocusFocalLengthBase = 40.0f;
    [ConditionalField(nameof(autoFocus))]
    public float autoFocusFocalLengthMultiplier = 4.0f;

    // private fields
    private float currentCameraDistance;
    private Vector3 lastMousePos;
    private Vector3 lastPanSpeed = Vector3.zero;
    private Vector3 goingToCameraTarget = Vector3.zero;
    private bool doingAutoMovement = false;
    private DoubleClickDetector doubleClickDetector;

    // Use this for initialization
    public void Start()
    {
        currentCameraDistance = minZoomDistance + ((maxZoomDistance - minZoomDistance) / 2.0f);
        lastMousePos = Vector3.zero;
        doubleClickDetector = GetComponent<DoubleClickDetector>();
        Camera.main.transform.rotation = Quaternion.Euler(initialCameraRotationX, initialCameraRotationY, 1);

        if (!Application.isEditor)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
    }

    // Update is called once per frame
    public void Update()
    {
        if (allowDoubleClickMovement)
        {
            UpdateDoubleClick();
        }

        UpdatePanning();
        UpdateRotation();
        UpdateZooming();
        UpdatePosition();
        UpdateAutoMovement();
        UpdateAutoFocus();

        lastMousePos = Input.mousePosition;
    }

    public void GoTo(Vector3 position)
    {
        position.y = cameraTarget.y;
        doingAutoMovement = true;
        goingToCameraTarget = position;
        objectToFollow = null;
    }

    public void Follow(GameObject gameObjectToFollow)
    {
        objectToFollow = gameObjectToFollow;
    }

    #region private functions
    private void UpdateDoubleClick()
    {
        if (doubleClickDetector.IsDoubleClickLeft() && terrain && terrain.GetComponent<Collider>())
        {
            var cameraTargetY = cameraTarget.y;

            var collider = terrain.GetComponent<Collider>();
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            Vector3 pos;

            if (collider.Raycast(ray, out hit, Mathf.Infinity) && !EventSystem.current.IsPointerOverGameObject())
            {
                pos = hit.point;
                pos.y = cameraTargetY;
                GoTo(pos);
            }
        }
    }

    private void UpdatePanning()
    {

        Vector3 moveVector = new Vector3(0, 0, 0);

        //added
        float shiftMulti = 1.0f;
        
        float deltaAngleH = 0.0f;

        float deltaAngleV = 0.0f;



        if (Input.GetKey(KeyCode.LeftShift))
        {
            shiftMulti = 2.5f;
        }
        //added

        if (useKeyboardInput)
        {
            //! rewrite to adress xyz seperatly
            if (Input.GetKey(KeyCode.A))
            {
                moveVector.x -= 1 * shiftMulti;
            }
            if (Input.GetKey(KeyCode.S))
            {
                moveVector.z -= 1 * shiftMulti;
            }
            if (Input.GetKey(KeyCode.D))
            {
                moveVector.x += 1 * shiftMulti;
            }
            if (Input.GetKey(KeyCode.W))
            {
                moveVector.z += 1 * shiftMulti;
            }
        }
        if (allowScreenEdgeMovement)
        {
            if (Input.mousePosition.x < screenEdgeSize && Input.mousePosition.y < .75 * (Screen.height))
            {
                moveVector.x -= screenEdgeSpeed * shiftMulti;
                Cursor.SetCursor(null, Vector2.zero, cursorMode);
            }
            else if (Input.mousePosition.x > Screen.width - screenEdgeSize && Input.mousePosition.y < .75 * (Screen.height))
            {
                moveVector.x += screenEdgeSpeed * shiftMulti;
                Cursor.SetCursor(null, Vector2.zero, cursorMode);
            }
            else if (Input.mousePosition.x < screenEdgeSize && Input.mousePosition.y >= .75 * (Screen.height))
            {
                deltaAngleH = -2.0f * shiftMulti * .7f;//THE shiftmulti is a bit fast for rotation so lowered it to 70% 
                Cursor.SetCursor(CursorRotateLeft, hotSpot, cursorMode);//changing the cursor
            }
            else if (Input.mousePosition.x > Screen.width - screenEdgeSize && Input.mousePosition.y >= .75 * (Screen.height))
            {
                deltaAngleH = 2.0f * shiftMulti * .7f;//and for rotating other way
                Cursor.SetCursor(CursorRotateRight, hotSpotRight, cursorMode);
            }
            else
            {
                Cursor.SetCursor(null, Vector2.zero, cursorMode);
            }
            //FINISH rotating with mouse in corner section

            //added for deltaangle changed when moving mouse to corner
            transform.SetLocalEulerAngles(
                Mathf.Min(80.0f, Mathf.Max(5.0f, transform.localEulerAngles.x + deltaAngleV * Time.unscaledDeltaTime * rotationSpeed)),
                transform.localEulerAngles.y + deltaAngleH * Time.unscaledDeltaTime * rotationSpeed
                );

            //added
            
            if (Input.mousePosition.y < screenEdgeSize)
            {
                moveVector.z -= screenEdgeSpeed * shiftMulti;//more shiftmulti
            }
            else if (Input.mousePosition.y > Screen.height - screenEdgeSize)
            {
                moveVector.z += screenEdgeSpeed * shiftMulti;
            }
        }

        if (useMouseInput)
        {
            if (Input.GetMouseButton(2) /*&& Input.GetKey(KeyCode.LeftShift)*/)
            {
                Vector3 deltaMousePos = (Input.mousePosition - lastMousePos);
                moveVector += new Vector3(-deltaMousePos.x, 0, -deltaMousePos.y) * mousePanMultiplier;
            }
        }

        if (moveVector != Vector3.zero)
        {
            objectToFollow = null;
            doingAutoMovement = false;
        }

        var effectivePanSpeed = moveVector;
        if (smoothing)
        {
            float smoothingFactorDelta = (smoothingFactor * Time.unscaledDeltaTime);
            effectivePanSpeed = Vector3.Lerp(lastPanSpeed, moveVector, smoothingFactorDelta);
            lastPanSpeed = effectivePanSpeed;
        }

        var oldXRotation = transform.localEulerAngles.x;
        
        // Set the local X rotation to 0;
        transform.SetLocalEulerAngles(0.0f);

        float panMultiplier = increaseSpeedWhenZoomedOut ? (Mathf.Sqrt(currentCameraDistance)) : 1.0f;

        // This can lead to a very slight y movement that adds up when moving the camera long distance
        // So ensure that we zero out that movement
        Vector3 transformDirection = transform.TransformDirection(effectivePanSpeed);
        transformDirection.y = 0f;

        cameraTarget += transformDirection * panSpeed * panMultiplier * Time.unscaledDeltaTime;

        RaycastHit terrainHit;
        Vector3 rayOrigin = new Vector3(cameraTarget.x, 1000f, cameraTarget.z);
        Ray terrainRay = new Ray(rayOrigin, Vector3.down);
        if (Physics.Raycast(terrainRay, out terrainHit, Mathf.Infinity, layerMask))
        {
            cameraTarget.y = terrainHit.point.y + cameraHeightOffset;
        }

        // Set the old x rotation.
        transform.SetLocalEulerAngles(oldXRotation);
    }

    private void UpdateRotation()
    {
        float deltaAngleH = 0.0f;
        float deltaAngleV = 0.0f;

        float shiftMulti = 1.0f;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            shiftMulti = 2.5f;
        }

        if (useKeyboardInput)
        {
            // Should really be set in data...
            float rotationSpeed = 60f;
            if (Input.GetKey(KeyCode.Q))
            {
                deltaAngleH = -1.0f*rotationSpeed*shiftMulti*Time.unscaledDeltaTime;
            }
            if (Input.GetKey(KeyCode.E))
            {
                deltaAngleH = 1.0f*rotationSpeed*shiftMulti*Time.unscaledDeltaTime;
            }
        }

        if (useMouseInput)
        {
            if (Input.GetMouseButton(1) && EntitySelector._selectedUnits.Count == 0 /*&& !Input.GetKey(KeyCode.LeftShift)*/)
            {//don't need above commented out NOTshift key cuz not using click and drag to pan
                var deltaMousePos = (Input.mousePosition - lastMousePos);
                deltaAngleH += deltaMousePos.x * mouseRotationMultiplier;
                deltaAngleV -= deltaMousePos.y * mouseRotationMultiplier;
            }
        }

        // Turns out this shouldn't be scaling by deltatime at all! This should only be dependent on how far the mouse has moved, deltatime is irrelevant.
        // THe original stratecam is incorrect here. They are scaling by deltatime, however this (counter-intuitively) actually makes the rotation code framerate-dependent instead of framerate-independent
        // FUTURE NOTE: deltaAngleH (from keyboard input) *does* however need to be scaled by dt (see aboev)
        transform.SetLocalEulerAngles(
            Mathf.Min(80.0f, Mathf.Max(5.0f, transform.localEulerAngles.x + deltaAngleV * 0.02f * rotationSpeed)),
            transform.localEulerAngles.y + deltaAngleH * 0.02f * rotationSpeed
        );
    }

    private void UpdateZooming()
    {
        float deltaZoom = 0.0f;
        if (useKeyboardInput)
        {
            if (Input.GetKey(KeyCode.F))
            {
                deltaZoom = 1.0f;
            }
            if (Input.GetKey(KeyCode.R))
            {
                deltaZoom = -1.0f;
            }
        }
        if (useMouseInput)
        {
            var scroll = Input.GetAxis("Mouse ScrollWheel");
            deltaZoom -= scroll * mouseZoomMultiplier;

            //ADDED FOR ORTHOGRAPHIC
            Camera.main.orthographicSize -= scroll;
        }
        var zoomedOutRatio = correctZoomingOutRatio ? (currentCameraDistance - minZoomDistance) / (maxZoomDistance - minZoomDistance) : 0.0f;
        currentCameraDistance = Mathf.Max(minZoomDistance, Mathf.Min(maxZoomDistance, currentCameraDistance + deltaZoom * Time.unscaledDeltaTime * zoomSpeed * (zoomedOutRatio * 2.0f + 1.0f)));
    }

    private void UpdatePosition()
    {
        if (objectToFollow != null)
        {
            cameraTarget = Vector3.Lerp(cameraTarget, objectToFollow.transform.position, goToSpeed);
        }

        float terrainX = terrain.transform.position.x;
        float terrainZ = terrain.transform.position.z;
        float sizeX = terrain.terrainData.size.x;
        float sizeZ = terrain.terrainData.size.z;

        cameraTarget.x = Mathf.Clamp(cameraTarget.x, terrainX - edgeMargin, terrainX + sizeX + edgeMargin);
        cameraTarget.z = Mathf.Clamp(cameraTarget.z, terrainZ - edgeMargin, terrainZ + sizeZ + edgeMargin);

        transform.position = cameraTarget;
        transform.Translate(Vector3.back * currentCameraDistance);

        if (adaptToTerrainHeight && terrain != null)
        {
            transform.SetPosition(
                null,
                Mathf.Max(terrain.SampleHeight(transform.position) + terrain.transform.position.y + minZoomDistance, transform.position.y)
            );
        }
    }

    private void UpdateAutoMovement()
    {
        if (doingAutoMovement)
        {
            cameraTarget = Vector3.Lerp(cameraTarget, goingToCameraTarget, goToSpeed);
            if (Vector3.Distance(goingToCameraTarget, cameraTarget) < 1.0f)
            {
                doingAutoMovement = false;
            }
        }
    }

    private void UpdateAutoFocus()
    {
        if (!autoFocus)
        {
            return;
        }

        UnityEngine.Rendering.Volume volume = GetComponentInChildren<UnityEngine.Rendering.Volume>();
        UnityEngine.Rendering.Universal.DepthOfField dof = null;
        if(volume != null && volume.profile.TryGet<UnityEngine.Rendering.Universal.DepthOfField>(out dof))
        {
            dof.focusDistance.value = currentCameraDistance + autoFocusDistanceOffset;
            dof.focalLength.value = autoFocusFocalLengthBase + currentCameraDistance*autoFocusFocalLengthMultiplier;
        }
    }

    #endregion
}