using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SceneSelector : MonoBehaviour
{
    public Dropdown dropdown1;
    public Dropdown dropdown2;
    public GameObject alliances; // Parent object containing factions
    public Text infantryCount1, skirmisherCount1, cavalryCount1;
    public Text infantryCount2, skirmisherCount2, cavalryCount2;    

    private Dictionary<string, int[]> allianceUnits = new Dictionary<string, int[]>(); // Stores unit counts for each alliance

    private void Start()
    {
        PopulateDropdown(dropdown1);
        PopulateDropdown(dropdown2);
        ResetUnitCounts();
    }

    private void PopulateDropdown(Dropdown dropdown)
    {
        if (dropdown == null || alliances == null) return;

        dropdown.ClearOptions();
        List<string> factionOptions = new List<string>();

        // Loop through all children of Alliances and add their names
        foreach (Transform child in alliances.transform)
        {
            factionOptions.Add(child.gameObject.name);
        }

        dropdown.AddOptions(factionOptions);
    }    

    private void ResetUnitCounts()
    {
        allianceUnits.Clear();
        allianceUnits.Add("Alliance1", new int[3]);
        allianceUnits.Add("Alliance2", new int[3]);
        UpdateUI();
    }

    public void AddInfantryAlliance1() { AddUnit("Alliance1", 0); }
    public void AddSkirmisherAlliance1() { AddUnit("Alliance1", 1); }
    public void AddCavalryAlliance1() { AddUnit("Alliance1", 2); }

    public void AddInfantryAlliance2() { AddUnit("Alliance2", 0); }
    public void AddSkirmisherAlliance2() { AddUnit("Alliance2", 1); }
    public void AddCavalryAlliance2() { AddUnit("Alliance2", 2); }

    private void AddUnit(string allianceKey, int unitType)
    {
        if (allianceUnits.ContainsKey(allianceKey))
        {
            allianceUnits[allianceKey][unitType]++;
            UpdateUI();
        }
    }

    private void UpdateUI()
    {
        int[] alliance1Units = allianceUnits["Alliance1"];
        int[] alliance2Units = allianceUnits["Alliance2"];

        infantryCount1.text = alliance1Units[0].ToString();
        skirmisherCount1.text = alliance1Units[1].ToString();
        cavalryCount1.text = alliance1Units[2].ToString();

        infantryCount2.text = alliance2Units[0].ToString();
        skirmisherCount2.text = alliance2Units[1].ToString();
        cavalryCount2.text = alliance2Units[2].ToString();
    }

    public void LoadShowcaseScene()
    {
        SaveUnitData();
        Debug.Log("Loading Showcase Area...");
        SceneManager.LoadScene("Showcase Area");
    }

    public void LoadDemoScene()
    {
        SaveUnitData();
        Debug.Log("Loading demo...");
        SceneManager.LoadScene("demo");
    }   

    private void SaveUnitData()
    {
        string alliance1Name = dropdown1.options[dropdown1.value].text;
        string alliance2Name = dropdown2.options[dropdown2.value].text;

        PlayerPrefs.SetString("Alliance1", alliance1Name);
        PlayerPrefs.SetString("Alliance2", alliance2Name);

        for (int i = 0; i < 3; i++) // Increase this number if unit type amount is increased
        {
            PlayerPrefs.SetInt("Alliance1_Unit" + i, allianceUnits["Alliance1"][i]);
            PlayerPrefs.SetInt("Alliance2_Unit" + i, allianceUnits["Alliance2"][i]);


        }

        PlayerPrefs.Save();
        Debug.Log("Saving Unit Data...");
        Debug.Log($"Alliance1 ({alliance1Name}) Units: Infantry={allianceUnits["Alliance1"][0]}, Skirmishers={allianceUnits["Alliance1"][1]}, Cavalry={allianceUnits["Alliance1"][2]}");
        Debug.Log($"Alliance2 ({alliance2Name}) Units: Infantry={allianceUnits["Alliance2"][0]}, Skirmishers={allianceUnits["Alliance2"][1]}, Cavalry={allianceUnits["Alliance2"][2]}");   
    } 
}