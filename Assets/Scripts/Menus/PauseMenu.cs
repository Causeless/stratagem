using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public void returnToMainMenu()
    {
        Debug.Log("Returning to main menu...");
        SceneManager.LoadScene("Main Menu");
    }
}
