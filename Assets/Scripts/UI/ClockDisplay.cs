﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockDisplay : MonoBehaviour
{
    private float clockTime = 0.0f;
    private int minutes = 0;
    private int seconds = 0;

    void Update()
    {
        clockTime += Time.deltaTime;
        minutes = Mathf.FloorToInt(clockTime / 60f);
        seconds = Mathf.FloorToInt(clockTime - minutes * 60);
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(60, 50, 100, 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        string text = string.Format("{0:00}:{1:00}", minutes, seconds);
        GUI.Label(rect, text, style);
    }
}