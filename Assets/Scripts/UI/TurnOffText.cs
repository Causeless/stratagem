﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnOffText : MonoBehaviour {

    Text text;

	// Use this for initialization
	void Start () {
        text = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (TimeScript.IsPausedTurn)
        {
            //text.enabled = true;//uncomment if want pausing back
        }
        else
        {
            text.enabled = false;
        }
	}
}
