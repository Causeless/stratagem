﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using LinearAlgebra;
using Pathfinding;

public class EntitySelector : MonoBehaviour
{
    public LayerMask _layerMask;
    public Image _selectionImage;
    public Dropdown ChargeBehaviourDropdown;
    public Dropdown ShootBehaviourDropdown;
    public Dropdown FormationBehaviourDropdown;

    [HideInInspector]
    public Vector3[] _worldPosCornerArray = new Vector3[4];

    public static List<Unit> _selectedUnits = new List<Unit>();

    Rectangle _selectionRect;
    LineSegment _clickDragLine;
    StrateCam _camera;
    [HideInInspector]
    public float _verticalFormationSpacing = 1f;
    [HideInInspector]
    public float _horizontalFormationSpacing = 1f;
    public float _formationSpacing = 2f;
    public float _looseFormationSpacing = 1f;
    public float _singleClickSelectionSize = 40f;

    private int _clickCount = 0;

    public int _dragThresholdInGridSquaresPerUnit = 2;

    GameObject _unitCollection;

    public float _speed = 3.5f;

    DoubleClickDetector _dcd;

    // Use this for initialization
    void Start()
    {
        _unitCollection = GameObject.FindWithTag("Unit Collection");
        _dcd = GetComponent<DoubleClickDetector>();

        Camera mainCamera = Camera.main;
         _camera = mainCamera.GetComponent<StrateCam>();

        PopulateDropdownWithEnumValues();

        ChargeBehaviourDropdown.onValueChanged.AddListener(delegate
        {
            ChargeBehaviourDropdownValueChanged(ChargeBehaviourDropdown);
        });

        ShootBehaviourDropdown.onValueChanged.AddListener(delegate
        {
            ShootBehaviourDropdownValueChanged(ShootBehaviourDropdown);
        });

        FormationBehaviourDropdown.onValueChanged.AddListener(delegate
        {
            FormationBehaviourDropdownValueChanged(FormationBehaviourDropdown);
        });
    }

    bool PointNearUnit(Unit unit, Vector3 point, float dCheck = 1.2f)
    {
        bool inside = false;
        foreach (SubUnit subUnit in unit.SubUnits)
        {
            point.y = subUnit.GetPosition().y;

            if ((subUnit.GetPosition() - point).sqrMagnitude <= dCheck * dCheck)
            {
                inside = true;
                break;
            }
        }

        return inside;
    }

    void ResetSelectionBox()
    {
        _selectionRect.topLeftCorner = Input.mousePosition + new Vector3(-_singleClickSelectionSize * 0.5f, _singleClickSelectionSize * 0.5f, 0.0f);

        // Note - this'll immediately be reset to the actual mouse position in UpdateSelectionBox, but setting this so the box is around the clicked position is convenient for single click selection
        _selectionRect.bottomRightCorner = Input.mousePosition + new Vector3(_singleClickSelectionSize * 0.5f, -_singleClickSelectionSize * 0.5f, 0.0f);

        _selectionImage.enabled = false;
    }

    void UpdateSelectionBox()
    {
        _selectionRect.bottomRightCorner = Input.mousePosition;
        float selectionWidth = _selectionRect.bottomRightCorner.x - _selectionRect.topLeftCorner.x;
        float selectionHeight = _selectionRect.topLeftCorner.y - _selectionRect.bottomRightCorner.y;

        _selectionImage.rectTransform.sizeDelta = new Vector2(Mathf.Abs(selectionWidth), Mathf.Abs(selectionHeight)); //get the absolute value of width and height, otherwise the image would not render
        _selectionImage.rectTransform.position = new Vector3(_selectionRect.topLeftCorner.x + selectionWidth / 2, _selectionRect.bottomRightCorner.y + selectionHeight / 2, 0);

        _selectionImage.enabled = IsDragSelecting();
    }

    bool IsDragSelecting()
    {
        // The player is only drag selecting if clicking and the box is sufficiently large (otherwise it's presumably a click)
        return Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject() &&
               System.Math.Abs(_selectionRect.bottomRightCorner.x - _selectionRect.topLeftCorner.x) > _singleClickSelectionSize &&
               System.Math.Abs(_selectionRect.topLeftCorner.y - _selectionRect.bottomRightCorner.y) > _singleClickSelectionSize;
    }

    List<Unit> GetUnitsInBounds(Bounds bounds)
    {
        var units = new List<Unit>();

        foreach (Unit unit in _unitCollection.GetComponentsInChildren<Unit>())
        {
            // todo; actually add alliance checks. Once we have AI or MP or whatever...
            /*
            if (unit.alliance != _ourAlliance)
            {
                continue;
            }
            */

            foreach (SubUnit subUnit in unit.SubUnits)
            {
                if (bounds.Contains(Camera.main.WorldToScreenPoint(subUnit.Position)))
                {
                    units.Add(unit);
                    unit.UnitSelected = true;
                    break;
                }
            }
        }

        return units;
    }

    void CheckUnitClicked()
    {
        if (!Input.GetKey(KeyCode.LeftControl))
        {
            ClearSelections();
        }

        // We reset the selection box so it is centered around the mouse and use the minimum selection area to find an entity within that area
        ResetSelectionBox();
        var screenBounds = GetBounds(_selectionRect.topLeftCorner, _selectionRect.bottomRightCorner);

        var units = GetUnitsInBounds(screenBounds);
        if (units.Count == 0)
        {
            return;
        }

        // Select the unit by doing click count mod unit count. This will cycle through the unit selected with each click
        var unit = units[_clickCount % units.Count];

        if (Input.GetKey(KeyCode.LeftControl) && _selectedUnits.Contains(unit))
        {
            ClearSelection(unit);
        }
        else
        {
            SelectUnit(unit);
            UpdateDropdownForSelectedUnits();
        }
    }

    void CheckUnitDragSelected()
    {
        UpdateSelectionBox();
        var screenBounds = GetBounds(_selectionRect.topLeftCorner, _selectionRect.bottomRightCorner);

        var units = GetUnitsInBounds(screenBounds);

        // Deselect anything that's in _selectedUnits but not in units
        if (!Input.GetKey(KeyCode.LeftControl))
        {
            for (int i = 0; i < _selectedUnits.Count; i++)
            {
                if (!units.Contains(_selectedUnits[i]))
                {
                    ClearSelection(_selectedUnits[i]);
                }
            }
        }

        // Select anything that's in units but not in _selectedUnits
        for (int i = 0; i < units.Count; i++)
        {
            if (!_selectedUnits.Contains(units[i]))
            {
                SelectUnit(units[i]);
            }
        }
        UpdateDropdownForSelectedUnits();
    }

    void SortAlongLine(List<Unit> units, LineSegment line)
    {
        System.Comparison<Unit> Sort = (Unit f1, Unit f2) =>
        {
            Vector3 f1vec = Vector3.Project(f1.GetPrimarySubUnit().Position, line.GetDirection());
            Vector3 f2vec = Vector3.Project(f2.GetPrimarySubUnit().Position, line.GetDirection());

            float f1val = f1vec.sqrMagnitude;
            float f2val = f2vec.sqrMagnitude;

            // We want to get the distance along a theoretically infinite line, not the strict distance from the first pos, so we negate the value if it is behind the startpoint of the line
            if (Vector3.Dot(_clickDragLine.GetDirection(), f1.GetPrimarySubUnit().Position) < 0)
                f1val = -f1val;

            if (Vector3.Dot(_clickDragLine.GetDirection(), f2.GetPrimarySubUnit().Position) < 0)
                f2val = -f2val;

            return f1val.CompareTo(f2val);
        };

        units.Sort(Sort);
    }

    // Update is called once per frame
    void Update()
    {
        #region test kill
        if (Input.GetKeyDown(KeyCode.Space) && _selectedUnits.Count == 1)//highlight one unit and press space bar kill an entity
        {
            // todo, write kill logic
        }
        #endregion

        if(Input.GetKeyDown(KeyCode.O) && _selectedUnits.Count > 0)
        {
            Unit firstUnit = _selectedUnits[0];
            SubUnit firstSubUnit = firstUnit.SubUnits[0];
            GameObject followObject = firstSubUnit.gameObject;
            if(_camera == null)
            {
                Debug.Log("Entity Selector cannot find a camera!");
            }
            _camera.Follow(followObject);
        }


        // Update the selection box so we can determine if the player is currently click-dragging
        UpdateSelectionBox();

        PlaceHolderManager.SetPlaceHoldersInactive();

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()) // Left click down
        {
            CheckUnitClicked();
        }
        else if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject()) // Left click up
        {
            // Player stopped click, so we reset selection box
            ResetSelectionBox();
            _clickCount++;
        }

        if (IsDragSelecting())
        {
            CheckUnitDragSelected();
        }

        if ((Input.GetMouseButtonDown(1) && !EventSystem.current.IsPointerOverGameObject() && TimeScript.IsPausedTurn))
        {
            #region RMB Down
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
            {
                _clickDragLine.firstPos = hit.point;
            }
            #endregion
        }
        else if (Input.GetMouseButtonUp(1) && !EventSystem.current.IsPointerOverGameObject() && _selectedUnits.Count > 0 && TimeScript.IsPausedTurn)
        {
            #region RMB Up
            // Todo, reimplement non-drag single click case
            if (IsDragLineOverThreshold())
            {
                var lines = GetLinesForUnits();
                for (int i = 0; i < _selectedUnits.Count; i++)
                {
                    Command_MoveUnit(_selectedUnits[i], lines[i]);
                }
            }
            #endregion
        }

        if (Input.GetMouseButton(1) && !EventSystem.current.IsPointerOverGameObject() && _selectedUnits.Count > 0 && TimeScript.IsPausedTurn)
        {
            #region RMB Hold
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
            {
                _clickDragLine.lastPos = hit.point;

                if (IsDragLineOverThreshold())
                {
                    SortAlongLine(_selectedUnits, _clickDragLine);

                    var lines = GetLinesForUnits();
                    var formations = GetPlaceholderFormations(_selectedUnits, lines);

                    for (int i = 0; i < formations.Count; i++)
                    {
                        var f = formations[i];
                        for (int j = 0; j < f.spots.Count; j++)
                        {
                            FormationSpot fs = f.spots[j];
                            PlaceHolderManager.SpawnPlaceholder(fs.position + new Vector3(0, .1f, 0), Quaternion.LookRotation(Vector3.up, fs.direction));
                        }
                    }
                }
            }
            #endregion
        }

        UpdateRunIcon();
    }

    public UnitCommander GetClosestCommander(Vector3 position, Faction faction)
    {
        UnitCommander closest = null;
        float minLengthSqr = Mathf.Infinity;
        for (int i = 0; i < _unitCollection.transform.childCount; i++)
        {
            var t = _unitCollection.transform.GetChild(i);
            Unit u = t.GetComponent<Unit>();
            UnitCommander uc = t.GetComponent<UnitCommander>();

            if (u == null ||
                u.FriendlyFaction != faction ||
                uc == null ||
                !uc.isActiveAndEnabled ||
                !uc.CanSendCourier())
            {
                continue;
            }

            var unit = t.GetComponent<Unit>();

            ABPath path = ABPath.Construct(unit.GetPrimarySubUnit().Position, position);
            AstarPath.StartPath(path, true);
            AstarPath.BlockUntilCalculated(path); // todo, in the future make this async

            float lengthSqr = path.GetTotalLengthSqr();
            if (lengthSqr < minLengthSqr)
            {
                closest = uc;
                minLengthSqr = lengthSqr;
            }
        }

        return closest;
    }

    public void Command_MoveUnit(Unit unit, LineSegment frontLine)
    {
        var command = new Unit.PathMovementCommand(frontLine);

        if (unit.GetComponent<UnitCommander>())
        {
            unit.ClearQueuedCommands();
            unit.QueueCommand(command); // Commander units respond immediately
            return;
        }

        UnitCommander closestCommander = GetClosestCommander(unit.GetPrimarySubUnit().Position, unit.FriendlyFaction);
        if (closestCommander == null)
        {
            unit.ClearQueuedCommands();
            unit.QueueCommand(command); // No unit is a commander, respond immediately (we should probably change this to give no order at all in the future?)
        }
        else
        {
            closestCommander.CommandUnit(unit, command);
        }
    }

    public static void ClearSelections()
    {
        foreach (var f in _selectedUnits)
        {
            f.EnableSelectionHighlight(false);
            f.UnitSelected = false;
        }

        //deselect all other cards
        UnitCard[] fc = GameObject.FindWithTag("Unit Panel").GetComponentsInChildren<UnitCard>();
        for (int i = 0; i < fc.Length; i++)
        {
            fc[i].GetComponent<Toggle>().isOn = false;
        }

        _selectedUnits.Clear();
    }

    public void UpdateRunIcon()
    {
        UnitCard[] fc = GameObject.FindWithTag("Unit Panel").GetComponentsInChildren<UnitCard>();
        for (int i = 0; i < fc.Length; i++)
        {
            if(fc[i].GetComponent<Toggle>().isOn == false)
            {
                var unit = fc[i]._unit;

                bool isRunning = unit.IsRunning;
                if(isRunning)
                {
                    fc[i]._runIcon.enabled = true;
                }
                else
                {
                    fc[i]._runIcon.enabled = false;
                }
            }
        }
    }

    public static void SelectUnit(Unit unit)
    {
        if (_selectedUnits.Contains(unit))
        {
            return;
        }

        //add card selection
        UnitCard[] fc = GameObject.FindWithTag("Unit Panel").GetComponentsInChildren<UnitCard>();
        for (int i = 0; i < fc.Length; i++)
        {
            if (fc[i]._unit == unit)
            {
                fc[i].GetComponent<Toggle>().isOn = true;
                fc[i].SnapToMe();
                break;
            }
        }

        unit.EnableSelectionHighlight(true);
        _selectedUnits.Add(unit);
        unit.UnitSelected = true;
    }

    public static void ClearSelection(Unit unit)
    {
        if (!_selectedUnits.Contains(unit))
        {
            return;
        }

        //deselect card
        UnitCard[] fc = GameObject.FindWithTag("Unit Panel").GetComponentsInChildren<UnitCard>();
        for (int i = 0; i < fc.Length; i++)
        {
            if (fc[i]._unit == unit)
            {
                fc[i].GetComponent<Toggle>().isOn = false;
                break;
            }
        }

        unit.EnableSelectionHighlight(false);
        _selectedUnits.Remove(unit);
        unit.UnitSelected = false;
    }

    Vector3 GetMouseWorldPos()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000f, _layerMask))
            return hit.point;
        else
            return Vector3.zero;
    }
    Vector3 GetTerrainHeightAt(Vector3 pos)
    {
        RaycastHit terrainHit;
        Vector3 rayOrigin = new Vector3(pos.x, 100, pos.z);
        Ray terrainRay = new Ray(rayOrigin, Vector3.down);

        if (Physics.Raycast(terrainRay, out terrainHit, Mathf.Infinity, _layerMask))
            return new Vector3(pos.x, terrainHit.point.y, pos.z);//make sure the waypoint position matches the terrain height
        else
            return pos;
    }

    /// <summary>
    /// Calculates the new formation of each unit with the given line and outputs the formations to the given list.
    /// This method changed a lot for pike formations
    /// </summary>

    // Todo, in the future this should probably return actual formation components on gameobjects?
    public struct FormationSpotsWithLine
    {
        public LineSegment line;
        public List<FormationSpot> spots;
    }

    // This should probably really be on subunit to give us instead
    public static List<FormationSpotsWithLine> GetPlaceholderFormations(List<Unit> units, List<LineSegment> linesForUnits)
    {
        List<FormationSpotsWithLine> formations = new List<FormationSpotsWithLine>(units.Count);

        Assert.IsTrue(linesForUnits.Count >= units.Count);

        for (int i = 0; i < units.Count; i++)
        {
            var unit = units[i];
            var unitFormation = unit.GetComponent<UnitFormation>();
            var unitLines = unitFormation.GetForLine(linesForUnits[i], unit.SubUnits);
            for (int j = 0; j < unitLines.segments.Count; j++)
            {
                SubUnit subUnit = unit.SubUnits[j];

                LineSegment subUnitLine = unitLines.segments[j];
                LineSegment adjustedLine = subUnit.GetAdjustedEndGoalLine(subUnitLine, subUnit.Entities.Count, SubUnit.SubUnitFormationWidth.Square);
                var subUnitFormation = subUnit.GetFormation();
                formations.Add(new FormationSpotsWithLine
                {
                    line = subUnitLine,
                    spots = subUnitFormation.GetSpotsForLine(adjustedLine, subUnit.Entities.Count)
                });
            }
        }

        return formations;
    }

    private float GetAdjustedClickDragLineLength()
    {
        float totalLength = _clickDragLine.GetLength();

        // Add some artificial length, to make it feel a bit nicer:
        // so it doesn't need to fit strictly within the line's length
        // but instead generally matches the length
        totalLength += GridManager.s_instance.GridSquareSize;

        return totalLength;
    }

    private bool IsDragLineOverThreshold()
    {
        LineSegment newLine = _clickDragLine;
        newLine.lastPos = newLine.firstPos + (newLine.GetDirection() * GetAdjustedClickDragLineLength() * 0.95f /* epsilon */);

        var lines = GridManager.s_instance.AliasToGridCorners(newLine);
        return lines.Count >= _selectedUnits.Count * _dragThresholdInGridSquaresPerUnit;
    }

    public List<LineSegment> GetLinesForUnits()
    {
        List<LineSegment> segmentsForUnits = new List<LineSegment>();

        Vector3 newPos = _clickDragLine.firstPos;
        Vector3 dir = _clickDragLine.GetDirection();

        float totalLength = GetAdjustedClickDragLineLength();
        float lengthPerUnit = totalLength / _selectedUnits.Count;
        // Ugly hack to stop overlapping units...
        lengthPerUnit += 0.2f;

        // Add a little epsilon backwards to encourage units to stand next to each other
        Vector3 epsilon = dir * -GridManager.s_instance.GridSquareSize * 0.25f;

        for (int i = 0; i < _selectedUnits.Count; i++)
        {
            LineSegment unitLine = new LineSegment
            {
                firstPos = newPos,
                lastPos = newPos + (dir * lengthPerUnit)
            };

            newPos = unitLine.lastPos + epsilon;

            segmentsForUnits.Add(unitLine);
        }

        return segmentsForUnits;
    }

    public bool AreAnySelectedUnitsWalking()
    {
        foreach (var u in _selectedUnits)
        {
            if (!u._isRunning)
            {
                return true;
            }
        }

        return false;
    }

    public bool AreAnySelectedUnitsRunning()
    {
        foreach (var u in _selectedUnits)
        {
            if (u._isRunning)
            {
                return true;
            }
        }

        return false;
    }

    public void ToggleUnitsRunning()
    {
        // If any unit is walking, set all running, otherwise set all walking
        bool setRunning = AreAnySelectedUnitsWalking();
        foreach (var u in _selectedUnits)
        {
            u.SetRun(setRunning);
        }
    }

    private void PopulateDropdownWithEnumValues()
    {
        ChargeBehaviourDropdown.ClearOptions();
        ShootBehaviourDropdown.ClearOptions();
        FormationBehaviourDropdown.ClearOptions();

        List<string> chargeEnumNames = new List<string>{ "Mixed" };
        chargeEnumNames.AddRange(System.Enum.GetNames(typeof(Unit.UnitChargeBehaviour)));
        List<string> shootEnumNames = new List<string>{ "Mixed" };
        shootEnumNames.AddRange(System.Enum.GetNames(typeof(Unit.UnitShootingBehaviour)));
        List<string> formationEnumNames = new List<string>{ "Mixed" };
        formationEnumNames.AddRange(System.Enum.GetNames(typeof(Unit.UnitSpecialFormation)));

        ChargeBehaviourDropdown.AddOptions(chargeEnumNames);
        ShootBehaviourDropdown.AddOptions(shootEnumNames);
        FormationBehaviourDropdown.AddOptions(formationEnumNames);
    }

    private void UpdateDropdownForSelectedUnits()
    {
        if (_selectedUnits.Count == 0) return;

        UpdateDropdownValue(ChargeBehaviourDropdown, u => (int)u.ChargeBehaviour);
        UpdateDropdownValue(ShootBehaviourDropdown, u => (int)u.ShootingBehaviour);
        UpdateDropdownValue(FormationBehaviourDropdown, u => (int)u.SpecialFormation);

        ChargeBehaviourDropdown.RefreshShownValue();
        ShootBehaviourDropdown.RefreshShownValue();
        FormationBehaviourDropdown.RefreshShownValue();
    }

    private void UpdateDropdownValue(Dropdown dropdown, System.Func<Unit, int> getEnum)
    {
        int firstValue = getEnum(_selectedUnits[0]);
        for (int i = 1; i < _selectedUnits.Count; i++)
        {
            if (getEnum(_selectedUnits[i]) != firstValue)
            {
                dropdown.value = 0; // Set dropdowns to "Mixed"
                return;
            }
        }
        dropdown.value = firstValue + 1;
    }

    public void ChargeBehaviourDropdownValueChanged(Dropdown selection)
    {
        if (selection.value == 0) return; // "Mixed" should not change the behaviors

        Unit.UnitChargeBehaviour choice = (Unit.UnitChargeBehaviour)(selection.value - 1);

        foreach (var u in _selectedUnits)
        {
            u.ChargeBehaviour = choice;
        }
    }

    public void ShootBehaviourDropdownValueChanged(Dropdown selection)
    {
        if (selection.value == 0) return; // "Mixed" should not change the behaviors

        Unit.UnitShootingBehaviour choice = (Unit.UnitShootingBehaviour)(selection.value - 1);

        foreach (var u in _selectedUnits)
        {
            u.ShootingBehaviour = choice;
        }
    }

    public void FormationBehaviourDropdownValueChanged(Dropdown selection)
    {
        if (selection.value == 0) return; // "Mixed" should not change the behaviors

        Unit.UnitSpecialFormation choice = (Unit.UnitSpecialFormation)(selection.value - 1);
        
        foreach (var u in _selectedUnits)
        {
            u.SpecialFormation = choice;              
        }
    }

    List<Transform> GetEntityList(Unit unit)
    {
        List<Transform> entityList = new List<Transform>();
        foreach (Transform entity in unit.transform)
        {
            entityList.Add(entity);
        }
        return entityList;
    }

    Bounds GetBounds(Vector3 pos1, Vector3 pos2)
    {
        var min = Vector3.Min(pos1, pos2);
        var max = Vector3.Max(pos1, pos2);
        min.z = Camera.main.nearClipPlane;
        max.z = Camera.main.farClipPlane;

        Bounds bounds = new Bounds();
        bounds.SetMinMax(min, max);
        return bounds;
    }

    void OnDrawGizmosSelected()
    {
        Bounds b = GetBounds(_selectionRect.topLeftCorner, _selectionRect.bottomRightCorner);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(b.center, b.size);
    }
}