﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnOffButton : MonoBehaviour {

    Image image;
    Button button;

    // Use this for initialization
    void Start()
    {
        image = this.GetComponent<Image>();
        button = this.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (TimeScript.IsPausedTurn)
        {
            //image.enabled = true;//uncomment if you want to bring pausing back
            //button.enabled = true;//same here
        }
        else
        {
            image.enabled = false;
            button.enabled = false;
        }
    }
}
