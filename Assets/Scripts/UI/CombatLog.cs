using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatLog : MonoBehaviour
{
    public static CombatLog instance;

    [SerializeField] bool LogEnabled = true;
    [SerializeField] RectTransform displayRect;
    [SerializeField] Text displayText;

    float initHeight;
    
    void Awake()
    {
        if(CombatLog.instance != null)
            DestroyImmediate(gameObject);
        else CombatLog.instance = this;

        LogEnabled = true;
        initHeight = displayRect.anchoredPosition.y;
    }

    public void ChangeDisplayPosition(float newPos)
    {
        displayRect.anchoredPosition = new Vector2(displayRect.anchoredPosition.x, initHeight + newPos);
    }

    public void Log(string newLog)
    {
        if(LogEnabled)
        {
            displayText.text = newLog + "\n" + displayText.text;
        }
    }
    
    void Update()
    {
        
    }
}
