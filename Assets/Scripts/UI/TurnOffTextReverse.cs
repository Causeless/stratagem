﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnOffTextReverse : MonoBehaviour {

    Text text;

	// Use this for initialization
	void Start () {
        text = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (TimeScript.IsPausedTurn)
        {
            text.enabled = false;
        }
        else
        {
            //text.enabled = true;//re-enable if you want to add pausing back in
        }
	}
}
