using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System;
using System.Numerics;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;


public class Frontline : MonoBehaviour
{    
    [HideInInspector]       
    public Unit_SpatialPartitionGrid FrontlineUnitGrid;
    [HideInInspector]
    public BoundingBoxRenderer _boundingBoxRender;

    [Header("Frontline Variables")]
    public Unit ChargeLeader;
    public bool FrontlineInitiated = false;
    public Vector3 CentrePoint;
    public Vector3 StartPoint;
    public Vector3 EndPoint;
    public Vector3 FrontlineFacing;
    public int MaxFrontlineSubUnitWidth;
    public float FrontlineSubUnitThreshold = 0.3f;
    public int MaxUnits;
    public int AlliedReserveLines = 1;
    public int EnemyReserveLines = 1;
    public float TooFarFromFrontline = 50;
    public int ReserveDistanceFromFrontline = 35;
    public float FlankingDotThreshold = 25f;
    public float FrontlineSearchRadius = 70f;
    public int SearchLengthReduction = 5;
    public float FrontlineCheckCooldown = 3f;
    private float FrontlineCheckTimer = 0f;
    [Header("Frontline Variables")]
    public bool FrontlineInLull = false;
    public float FrontlineLullCooldown = 15f;
    private float FrontlineLullTimer = 0f;
    private bool FrontlineLullPositionsSet = false;
    public float FrontlineMoralePushback = 4f;
    public float CombatDistTimeThreshold = 5f;
    private float CombatDistTimer = 0f;
    

    [Header("Frontline Lists")]
    public List<Unit> CombinedUnits;
    public List<Unit> AlliedUnits;
    public List<Unit> EnemyUnits;
    public Vector3 AlliedPosition;
    public Vector3 EnemyPosition;    
    public List<Unit> AlliedFrontlineUnits;
    public List<Unit> AlliedReserveUnits;
    public List<Unit> EnemyFrontlineUnits;
    public List<Unit> EnemyReserveUnits;
    public List<Unit> AlliedFlankingUnits;
    public List<Unit> EnemyFlankingUnits;
    private List<Unit> _nearbyUnits;
    public List<AllianceFrontline> AllianceFrontlines;
    public List<AllianceFrontline> FlankingFrontlines;
    public List<AllianceFrontline> ReactionFrontlines;

    [Header("Debug Settings")]
    private bool _debugAllianceFrontlines = true;
    private bool _debugFlankingFrontlines = true;


    [Serializable]
    public class AllianceFrontline
    {
        public string lineName;
        public bool isAllied;
        public int lineValue;
        public Vector3 centrePoint;
        public Vector3 startPoint;
        public Vector3 endPoint;
        public Vector3 lineFacing;
        public List<Unit> attachedUnits;

        public AllianceFrontline(bool allied, int value, Vector3 start, Vector3 end, Vector3 facing, List<Unit> unitList)
        {
            isAllied = allied;
            lineValue = value;
            startPoint = start;
            endPoint = end;
            centrePoint = (start + end) * 0.5f;
            lineFacing = facing;
            lineName = (allied ? "AlliedLine" : "EnemyLine") + value;
            attachedUnits = new List<Unit>(unitList);
            Debug.DrawLine(startPoint, endPoint, isAllied ? Color.white : Color.red, 3f);
        }
    }

    private void Start()
    {
        Debug.Log(this.name + " initialised at " + CentrePoint);
        AlliedFrontlineUnits = new List<Unit>();
        AlliedReserveUnits = new List<Unit>();
        EnemyFrontlineUnits = new List<Unit>();
        EnemyReserveUnits = new List<Unit>();
        AlliedFlankingUnits = new List<Unit>();
        EnemyFlankingUnits = new List<Unit>();
        _nearbyUnits = new List<Unit>();
        AllianceFrontlines = new List<AllianceFrontline>();
        FlankingFrontlines = new List<AllianceFrontline>();
        FrontlineFacing = (CentrePoint - AlliedPosition).normalized;
        transform.position = CentrePoint;
        transform.forward = FrontlineFacing; 
        CreateFrontline();
        InitialiseFrontline();
        CalculateReserveLines();
        RedefineFrontline();   
        AssignCommitmentFrontlines();
        HaltReserveCharges();

        //Add frontline to faction list of frontlines.
        AlliedUnits[0].FriendlyFaction.AddFrontlines(this.gameObject);
        EnemyUnits[0].FriendlyFaction.AddFrontlines(this.gameObject);

        FrontlineInitiated = true;
    }

    private void FixedUpdate()
    {
        //If units in frontline are engaged, search for units entering or leaving the frontline.
        if(FrontlinesHaveEngaged()) 
        {
            FrontlineCheckTimer += Time.deltaTime;
            CombatDistTimer += Time.deltaTime;
            bool Timer = CombatDistTimer > CombatDistTimeThreshold;
            bool FrontlineCheck = FrontlineCheckTimer > FrontlineCheckCooldown;

            if(FrontlineCheck)
            {
                UpdateUnitsOnFrontline(FrontlineSearchRadius);
                CalculateFlankingLines();
                UpdateFrontlineSlots(); //Might need to turn this off while starting a lull?


                if(AllianceFrontlines.Count == 0)
                {
                    Debug.LogError(this.name + " Frontline has no slots assigned!");
                }

                foreach(Unit unit in CombinedUnits)
                {
                    unit._commitment.ChargeHalted = true; //This will have bad consequences, I'm warning you now.
                }      

                UpdateFrontlineCombatDistance(Timer);

                FrontlineCheckTimer = 0f; //Reset Frontline Search Timer
            }

            if(Timer && FrontlineCheck)
            {
                CombatDistTimer = 0f; //Reset Combat Dist Timer
            }
        }

        CheckForLulls();

        foreach(Unit unit in CombinedUnits)
        {
            unit._sprintExpired = unit._commitment.ChargeHalted;
        }
        
        DebugDrawFrontlineSlots();
        DebugDrawFlankingSlots();
        _boundingBoxRender.UpdateBoundingBoxRender(null, this, 1, Color.blue);
        if(Input.GetKeyDown(KeyCode.Space))
        {
            _boundingBoxRender.VisualizeBoundingBoxes = true;
        }

        transform.position = CentrePoint;
        transform.forward = FrontlineFacing;           
    }

    private bool FrontlinesHaveEngaged()
    {
        bool alliesReady = false;
        bool enemiesReady = false;

        foreach(Unit unit in AlliedUnits)
        {
            if(unit.UnitState == Unit.UnitStateMachine.Combat || unit.UnitState == Unit.UnitStateMachine.Braced)
            {
                alliesReady = true;
            }
        }

        foreach(Unit unit in EnemyUnits)
        {
            if(unit.UnitState == Unit.UnitStateMachine.Combat || unit.UnitState == Unit.UnitStateMachine.Braced)
            {
                enemiesReady = true;
            }
        }

        if(alliesReady && enemiesReady)
        {
            return true;
        }        

        return false;
    }

    private float CalculateCombatDistance(Unit unit)
    {
        //InverseLerp commitment between 0 morale = 0 and 100 morale = 1. 
        float commitment = Mathf.InverseLerp(0f, 100f, unit.Morale);
        //Then scale it so 100 morale = - Morale Pushback and 0 morale = Morale Pushback
        float scaledCommitment = Mathf.Lerp(FrontlineMoralePushback, - FrontlineMoralePushback + 1f, commitment);
        
        return scaledCommitment;
    }

    private void UpdateFrontlineCombatDistance(bool Timer)
    {
        foreach(Unit ally in AlliedFrontlineUnits)
        {
            ally._commitment.CombatDistance = CalculateCombatDistance(ally);
            ally._commitment.SetSubUnitCombatDistance(Timer);
        }  

        foreach(Unit enemy in EnemyFrontlineUnits)
        {
            enemy._commitment.CombatDistance = CalculateCombatDistance(enemy);
            enemy._commitment.SetSubUnitCombatDistance(Timer);
        }

        foreach(Unit ally in AlliedFlankingUnits)
        {
            ally._commitment.CombatDistance = CalculateCombatDistance(ally);
            ally._commitment.SetSubUnitCombatDistance(Timer);
        }  

        foreach(Unit enemy in EnemyFlankingUnits)
        {
            enemy._commitment.CombatDistance = CalculateCombatDistance(enemy);
            enemy._commitment.SetSubUnitCombatDistance(Timer);
        }
    }

    private void CheckForLulls()
    {
        if(!FrontlineInLull)
        {
            FrontlineLullPositionsSet = false;
            return;
        }

        foreach(Unit unit in CombinedUnits)
        {
            if(unit.UnitState != Unit.UnitStateMachine.Disengage)
            {
                unit._commitment.Breakoff = true;
            }
        }

        if(!FrontlineLullPositionsSet)
        {
            SetFallbackPosition();
            RetreatUnitsinLull(); 
        }

        FrontlineLullPositionsSet = true;
        
        foreach(Unit unit in CombinedUnits)
        {
            unit._commitment.Breakoff = false;
        }        
    }    

    private void InitialiseFrontline()
    {
        FrontlineUnitGrid = GameObject.FindWithTag("SpatialPartitionGrid").GetComponent<Unit_SpatialPartitionGrid>();
        _boundingBoxRender = this.gameObject.AddComponent<BoundingBoxRenderer>();
        Debug.Assert(_boundingBoxRender != null, "Bounding box data missing for " + this.name);

        //Start Bounding Box Renderer
        if (_boundingBoxRender != null)
        {
            _boundingBoxRender.InitializeBoundingBoxVisual();
        }

        PlaceFarUnitsInReserve(TooFarFromFrontline);
        CalculateFrontlineUnitCapacity();
        RemoveNonViableReserves();

        foreach(Unit ally in AlliedUnits)
        {   
            bool allyIsReserve = ally._commitment.IsReserveUnit;
            if(allyIsReserve)
            {                
                if(!AlliedReserveUnits.Contains(ally))
                {
                    AlliedReserveUnits.Add(ally);
                } 
            }
            else
            {
                if(!AlliedFrontlineUnits.Contains(ally))
                {
                    AlliedFrontlineUnits.Add(ally);
                } 
            }
        }

        foreach(Unit enemy in EnemyUnits)
        {   
            bool enemyIsReserve = enemy._commitment.IsReserveUnit;
            if(enemyIsReserve)
            {
                if(!EnemyReserveUnits.Contains(enemy))
                {
                    EnemyReserveUnits.Add(enemy);
                } 
            }
            else
            {
                if(!EnemyFrontlineUnits.Contains(enemy))
                {
                    EnemyFrontlineUnits.Add(enemy);
                } 
            }
        }   
    }

    private float CalculateChargeCommitment(Unit unit)
    {
        var unitCommit = unit._commitment;

        //Currently just using debug values to test.
        return 0f + unitCommit.DebugReduceChargeCommitment;
    }     

    private void CalculateReserveLines()
    {
        int alliedFrontlineSubUnits = ReturnSubUnitsInUnitList(AlliedFrontlineUnits);
        int alliedReserveSubUnits = ReturnSubUnitsInUnitList(AlliedReserveUnits);

        int enemyFrontlineSubUnits = ReturnSubUnitsInUnitList(AlliedFrontlineUnits);
        int enemyReserveSubUnits = ReturnSubUnitsInUnitList(AlliedReserveUnits);        

        AlliedReserveLines = Mathf.CeilToInt(alliedReserveSubUnits / alliedFrontlineSubUnits);
        EnemyReserveLines = Mathf.CeilToInt(enemyReserveSubUnits / enemyFrontlineSubUnits);
    }

    private void HaltCompleteCharges()
    {
        foreach(Unit ally in AlliedFrontlineUnits)
        {
            var allyCommmit = ally._commitment;
            allyCommmit.ChargeHalted = true; //IsChargeHalted(ally);
        }

        foreach(Unit enemy in EnemyFrontlineUnits)
        {
            var enemyCommmit = enemy._commitment;
            enemyCommmit.ChargeHalted = true; //IsChargeHalted(enemy);
        }        
    }     

    private void HaltReserveCharges()
    {
        foreach(Unit ally in AlliedUnits)
        {
            var allyCommmit = ally._commitment;
            if(allyCommmit.IsReserveUnit)
            {
                allyCommmit.ChargeHalted = true;
            }
        }

        foreach(Unit enemy in EnemyUnits)
        {
            var enemyCommmit = enemy._commitment;
            if(enemyCommmit.IsReserveUnit)
            {
                enemyCommmit.ChargeHalted = true;
            }
        }           
    }

    private bool IsChargeHalted(Unit unit)
    {
        //If a unit isn't in combat but has no tasks left, then it should halt its charge and change to braced state.
        var unitCommit = unit._commitment;

        if(unit.UnitState == Unit.UnitStateMachine.Combat || unitCommit.IsInChargeCommitment == false)
        {
            return false;
        }

        foreach(SubUnit subUnit in unit.SubUnits)
        {
            if(subUnit.TaskQueue.Count >= 1)
            {
                return false;
            }
        }

        return true;
    }

    private int ReturnSubUnitsInUnitList(List<Unit> unitList)
    {
        List<SubUnit> subUnitList = new List<SubUnit>();
        foreach(Unit unit in unitList)
        {
            foreach(SubUnit subUnit in unit.SubUnits)
            {
                subUnitList.Add(subUnit);
            }
        }

        return subUnitList.Count;
    }

    private void CreateFrontline()
    {
        //Get Direction between current postion and frontline centre point.
        Vector3 toSide = Vector3.Cross(FrontlineFacing, Vector3.up).normalized; 
        toSide *= MaxFrontlineSubUnitWidth * GridManager.s_instance.GridSquareSize * 0.5f;

        //Create frontline perpendicular to angle to centre point
        Vector3 startPoint = CentrePoint - toSide;
        Vector3 endPoint = CentrePoint + toSide;
        StartPoint = startPoint; 
        EndPoint = endPoint;        
    }

    private void CalculateFlankingLines()
    {
        FlankingFrontlines.Clear();
        CreateFlankingFrontline(1, true); //allied left
        CreateFlankingFrontline(2, true); //allied right
        CreateFlankingFrontline(3, true); //allied rear

        CreateFlankingFrontline(1, false); //enemy left
        CreateFlankingFrontline(2, false); //enemy right
        CreateFlankingFrontline(3, false); //enemy rear 

    }      

    private void CreateFlankingFrontline(int FrontlineValue, bool isAllied)
    {
        
        int reserveDist = Mathf.FloorToInt(ReserveDistanceFromFrontline / GridManager.s_instance.GridSquareSize);
        Vector3 facing = Vector3.zero;
        Vector3 toSide = Vector3.zero;
        Vector3 flankCentre = Vector3.zero;
        Vector3 left = Vector3.zero;
        Vector3 right = Vector3.zero;
        Vector3 startPoint = Vector3.zero;
        Vector3 endPoint = Vector3.zero;
        Vector3 centrePoint = Vector3.zero;

        List<Unit> unitList = new List<Unit>();
        
        int frontlineUnitDepth = 0;
        int reserveUnitDepth = 0;
        
        if(!isAllied)
        {
            var alliedFrontline = AllianceFrontlines.Find(frontline => frontline.lineValue == 1 && frontline.isAllied == true);
            startPoint = alliedFrontline.startPoint;
            endPoint = alliedFrontline.endPoint;
            frontlineUnitDepth = AllianceFrontlineUnitDepth(alliedFrontline);
        }
        else
        {
            var enemyFrontline = AllianceFrontlines.Find(frontline => frontline.lineValue == 1 && frontline.isAllied == false);
            startPoint = enemyFrontline.startPoint;
            endPoint = enemyFrontline.endPoint;
            frontlineUnitDepth = AllianceFrontlineUnitDepth(enemyFrontline);
        }

        if(startPoint == Vector3.zero || endPoint == Vector3.zero)
        {
            startPoint = StartPoint;
            endPoint = EndPoint;
        }

        //Left
        if(FrontlineValue == 1)
        {
            //If reserves are present, make the frontline longer.
            Math.Clamp(frontlineUnitDepth, 0, 6);
            if(EnemyReserveLinesActive(isAllied))
            {
                if(!isAllied)
                {
                    var alliedFrontline = AllianceFrontlines.Find(frontline => frontline.lineValue > 1 && frontline.isAllied == true);
                    if(alliedFrontline != null)
                    {
                        reserveUnitDepth = AllianceFrontlineUnitDepth(alliedFrontline);
                        frontlineUnitDepth = reserveUnitDepth + reserveDist;
                    }
                }
                else
                {
                    var enemyFrontline = AllianceFrontlines.Find(frontline => frontline.lineValue > 1 && frontline.isAllied == false);
                    if(enemyFrontline != null)
                    {
                        reserveUnitDepth = AllianceFrontlineUnitDepth(enemyFrontline);
                        frontlineUnitDepth = reserveUnitDepth + reserveDist;
                    }
                }
            }
 
            facing = Vector3.Cross(FrontlineFacing, Vector3.up).normalized; 
            toSide = FrontlineFacing;
            toSide *= frontlineUnitDepth * GridManager.s_instance.GridSquareSize;
            left = isAllied ? startPoint : startPoint - toSide;
            right = isAllied ? startPoint + toSide : startPoint;

            AllianceFrontline leftFrontline = new AllianceFrontline(isAllied, 1, left, right, facing, unitList); 
            FlankingFrontlines.Add(leftFrontline);
        }


        //Right
        if(FrontlineValue == 2)
        {
            //If reserves are present, make the frontline longer.
            Math.Clamp(frontlineUnitDepth, 0, 6);
            if(EnemyReserveLinesActive(isAllied))
            {
                if(!isAllied)
                {
                    var alliedFrontline = AllianceFrontlines.Find(frontline => frontline.lineValue > 1 && frontline.isAllied == true);
                    if(alliedFrontline != null)
                    {
                        reserveUnitDepth = AllianceFrontlineUnitDepth(alliedFrontline);
                        frontlineUnitDepth = reserveUnitDepth + reserveDist;
                    }
                }
                else
                {
                    var enemyFrontline = AllianceFrontlines.Find(frontline => frontline.lineValue > 1 && frontline.isAllied == false);
                    if(enemyFrontline != null)
                    {
                        reserveUnitDepth = AllianceFrontlineUnitDepth(enemyFrontline);
                        frontlineUnitDepth = reserveUnitDepth + reserveDist;
                    }
                }
            }

            if(startPoint == Vector3.zero || endPoint == Vector3.zero)
            {
                startPoint = StartPoint;
                endPoint = EndPoint;
            }
            
            
            facing =  - Vector3.Cross(FrontlineFacing, Vector3.up).normalized;  
            toSide = FrontlineFacing;
            toSide *= frontlineUnitDepth * GridManager.s_instance.GridSquareSize;
            left = isAllied ? endPoint : endPoint - toSide;
            right = isAllied ? endPoint + toSide : endPoint;
            AllianceFrontline rightFrontline = new AllianceFrontline(isAllied, 2, left, right, facing, unitList); 
            FlankingFrontlines.Add(rightFrontline);
        }

        //Rear
        if(FrontlineValue == 3) //TODO: Should make it so its a line between the end points of left and right flanks!!
        {
            AllianceFrontline flankSlotLeft = null;
            AllianceFrontline flankSlotRight = null;
            
            if(isAllied)
            {
                flankSlotLeft = FlankingFrontlines.Find(frontline => frontline.lineValue == 1 && frontline.isAllied == true);
                flankSlotRight = FlankingFrontlines.Find(frontline => frontline.lineValue == 2 && frontline.isAllied == true);
            }
            else
            {
                flankSlotLeft = FlankingFrontlines.Find(frontline => frontline.lineValue == 1 && frontline.isAllied == false);
                flankSlotRight = FlankingFrontlines.Find(frontline => frontline.lineValue == 2 && frontline.isAllied == false);               
            }

            if(flankSlotLeft == null && flankSlotRight == null)
            {
                return;
            }

            left = isAllied ? flankSlotLeft.endPoint : flankSlotLeft.startPoint;
            right = isAllied ? flankSlotRight.endPoint : flankSlotRight.startPoint;
            facing = FrontlineFacing;

            AllianceFrontline rearFrontline = new AllianceFrontline(isAllied, 3, left, right, facing, unitList); 
            FlankingFrontlines.Add(rearFrontline);
        }

        if(FrontlineValue > 3 || FrontlineValue <= 0)
        {
            Debug.LogWarning("Invalid FronlineValue when trying to create flanking positions");
        }
    }

    private bool EnemyReserveLinesActive(bool isAllied)
    {
        if(isAllied)
        {
            if(AllianceFrontlines.Find(frontline => frontline.lineValue > 1 && frontline.isAllied == false) != null)
            {
                return true;
            }
        }
        else
        {
            if(AllianceFrontlines.Find(frontline => frontline.lineValue > 1 && frontline.isAllied == true) != null)
            {
                return true;
            }
        }

        return false;
    }

    private int AllianceFrontlineUnitDepth(AllianceFrontline frontlineSlot)
    {
        float addedDepth = 0;
        
        foreach(Unit unit in frontlineSlot.attachedUnits)
        {
            float depth = unit.SubUnitDepth;
            addedDepth += unit.SubUnitDepth;
        }
        float count = frontlineSlot.attachedUnits.Count;
        float averageDepth = addedDepth / count;
        int totalDepth = Mathf.CeilToInt(averageDepth);
        return totalDepth;
    }

    public void AssignCommitmentFrontlines() 
    {    
        if(AlliedUnits == null || AlliedUnits.Count <= 0)
        {
            Debug.LogError("Trying to start a charge around " + this.name + " but has no allied units listed");
            return;
        }

        if(EnemyUnits == null || EnemyUnits.Count <= 0)
        {
            Debug.LogError("Trying to start a charge around " + this.name + " but has no enemy units listed");
            return;
        }        

        // Instead we need to look to create a whole frontline that then uses the hungo alg to assign linesegments based on distance (we did this for movement orders already).
        if(CommitmentListUnitChargeBehaviour(AlliedFrontlineUnits, Unit.UnitChargeBehaviour.Charge))
        {
            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Charge))
            {
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, 0f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, 0f);

                //Reserve Units
                CreateAllianceFrontline(true, 2, AlliedReserveUnits, EndPoint, StartPoint, FrontlineFacing.normalized,  ReserveDistanceFromFrontline);
                CreateAllianceFrontline(false, 2, EnemyReserveUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, ReserveDistanceFromFrontline);
                return; 
            }

            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Hold))
            {
                Vector3 alliedAdjustment = FrontlineFacing * (ChargeLeader.ChargeRange / 2f);

                CentrePoint += alliedAdjustment;
                CreateFrontline(); 
                
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, 0f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, 0f);

                //Reserve Units
                CreateAllianceFrontline(true, 2, AlliedReserveUnits, EndPoint, StartPoint, FrontlineFacing.normalized,  ReserveDistanceFromFrontline - 1.15f);
                CreateAllianceFrontline(false, 2, EnemyReserveUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, ReserveDistanceFromFrontline - 1.15f); 

                foreach(Unit enemy in EnemyFrontlineUnits)
                {
                    var enemyCommmit = enemy._commitment;
                    enemyCommmit.ChargeHalted = true; 
                }  
            
                return;
            }

            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Fallback))
            {
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, - ChargeLeader.ChargeRange/2f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, ChargeLeader.ChargeRange);
                return;
            }
        }

        if(CommitmentListUnitChargeBehaviour(AlliedFrontlineUnits, Unit.UnitChargeBehaviour.Hold))
        {
            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Charge))
            {
                Vector3 alliedAdjustment = FrontlineFacing * (ChargeLeader.ChargeRange / 2f);

                CentrePoint -= alliedAdjustment;
                CreateFrontline();  
                
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, 1.15f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, 1.15f);

                //Reserve Units
                CreateAllianceFrontline(true, 2, AlliedReserveUnits, EndPoint, StartPoint, FrontlineFacing.normalized, 1.15f + ReserveDistanceFromFrontline);
                CreateAllianceFrontline(false, 2, EnemyReserveUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, 1.15f + ReserveDistanceFromFrontline);               
                
                foreach(Unit ally in AlliedFrontlineUnits)
                {
                    var allyCommmit = ally._commitment;
                    allyCommmit.ChargeHalted = true; 
                }  

                return;
            }  
            
            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Hold))
            {
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, ChargeLeader.ChargeRange/3f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, ChargeLeader.ChargeRange/3f);

                CreateAllianceFrontline(true, 2, AlliedReserveUnits, EndPoint, StartPoint, FrontlineFacing.normalized, (ChargeLeader.ChargeRange/2.1f) + ReserveDistanceFromFrontline);
                CreateAllianceFrontline(false, 2, EnemyReserveUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, (ChargeLeader.ChargeRange/2.1f) + ReserveDistanceFromFrontline);                
                
                HaltCompleteCharges();
                return;
            }

            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Fallback))
            {
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, ChargeLeader.ChargeRange/2.1f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, ChargeLeader.ChargeRange/2.1f);

                CreateAllianceFrontline(true, 2, AlliedReserveUnits, EndPoint, StartPoint, FrontlineFacing.normalized, (ChargeLeader.ChargeRange/2.1f) + ReserveDistanceFromFrontline);
                CreateAllianceFrontline(false, 2, EnemyReserveUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, (ChargeLeader.ChargeRange/2.1f) + ReserveDistanceFromFrontline);               
                
                HaltCompleteCharges();
                return;
            }            
        }

        if(CommitmentListUnitChargeBehaviour(AlliedFrontlineUnits, Unit.UnitChargeBehaviour.Fallback))
        {
            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Charge))
            {
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, ChargeLeader.ChargeRange);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, - ChargeLeader.ChargeRange/2f);

                Vector3 alliedAdjustment = FrontlineFacing * (ChargeLeader.ChargeRange / 2f);

                CentrePoint -= alliedAdjustment;
                CreateFrontline();  

                CreateAllianceFrontline(true, 2, AlliedReserveUnits, EndPoint, StartPoint, FrontlineFacing.normalized, ChargeLeader.ChargeRange + ReserveDistanceFromFrontline);
                CreateAllianceFrontline(false, 2, EnemyReserveUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, (- ChargeLeader.ChargeRange/2f) + ReserveDistanceFromFrontline);     
                return;
            }  
            
            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Hold))
            {
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, ChargeLeader.ChargeRange/2.1f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, ChargeLeader.ChargeRange/2.1f);

                CreateAllianceFrontline(true, 2, AlliedReserveUnits, EndPoint, StartPoint, FrontlineFacing.normalized, (ChargeLeader.ChargeRange/2.1f) + ReserveDistanceFromFrontline);
                CreateAllianceFrontline(false, 2, EnemyReserveUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, (ChargeLeader.ChargeRange/2.1f) + ReserveDistanceFromFrontline);                  
                
                HaltCompleteCharges();
                return;
            }

            if(CommitmentListUnitChargeBehaviour(EnemyFrontlineUnits, Unit.UnitChargeBehaviour.Fallback))
            {
                CreateAllianceFrontline(true, 1, AlliedFrontlineUnits, EndPoint, StartPoint, FrontlineFacing.normalized, ChargeLeader.ChargeRange/2f);
                CreateAllianceFrontline(false, 1, EnemyFrontlineUnits, StartPoint, EndPoint, - FrontlineFacing.normalized, ChargeLeader.ChargeRange/2f);
                return;
            }            
        }   

        Debug.Log("Charge Commitment Started at centre of " + CentrePoint.ToString() + " | Allied Position Set at " + AlliedPosition.ToString() + " | Enemy Postion Set at " + EnemyPosition.ToString());     
        foreach(Unit ally in AlliedUnits)
        {
            Debug.Log("Allied Unit: " + ally);
        }
        foreach(Unit enemy in EnemyUnits)
        {
            Debug.Log("Enemy Unit: " + enemy);
        }
        
    }

    private void UpdateReserveLine(bool isAllied)
    {
        var reserveLines = AllianceFrontlines.FindAll(AllianceFrontline => AllianceFrontline.isAllied == isAllied && AllianceFrontline.lineValue > 1);

        if(reserveLines.Count > 0)
        {
            foreach(var reserveLine in reserveLines)
            {
                reserveLine.attachedUnits.Clear();
                reserveLine.attachedUnits.AddRange(isAllied ? AlliedReserveUnits : EnemyReserveUnits);
                AssignFrontlineSegmentsToUnits(reserveLine);
            }
        }
        else
        {
            var frontLines = AllianceFrontlines.FindAll(AllianceFrontline => AllianceFrontline.isAllied == isAllied && AllianceFrontline.lineValue == 1);
            foreach(var frontLine in frontLines)
            {
                Vector3 toFrontline = frontLine.lineFacing;
                Vector3 left = frontLine.startPoint;
                Vector3 right = frontLine.endPoint;
                CreateAllianceFrontline(isAllied, 2, isAllied ? AlliedReserveUnits : EnemyReserveUnits, left, right, toFrontline, ReserveDistanceFromFrontline);
            }
        }        
    }

    private void AssignUnitToFlankingPosition(Unit unit, bool isAllied)  
    {
        var flankingLines = FlankingFrontlines.FindAll(AllianceFrontline => AllianceFrontline.isAllied == isAllied);
        float closestDist = Mathf.Infinity;
        AllianceFrontline closestFlankingPos = null;
        int MaxRearFlankingSize = Mathf.CeilToInt((AlliedFrontlineUnits.Count + EnemyFrontlineUnits.Count) / 2);

        if(flankingLines != null || flankingLines.Count == 0)
        {
            Debug.LogWarning("Tring to assign flanking lines but no flanking slots available.");
        }

        foreach(var flankingLine in flankingLines)
        {
            if(flankingLine.lineValue == 3)
            {
                if(flankingLine.attachedUnits == null || flankingLine.attachedUnits.Count < MaxRearFlankingSize) 
                {
                    if(closestDist > DistanceToFrontline(unit.UnitPosition, flankingLine.startPoint, flankingLine.endPoint))
                    {
                        closestDist = DistanceToFrontline(unit.UnitPosition, flankingLine.startPoint, flankingLine.endPoint);
                        closestFlankingPos = flankingLine;
                    }
                }
            }
            else if(flankingLine.attachedUnits == null || flankingLine.attachedUnits.Count == 0) //Side Flanks should only allow one unit.
            {
                if(closestDist > DistanceToFrontline(unit.UnitPosition, flankingLine.startPoint, flankingLine.endPoint))
                {
                    closestDist = DistanceToFrontline(unit.UnitPosition, flankingLine.startPoint, flankingLine.endPoint);
                    closestFlankingPos = flankingLine;
                }
            }
        }

        if(closestFlankingPos != null)
        {
            if(!closestFlankingPos.attachedUnits.Contains(unit))
            {
                closestFlankingPos.attachedUnits.Add(unit);
            }

            AssignFrontlineSegmentsToUnits(closestFlankingPos);
        }
    }  

    private void ReactToNearbyFrontline(Unit unit, bool isAllied)
    {
        Vector3 toFrontline = (CentrePoint - unit.UnitPosition).normalized;
        Vector3 toSide = Vector3.Cross(toFrontline, Vector3.up).normalized; 
        toSide *= 3 * GridManager.s_instance.GridSquareSize / 2;

        float dist = DistanceToFrontline(unit.UnitPosition, StartPoint, EndPoint);
        float displacement = (FrontlineSearchRadius / dist) + 2.0f; //adding a small buffer so they're definitely out of range.
        Vector3 tolerance = toFrontline * displacement; 
        
        Vector3 left = unit.UnitPosition + toSide - tolerance;
        Vector3 right = unit.UnitPosition - toSide - tolerance;

        List<Unit> reactingUnits = new List<Unit>();
        reactingUnits.Add(unit);

        AllianceFrontline reactionFrontline = new AllianceFrontline(isAllied, 1, left, right, toFrontline, reactingUnits);
        AssignFrontlineSegmentsToUnits(reactionFrontline);
    }

    public void CreateAllianceFrontline(bool isAllied, int frontlineValue, List<Unit> unitList, Vector3 frontlineLeft, Vector3 frontlineRight, Vector3 frontlineFacing, float frontlineTolerance)
    {
        int unitCount = unitList.Count;

        if (unitCount == 0)
        {
            Debug.LogWarning("Alliance frontline has no assigned units! " + this.name);
            return;
        }

        //Get CentrePoint and use frontline tolerance to move frontline away from it.
        Vector3 toFrontline = frontlineFacing;
        Vector3 tolerance = toFrontline * frontlineTolerance;
        Vector3 left = frontlineLeft - tolerance;
        Vector3 right = frontlineRight - tolerance;


        //Create frontline 
        AllianceFrontline allianceFrontline = new AllianceFrontline(isAllied, frontlineValue, left, right, toFrontline, unitList);
        AllianceFrontlines.Add(allianceFrontline);
        AssignFrontlineSegmentsToUnits(allianceFrontline);

    }

    private void AssignFrontlineSegmentsToUnits(AllianceFrontline allianceFrontline)
    {
        List<Unit> unitList = allianceFrontline.attachedUnits;
        int unitCount = unitList.Count;
        Vector3 frontlineLeft = allianceFrontline.startPoint;
        Vector3 frontlineRight = allianceFrontline.endPoint;
        Vector3 toFrontline = allianceFrontline.lineFacing;

        LinearAlgebra.LineSegment allianceLineSegment = new LinearAlgebra.LineSegment
        {
            firstPos = frontlineLeft,
            lastPos = frontlineRight
        };

        Vector3 dirAlliance = (allianceLineSegment.lastPos - allianceLineSegment.firstPos) / unitCount;
        List<LinearAlgebra.LineSegment> frontlineSegments = new List<LinearAlgebra.LineSegment>();

        // Create the line segments for each unit
        for (int i = 0; i < unitCount; i++)
        {
            LinearAlgebra.LineSegment unitSegment = new LinearAlgebra.LineSegment
            {
                firstPos = GridManager.s_instance.AliasToGridCorner(allianceLineSegment.firstPos + (dirAlliance * i)),
                lastPos = GridManager.s_instance.AliasToGridCorner(allianceLineSegment.firstPos + (dirAlliance * (i + 1)))
            };

            frontlineSegments.Add(unitSegment);

            // Debugging: Draw each segment for visualization
            Debug.DrawLine(unitSegment.firstPos, unitSegment.lastPos, Color.green, 3f);
        }

        // Create matrix of units and frontline segments available and reduce max travel distance
        float[,] multiDimensionalArrayOfAllListsOfPossibleRoutes = new float[unitCount, frontlineSegments.Count];

        for (int j = 0; j < unitCount; j++)
        {
            Vector3 unitPosition = unitList[j].UnitPosition; // Get the unit's position
            for (int k = 0; k < frontlineSegments.Count; k++)
            {
                Vector3 segmentMidpoint = frontlineSegments[k].GetMidpoint();
                float dist = (unitPosition - segmentMidpoint).sqrMagnitude; 
                multiDimensionalArrayOfAllListsOfPossibleRoutes[j, k] = dist; 
            }
        }

        // Run the Hungarian algorithm to find most optimal match ups
        TheHungarianAlgorithm hungarianAlgorithm = new TheHungarianAlgorithm(multiDimensionalArrayOfAllListsOfPossibleRoutes);
        int[] optimalAssignment = hungarianAlgorithm.Run();

        
        for (int j = 0; j < unitCount; j++)
        {
            int assignedSegmentIndex = optimalAssignment[j];
            LinearAlgebra.LineSegment assignedSegment = frontlineSegments[assignedSegmentIndex];

            // Adjust the assigned line segment using unit charge commitment.
            float unitTolerance = (unitList[j].ChargeRange * CalculateChargeCommitment(unitList[j])) / 2; // Should probably be distance to centrepoint.
            Vector3 unitToleranceOffset = toFrontline * unitTolerance;

            LinearAlgebra.LineSegment adjustedSegment = new LinearAlgebra.LineSegment
            {
                firstPos = assignedSegment.firstPos - unitToleranceOffset,
                lastPos = assignedSegment.lastPos - unitToleranceOffset
            };

            // Issue movement commands to the assigned segment
            Unit.PathMovementCommand moveCommand = new Unit.PathMovementCommand(adjustedSegment);
            unitList[j].ClearQueuedCommands(); 
            unitList[j].QueueCommand(moveCommand); 
        }
    } 

    public bool CommitmentListUnitChargeBehaviour(List<Unit> unitList, Unit.UnitChargeBehaviour behaviour)
    {
        foreach(Unit targetUnit in unitList)
        {
            if(targetUnit.ChargeBehaviour == behaviour)
            {
                return true;
            }
        }
        return false;
    }

    public bool CommitmentListUnitState(List<Unit> unitList, Unit.UnitStateMachine behaviour)
    {
        foreach(Unit targetUnit in unitList)
        {
            if(targetUnit.UnitState == behaviour)
            {
                return true;
            }
        }
        return false;
    }    

    private void UpdateUnitsOnFrontline(float range)
    {        
        float lineWidth = SearchLengthReduction * GridManager.s_instance.GridSquareSize;
        Vector3 rightToLeft = (StartPoint - EndPoint).normalized;
        Vector3 tolerance = rightToLeft * lineWidth;

        List<Unit> nearbyUnits = ChargeLeader._grid.GetEntsInBox(StartPoint - tolerance, EndPoint + tolerance, range);
        Faction faction = ChargeLeader.FriendlyFaction;

        if(nearbyUnits == null || nearbyUnits.Count == 0)
        {
            return;
        }

        if(_nearbyUnits == null || nearbyUnits.Count == 0)
        {
            _nearbyUnits = nearbyUnits;
        } 

        foreach(Unit unit in _nearbyUnits)
        {
            if(!nearbyUnits.Contains(unit) && unit._commitment.IsReacting)
            {
                unit._commitment.IsReacting = false;
            }
        }

        foreach(Unit nearbyUnit in nearbyUnits)
        {
            bool isAllied = nearbyUnit.FriendlyFaction.IsAllied(faction);

            //If frontline is at capacity, just move all new units out of the way
            if(CombinedUnits.Count >= MaxUnits)
            {
                if(!nearbyUnit._commitment.IsReacting)
                {
                    nearbyUnit._commitment.IsReacting = true;
                    ReactToNearbyFrontline(nearbyUnit, isAllied);
                }
            }

            //Determine whether the unit is far enough past the enemy line to flank if viable to join.
            if(!nearbyUnit._commitment.IsInChargeCommitment && ViableChargeBehaviour(nearbyUnit))
            {                 
                if(IsUnitFlanking(nearbyUnit, StartPoint, EndPoint, isAllied))
                {
                    AddNearbyUnitsToFlankSlots(nearbyUnit);  
                }
                else
                {
                    AddNearbyUnitsToReserve(nearbyUnit);
                }
            }

            //if not in charge, but too close, move out of range instead.
            if(!nearbyUnit._commitment.IsInChargeCommitment && !ViableChargeBehaviour(nearbyUnit))
            {
                if(!nearbyUnit._commitment.IsReacting)
                {
                    nearbyUnit._commitment.IsReacting = true;
                    ReactToNearbyFrontline(nearbyUnit, isAllied);
                }
            }
        }

        _nearbyUnits = nearbyUnits;
    }

    private void UpdateFrontlineSlots()
    {   
        List<AllianceFrontline> slotsToRemove = new List<AllianceFrontline>();
        List<Unit> unitsToRemove = new List<Unit>();

        FindEmptySlots(slotsToRemove);
        
        foreach(AllianceFrontline frontlineSlot in AllianceFrontlines)
        {
            if(frontlineSlot.attachedUnits != null || frontlineSlot.attachedUnits.Count > 0)
            {
                FindUnitsFarFromSlots(frontlineSlot, unitsToRemove);
            } 

            if(frontlineSlot.attachedUnits == null)
            {
                Debug.LogWarning("Frontline Slot " + frontlineSlot.lineName + "has no valid unit list");
            } 
        }
        
        foreach(AllianceFrontline flankSlot in FlankingFrontlines)
        {
            if(flankSlot.attachedUnits != null || flankSlot.attachedUnits.Count > 0)
            {
                FindUnitsFarFromSlots(flankSlot, unitsToRemove);
            }         
        }

        foreach(Unit unit in unitsToRemove)
        {
            RemoveUnitsFromSlots(unit);
        }

        foreach(AllianceFrontline frontlineSlot in slotsToRemove)
        {
            RemoveEmptySlots(frontlineSlot);
        }        
    }
    private void FindEmptySlots(List<AllianceFrontline> SlotList)
    {
        foreach(AllianceFrontline frontlineSlot in AllianceFrontlines)
        {
            if(frontlineSlot.attachedUnits.Count == 0)
            {
                if(!SlotList.Contains(frontlineSlot))
                {
                    SlotList.Add(frontlineSlot);
                    Debug.Log("Frontline Slot is empty and will be removed " + frontlineSlot.lineName);
                }
            }
        }

        foreach(AllianceFrontline frontlineSlot in FlankingFrontlines)
        {
            if(frontlineSlot.attachedUnits.Count == 0)
            {
                if(!SlotList.Contains(frontlineSlot))
                {
                    SlotList.Add(frontlineSlot);
                }  
            }
        }
    }

    private void FindUnitsFarFromSlots(AllianceFrontline frontlineSlot, List<Unit> removalList)
    {
        foreach(Unit unit in frontlineSlot.attachedUnits)
        {
            if(DistanceToFrontline(unit.UnitPosition, frontlineSlot.startPoint, frontlineSlot.endPoint) > FrontlineSearchRadius / 2)
            {
                if(!removalList.Contains(unit))
                {
                    removalList.Add(unit);
                }
            }
        }
    }    

    private void RemoveUnitsFromSlots(Unit unit)
    {
        bool isAllied = unit.FriendlyFaction.IsAllied(ChargeLeader.FriendlyFaction);

        CombinedUnits.Remove(unit); 
        unit._commitment.RemoveCommitment(unit);

        if(isAllied)
        {
            AlliedUnits.Remove(unit);
            AlliedReserveUnits.Remove(unit);
            AlliedFlankingUnits.Remove(unit); 

            if(unit == ChargeLeader)
            {
                if(AlliedFrontlineUnits.Count > 0)
                {
                    AlliedFrontlineUnits[0] = ChargeLeader;
                }
            }  
        }
        else
        {
            EnemyUnits.Remove(unit);
            EnemyReserveUnits.Remove(unit);
            EnemyFlankingUnits.Remove(unit); 

            if(unit == ChargeLeader)
            {
                if(EnemyFrontlineUnits.Count > 0)
                {
                    EnemyFrontlineUnits[0] = ChargeLeader;
                }
            }  
        }

        foreach(AllianceFrontline frontlineSlot in AllianceFrontlines)
        {
            if(frontlineSlot.attachedUnits.Contains(unit))
            {
                frontlineSlot.attachedUnits.Remove(unit);
                Debug.Log("Removing unit " + unit.name + " from Frontline Slot " + frontlineSlot.lineName + ". Remaining Unit Count: " + frontlineSlot.attachedUnits.Count);
            }
        }

        foreach(AllianceFrontline flankSlot in FlankingFrontlines)
        {
            if(flankSlot.attachedUnits.Contains(unit))
            {
                flankSlot.attachedUnits.Remove(unit);
                Debug.Log("Removing unit " + unit.name + " from Flanking Slot " + flankSlot.lineName + ". Remaining Unit Count: " + flankSlot.attachedUnits.Count);
            }
        }        
    }

    private void RemoveEmptySlots(AllianceFrontline emptySlot)
    {
        AllianceFrontlines.Remove(emptySlot);
        //FlankingFrontlines.Remove(emptySlot);
    }        

    private void AddNearbyUnitsToReserve(Unit nearbyUnit)
    {
        Faction faction = ChargeLeader.FriendlyFaction;

        //Make sure unit isn't already part of this frontline
        var nearbyCommit = nearbyUnit._commitment;
        if(!AlliedUnits.Contains(nearbyUnit)  && !EnemyUnits.Contains(nearbyUnit))
        {
            if(nearbyUnit.FriendlyFaction.IsAllied(faction))
            {
                if(nearbyCommit.UnitViableToCharge(nearbyUnit))
                {
                    if(!AlliedReserveUnits.Contains(nearbyUnit))
                    {
                        AlliedReserveUnits.Add(nearbyUnit);
                        UpdateReserveLine(true); //isAllied?
                        Debug.Log("Adding " + nearbyUnit + "to allied reserve pool for commitment " + this.name);
                    }
                    if(!AlliedUnits.Contains(nearbyUnit))
                    {
                        AlliedUnits.Add(nearbyUnit);
                        nearbyCommit.IsInChargeCommitment = true;
                        nearbyCommit.IsReserveUnit = true;
                        nearbyCommit.ChargeHalted = true;
                    }
                        if(!CombinedUnits.Contains(nearbyUnit))
                    {
                        CombinedUnits.Add(nearbyUnit);
                    }                        
                }
            }

            if(nearbyUnit.FriendlyFaction.IsHostile(faction))
            {
                if(nearbyCommit.UnitViableToCharge(nearbyUnit))
                {
                    if(!EnemyReserveUnits.Contains(nearbyUnit))
                    {
                        EnemyReserveUnits.Add(nearbyUnit);
                        UpdateReserveLine(false); //isAllied?
                        Debug.Log("Adding " + nearbyUnit + "to enemy reserve pool for commitment " + this.name);
                    }
                        if(!EnemyUnits.Contains(nearbyUnit))
                    {
                        EnemyUnits.Add(nearbyUnit);
                        nearbyCommit.IsInChargeCommitment = true;
                        nearbyCommit.IsReserveUnit = true;
                    }
                        if(!CombinedUnits.Contains(nearbyUnit))
                    {
                        CombinedUnits.Add(nearbyUnit);
                    }                        
                }
            }                
        }
    }

    private void AddNearbyUnitsToFlankSlots(Unit nearbyUnit)
    {
        Faction faction = ChargeLeader.FriendlyFaction;

        //Make sure unit isn't already part of this flanking line
        var nearbyCommit = nearbyUnit._commitment;
        var isAllied = nearbyUnit.FriendlyFaction.IsAllied(faction);
        if(!AlliedFlankingUnits.Contains(nearbyUnit)  && !EnemyFlankingUnits.Contains(nearbyUnit))
        {            
            if(!nearbyCommit.IsInChargeCommitment && ViableChargeBehaviour(nearbyUnit))
            {
                if(isAllied)
                {
                    AlliedFlankingUnits.Add(nearbyUnit);
                    AssignUnitToFlankingPosition(nearbyUnit, true); //isAllied?
                    Debug.Log(nearbyUnit + " is flanking the enemy lines in commitment " + this.name);
                    
                    if(!AlliedUnits.Contains(nearbyUnit))
                    {
                        AlliedUnits.Add(nearbyUnit);
                        nearbyCommit.IsInChargeCommitment = true;
                    }
                }
                else
                {
                    EnemyFlankingUnits.Add(nearbyUnit);
                    AssignUnitToFlankingPosition(nearbyUnit, false); //isAllied?
                    Debug.Log(nearbyUnit + " is flanking the allied lines in commitment " + this.name);
                
                    if(!EnemyUnits.Contains(nearbyUnit))
                    {
                        EnemyUnits.Add(nearbyUnit);
                        nearbyCommit.IsInChargeCommitment = true;
                    }
                }

                if(!CombinedUnits.Contains(nearbyUnit))
                {
                    CombinedUnits.Add(nearbyUnit);
                }
            }
        }
    }    

    private void CalculateFrontlineUnitCapacity()
    {
        List<Unit> frontlineAllies = new List<Unit>();
        List<Unit> frontlineEnemies = new List<Unit>();

        foreach(Unit ally in AlliedUnits)
        {
            if(!ally._commitment.IsReserveUnit)
            {
                if(!frontlineAllies.Contains(ally))
                {
                    frontlineAllies.Add(ally);
                }
            }
        }

        foreach(Unit enemy in EnemyUnits)
        {
            if(!enemy._commitment.IsReserveUnit)
            {
                if(!frontlineEnemies.Contains(enemy))
                {
                    frontlineEnemies.Add(enemy);
                }
            }
        }        

        //Trim enemy subunits - requires frontline centre point.
        if(MaxFrontlineSubUnitWidth <= ReturnSubUnitsInUnitList(frontlineEnemies) * FrontlineSubUnitThreshold)
        {
            ReservesortedUnits(EnemyUnits);
        }

        //Trim allied subunits - requires frontline centre point.
        if(MaxFrontlineSubUnitWidth <= ReturnSubUnitsInUnitList(frontlineAllies) * FrontlineSubUnitThreshold)
        {
            ReservesortedUnits(AlliedUnits);         
        }   
    }

    private void ReservesortedUnits(List<Unit> unitlist) 
    {
        if(unitlist == null || unitlist.Count <= 1)
        {
            Debug.LogWarning(this.name + " Trying to remove a unit from frontline, but no units exist in list.");
            return;
        }
       
        List<Unit> sortedUnitsToRemove = new List<Unit>(unitlist);

        foreach(Unit removeProposal in unitlist)
        {
            var unitPosition = removeProposal.UnitPosition;
            // Distance cost
            removeProposal._chargeOrder = DistanceToFrontline(removeProposal.UnitPosition, StartPoint, EndPoint);
        }

        // Sort the list based on ChargeOrder in descending order
        sortedUnitsToRemove.Sort((unit1, unit2) => unit2._chargeOrder.CompareTo(unit1._chargeOrder));

        var unitToRemove = sortedUnitsToRemove[0]; // Get the first element (highest ChargeOrder)
        Debug.Log("Frontline is unbalanced, placing " + unitToRemove + " in Reserve");
        unitToRemove._commitment.IsReserveUnit = true;
    }       

    private void RedefineFrontline()
    {
        int alliedFrontlineSubUnits = ReturnSubUnitsInUnitList(AlliedFrontlineUnits);
        int enemyFrontlineSubUnits = ReturnSubUnitsInUnitList(EnemyFrontlineUnits);
        float alliedDepth = 0f;
        float enemyDepth = 0f;
        
        //Find depth of each frontline unit.
        foreach(Unit ally in AlliedFrontlineUnits)
        {
            alliedDepth += ally.SubUnitDepth;
        }

        foreach(Unit enemy in EnemyFrontlineUnits)
        {
            enemyDepth += enemy.SubUnitDepth;
        }        

        //Get average depth of each frontline
        int totalAlliedDepth = Mathf.CeilToInt(alliedDepth / AlliedFrontlineUnits.Count);
        int totalEnemyDepth = Mathf.CeilToInt(enemyDepth / EnemyFrontlineUnits.Count);

        //Divide each frontline subunit count by average depth.
        float averagedAlliedFrontline = alliedFrontlineSubUnits / totalAlliedDepth;
        float averagedEnemyFrontline = enemyFrontlineSubUnits / totalEnemyDepth;

        // Find larger frontline.
        float maxFrontline = Mathf.Max(averagedAlliedFrontline, averagedEnemyFrontline);
        int finalFrontlineWidth = Mathf.FloorToInt(maxFrontline);

        //TODO: Should average between both frontlines using threshold set in chargecommitment.
        if(MaxFrontlineSubUnitWidth >= finalFrontlineWidth)
        {
            MaxFrontlineSubUnitWidth = finalFrontlineWidth;
        }

        CreateFrontline();   
    }  

    private void RemoveNonViableReserves()
    {
        List<Unit> unitsToRemove = new List<Unit>();
        foreach(Unit unit in CombinedUnits)
        {
            if(unit._commitment.IsReserveUnit && !ViableChargeBehaviour(unit))
            {
                if(!unitsToRemove.Contains(unit))
                {
                    unitsToRemove.Add(unit);                   
                }
            }
        }

        foreach(Unit unitToRemove in unitsToRemove)
        {
            CombinedUnits.Remove(unitToRemove);
            AlliedUnits.Remove(unitToRemove);
            EnemyUnits.Remove(unitToRemove);
            unitToRemove._commitment.RemoveCommitment(unitToRemove);
        }
    }    

    private void PlaceFarUnitsInReserve(float Range)
    {
        foreach(Unit unit in CombinedUnits)
        {
            float dist = DistanceToFrontline(unit.UnitPosition, StartPoint, EndPoint);
            unit._distanceToFrontline = dist;
            if(dist > Range)
            {
                unit._commitment.IsReserveUnit = true;
            }
        }
    }

    private float DistanceToFrontline(Vector3 unitPosition, Vector3 startPoint, Vector3 endPoint)
    {
        Vector3 frontline = endPoint - startPoint;
        
        //Create intersecting line between unit position and line
        float t = Mathf.Clamp01(Vector3.Dot(unitPosition - startPoint, frontline) / frontline.sqrMagnitude);
        Vector3 closestPointOnLine = startPoint + t * frontline;
        
        return Vector3.Distance(unitPosition, closestPointOnLine);
    }   

    private bool IsUnitFlanking(Unit unit, Vector3 startPoint, Vector3 endPoint, bool isAllied)
    {
        Vector3 frontline = endPoint - startPoint; 
        Vector3 toFrontline = unit.UnitPosition - startPoint;

        float dot = Vector3.Dot(toFrontline, isAllied ? - FrontlineFacing : FrontlineFacing);
        if(dot < FlankingDotThreshold)
        {
            unit._commitment.FlankingDot = dot;
            unit._commitment.IsFlanking = true;
            return true;
        }

        unit._commitment.FlankingDot = dot;
        unit._commitment.IsFlanking = false;
        return false;
    }

    private bool ViableChargeBehaviour(Unit unit)
    {
        if(unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Fallback)
        {
            return false;
        }

        return true;
    }

    public BoundingBox GetBoundingBox(float scaleFactor)
    {

        float range = scaleFactor + FrontlineSearchRadius;

        float lineWidth = SearchLengthReduction * GridManager.s_instance.GridSquareSize;
        Vector3 rightToLeft = (StartPoint - EndPoint).normalized;
        Vector3 tolerance = rightToLeft * lineWidth;

        Vector3 left = StartPoint - tolerance;
        Vector3 right = EndPoint + tolerance;
        
        // Create AABB grid based on StartPoint and EndPoint
        Vector3 topLeftCorner = new Vector3(
            Mathf.Min(left.x, right.x) - range, 0f, Mathf.Min(left.z, right.z) - range
        );

        Vector3 bottomRightCorner = new Vector3(
            Mathf.Max(left.x, right.x) + range, 0f, Mathf.Max(left.z, right.z) + range
        );

        Vector3 centre = (topLeftCorner + bottomRightCorner) * 0.5f;
        Vector3 size = bottomRightCorner - topLeftCorner;
        size.y = 1;

        Quaternion facing = Quaternion.LookRotation(FrontlineFacing); 

        // Return the bounding box with frontline search radius
        return new BoundingBox(centre, size, Quaternion.identity);
    }        


    private void DebugDrawFrontlineSlots()
    {
        if(!_debugAllianceFrontlines)
        {
            return;
        }
        
        foreach(AllianceFrontline allianceSlot in AllianceFrontlines)
        {
            var allied = allianceSlot.isAllied; 
            Vector3 alliedStart = allianceSlot.startPoint;
            Vector3 alliedEnd = allianceSlot.endPoint;
            Vector3 toFrontline = Vector3.zero;

            if(allianceSlot.lineValue == 1)
            {
                toFrontline = FrontlineFacing;
                Vector3 tolerance = toFrontline * 1.15f; 
                alliedStart = allied ? allianceSlot.startPoint - tolerance: allianceSlot.startPoint + tolerance;
                alliedEnd = allied ? allianceSlot.endPoint - tolerance : allianceSlot.endPoint + tolerance;
            }
            Debug.DrawLine(alliedStart, alliedEnd, allied ? Color.blue : Color.red, 6f);
        }
    }

    private void DebugDrawFlankingSlots()
    {
        if(!_debugFlankingFrontlines)
        {
            return;
        }
        
        foreach(AllianceFrontline flankSlot in FlankingFrontlines)
        {
            var allied = flankSlot.isAllied; 
            Debug.DrawLine(flankSlot.startPoint, flankSlot.endPoint, allied ? Color.cyan : Color.magenta, 6f);
        }
    }

    private void SetFallbackPosition()
    {
        //Find the frontline for each alliance.
        var alliedFrontline = AllianceFrontlines.Find(AllianceFrontline => AllianceFrontline.isAllied == true && AllianceFrontline.lineValue == 1);
        var enemyFrontline = AllianceFrontlines.Find(AllianceFrontline => AllianceFrontline.isAllied == false && AllianceFrontline.lineValue == 1);
        
        Vector3 left = alliedFrontline.startPoint;
        Vector3 right = alliedFrontline.endPoint;
        Vector3 tolerance = alliedFrontline.lineFacing * (ReserveDistanceFromFrontline / 2);

        //Create new positions for each fronline.
        alliedFrontline.startPoint = left - tolerance;
        alliedFrontline.endPoint = right - tolerance;
        enemyFrontline.startPoint = right + tolerance;
        enemyFrontline.endPoint = left + tolerance; 

        AssignFrontlineSegmentsToUnits(alliedFrontline);
        AssignFrontlineSegmentsToUnits(enemyFrontline);

    }

    private void RetreatUnitsinLull()
    {
        //Find the frontline for each alliance.
        var alliedFrontline = AllianceFrontlines.Find(AllianceFrontline => AllianceFrontline.isAllied == true && AllianceFrontline.lineValue == 1);
        var enemyFrontline = AllianceFrontlines.Find(AllianceFrontline => AllianceFrontline.isAllied == false && AllianceFrontline.lineValue == 1);
        
        //AssignFrontlineSegmentsToUnits(alliedFrontline);
        //AssignFrontlineSegmentsToUnits(enemyFrontline);

        var reserveLines = AllianceFrontlines.FindAll(AllianceFrontline => AllianceFrontline.lineValue > 1);
        
        foreach(var reserveLine in reserveLines)
        {
            RemoveEmptySlots(reserveLine);
        }
        
        CalculateFlankingLines();
        UpdateReserveLine(true);
        UpdateReserveLine(false);
    }


}

