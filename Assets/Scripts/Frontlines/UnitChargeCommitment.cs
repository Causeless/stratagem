using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LinearAlgebra;
using MyBox;
using UnityEngine.UIElements;

public class UnitChargeCommitment : MonoBehaviour
{
    [HideInInspector]
    public Unit _unit;

    [ReadOnly]
    public GameObject FrontlineObject = null;
    [ReadOnly]
    public bool IsChargeCommitmentLeader = false;     
    [ReadOnly]
    public bool PreparedToCharge = false;    
    [ReadOnly]
    public bool IsInChargeCommitment = false;
    [ReadOnly]
    public bool IsReserveUnit = false;
    [ReadOnly]
    public bool ChargeHalted = false;
    [ReadOnly]
    public bool IsReacting = false;
    [ReadOnly]
    public bool IsFlanking = false;   
    [ReadOnly]
    public bool Breakoff = false;
    [ReadOnly] 
    public float FlankingDot = 0f;
    public float ReactionThreshold = 0.5f; 
    public float ReactionCooldown = 30f;
    public float CombatDistance = 1;
    [ReadOnly]
    public float UnitCommitment = 0f;
    public float MinPowerLevelCommitmentMod = 0.2f;
    public float MaxPowerLevelCommitmentMod = 1.8f;
    public float MaxCommitmentModifier = 2.5f;
    public float DebugReduceChargeCommitment = 0f;
    public float AlliedCommitmentJoinRange = 15f;
    [ReadOnly]
    public int FrontlineSubUnitNum = 0;
    [ReadOnly]
    public Vector3 FrontlineCentrePoint;
    [ReadOnly]
    public Unit EnemyCommitmentUnit;
    [ReadOnly]
    public List<Unit> NearbyAlliedUnits;
    [ReadOnly]
    public List<Unit> NearbyEnemyUnits;
    [ReadOnly]
    public List<Unit> ChargeCommitmentUnits;
    [ReadOnly]
    public List<Unit> ChargeCommitmentAlliedUnits;
    [ReadOnly]
    public List<Unit> ChargeCommitmentEnemyUnits;
    [ReadOnly]
    private Vector3 EnemyChargeTarget;
    [ReadOnly]
    private Vector3 AlliedChargeTarget;
    [ReadOnly]
    public List<SubUnit> AlliedFrontlineSubUnits;
    [ReadOnly]
    public List<SubUnit> EnemyFrontlineSubUnits;
    [ReadOnly]
    public float EnemyCommitmentPower;
    [ReadOnly]
    public float AlliedCommitmentPower;

    private void Start()
    {
        _unit = GetComponent<Unit>();
    }
        
    public void CreateChargeCommitment()
    {
        if(IsInChargeCommitment)
        {
            return;
        }
        
        if(_unit.UnitState == Unit.UnitStateMachine.Ready)
        {
            PrepareForCharge();
        }
    }

    private void PrepareForCharge()
    {
        if(PreparedToCharge)
        {
            return;
        }
        
        FindNearbyUnits(_unit.ChargeCommitmentRange);
        UpdateEnemyUnits();
        AddCommitmentLeader();
        IsCommitmentViable();

        if(IsChargeCommitmentLeader)
        {
            SetUnitsAsPrepared();
            UpdateUnitLists();
            SetReserveUnits();
            EnemyCommitmentUnit._commitment.SetReserveUnits();
            StartFrontline();
        }        
    }

    private void StartFrontline()
    {        
        //Define all info needed for Frontline
        CalculateFrontlineCentrePoint(_unit);
        DefineMaxFrontline();
        
        //Create new gameobject named frontline and attach script.
        if(FrontlineObject == null)
        {
            CreateFrontlineGameObject();
        }

        StartChargeCommitment();
    }

    private void StartChargeCommitment()
    {
        StartCharge();
    }

    private void CreateFrontlineGameObject()
    {       
        int ID = UnityEngine.Random.Range(1000, 9999);

        GameObject frontlineObject = new GameObject("Frontline: " + ID);
        Frontline _frontline = frontlineObject.AddComponent<Frontline>();
        Debug.Log(_unit + " Creating Frontline " + ID + " at " + FrontlineCentrePoint);

        if(_frontline.FrontlineInitiated)
        {
           return; 
        }
        
        //Assign commitment info to the frontline.
        frontlineObject.transform.position = FrontlineCentrePoint;
        _frontline.ChargeLeader = _unit;
        _frontline.CentrePoint = FrontlineCentrePoint;
        _frontline.CombinedUnits = new List<Unit>(ChargeCommitmentUnits);
        _frontline.AlliedUnits = new List<Unit>(ChargeCommitmentAlliedUnits);
        _frontline.EnemyUnits = new List<Unit>(ChargeCommitmentEnemyUnits);
        _frontline.MaxFrontlineSubUnitWidth = FrontlineSubUnitNum;
        _frontline.MaxUnits = 100; // Mathf.FloorToInt(FrontlineSubUnitNum / 2f); changing this for now until I think more on if its needed.
        _frontline.AlliedPosition = AlliedChargeTarget;
        _frontline.EnemyPosition = EnemyChargeTarget;

        if(_frontline == null)
        {
            Debug.LogError(frontlineObject.name + " has no script attached!");
            return;
        }
        
        foreach(Unit unit in ChargeCommitmentUnits)
        {
            unit._commitment.FrontlineObject = frontlineObject;
        }
    }

    private void FindNearbyUnits(float range)
    {
        Faction faction = _unit.FriendlyFaction;
        Unit closestUnit = null;
        float closestDistance = Mathf.Infinity;

        // Find all units in range - if in range, divide units into allied and enemy lists.
        List<Unit> nearbyUnits = _unit._grid.GetEntsInRange(_unit.UnitPosition, range);
        if(nearbyUnits == null || nearbyUnits.Count <= 0)
        {
            Debug.LogWarning(_unit + " is trying to initialise charge commitment but there is no units in range. Make sure commitment range is larger than charge range!");
            return;
        }

        foreach(Unit nearbyUnit in nearbyUnits)
        {
            //If allied add to allied list if viable to be added.
            if(nearbyUnit.FriendlyFaction.IsAllied(faction))
            {
                if(UnitViableToCharge(nearbyUnit))
                {

                    if(!NearbyAlliedUnits.Contains(nearbyUnit))
                    {
                        NearbyAlliedUnits.Add(nearbyUnit);
                    }
                }
            }

            if(nearbyUnit.FriendlyFaction.IsHostile(faction))
            {
                if(UnitViableToCharge(nearbyUnit))
                {
                    if(!NearbyEnemyUnits.Contains(nearbyUnit))
                    {
                        NearbyEnemyUnits.Add(nearbyUnit);
                    }

                    //Find nearest enemy unit.
                    float dist = Vector3.Distance(_unit.UnitPosition, nearbyUnit.UnitPosition);
                    if(dist < closestDistance)
                    {
                        closestDistance = dist;
                        closestUnit = nearbyUnit;
                    }
                }
            }
        }

        EnemyCommitmentUnit = closestUnit;
    }

    private void UpdateEnemyUnits()
    {
        if(EnemyCommitmentUnit == null)
        {
            return;
        }


        var EnemyCommit = EnemyCommitmentUnit._commitment;
        EnemyCommit.FindNearbyUnits(EnemyCommitmentUnit.ChargeCommitmentRange);
        NearbyEnemyUnits = EnemyCommit.NearbyAlliedUnits;
        EnemyCommit.NearbyEnemyUnits = NearbyAlliedUnits;
    }

    private void AddCommitmentLeader()
    {
        List<Unit> combinedUnits = new List<Unit>();
        foreach(Unit ally in NearbyAlliedUnits)
        {
            combinedUnits.Add(ally);
        }

        foreach(Unit enemy in NearbyEnemyUnits)
        {
            combinedUnits.Add(enemy);
        }

        foreach(Unit nearbyUnit in combinedUnits)
        {
            if(nearbyUnit._commitment.IsChargeCommitmentLeader)
            {
                if(nearbyUnit != this)
                {
                    IsChargeCommitmentLeader = false;
                    return;
                }
            }
        }

        IsChargeCommitmentLeader = true;
    }

    private void SetUnitsAsPrepared()
    {
        PreparedToCharge = true;

        foreach(Unit ally in NearbyAlliedUnits)
        {
            ally._commitment.PreparedToCharge = true;
        }

        foreach(Unit enemy in NearbyEnemyUnits)
        {
            enemy._commitment.PreparedToCharge = true;    
        }
    }
        
    //Only viable to be added if not committed to another charge.    
    public bool UnitViableToCharge(Unit targetUnit)
    {
        if(targetUnit._commitment.IsInChargeCommitment || targetUnit._commitment.PreparedToCharge)
        {
            return false;
        }

        return true;
    }

    private void IsCommitmentViable()
    {
        //Don't let a unit start a charge if there are no enemies viable to charge.
        if(NearbyEnemyUnits == null || NearbyEnemyUnits.Count <= 0)
        {
            IsChargeCommitmentLeader = false;
        }        
    }

    private void UpdateUnitLists()
    {
        foreach(Unit ally in NearbyAlliedUnits)
        {
            if(!ChargeCommitmentAlliedUnits.Contains(ally))
            {
                ChargeCommitmentAlliedUnits.Add(ally);
            }

            if(!ChargeCommitmentUnits.Contains(ally))
            {
                ChargeCommitmentUnits.Add(ally);
            }            
        }

        foreach(Unit enemy in NearbyEnemyUnits)
        {
            if(!ChargeCommitmentEnemyUnits.Contains(enemy))
            {
                ChargeCommitmentEnemyUnits.Add(enemy);
            }

            if(!ChargeCommitmentUnits.Contains(enemy))
            {
                ChargeCommitmentUnits.Add(enemy);
            }            
        }
    
        foreach(Unit ally in ChargeCommitmentAlliedUnits)
        {
            var allyCommit = ally._commitment;
            allyCommit.ChargeCommitmentUnits = ChargeCommitmentUnits;
            allyCommit.ChargeCommitmentAlliedUnits = ChargeCommitmentAlliedUnits;
            allyCommit.ChargeCommitmentEnemyUnits = ChargeCommitmentEnemyUnits;
        }   

        foreach(Unit enemy in ChargeCommitmentEnemyUnits)
        {
            var enemyCommit = enemy._commitment;
            enemyCommit.ChargeCommitmentUnits = ChargeCommitmentUnits;
            enemyCommit.ChargeCommitmentAlliedUnits = ChargeCommitmentEnemyUnits;
            enemyCommit.ChargeCommitmentEnemyUnits = ChargeCommitmentAlliedUnits;
        }     
    }

    private void SetReserveUnits()
    {
        foreach(Unit ally in ChargeCommitmentAlliedUnits)
        {
            var allyCommit = ally._commitment;

            foreach(Unit targetUnit in ChargeCommitmentAlliedUnits)
            {
                //If any unit is set to hold or fallback but has a charging unit to be placed behind, set unit as reserve.
                if(ally.ChargeBehaviour == Unit.UnitChargeBehaviour.Hold || ally.ChargeBehaviour == Unit.UnitChargeBehaviour.Fallback)
                {
                    if(targetUnit.ChargeBehaviour == Unit.UnitChargeBehaviour.Charge)
                    {
                        allyCommit.IsReserveUnit = true;
                    }
                }
            }

            //If set to fallback but there are hold units to be placed behind, set unit as reserve.
            foreach(Unit targetUnit in ChargeCommitmentAlliedUnits)
            {
                if(ally.ChargeBehaviour == Unit.UnitChargeBehaviour.Fallback)
                {
                    if(targetUnit.ChargeBehaviour == Unit.UnitChargeBehaviour.Hold)
                    {
                        allyCommit.IsReserveUnit = true;
                    }
                }                
            }
        }
    }    

    public void CalculateFrontlineCentrePoint(Unit unit)
    {
        FrontlineCentrePoint = Vector3.zero;
        AlliedChargeTarget = CalculateCommitmentPosition(unit, ChargeCommitmentAlliedUnits);
        EnemyChargeTarget = CalculateCommitmentPosition(unit, ChargeCommitmentEnemyUnits);

        FrontlineCentrePoint = Vector3.Lerp(AlliedChargeTarget, EnemyChargeTarget, 0.5f);

        foreach(Unit commitedUnit in ChargeCommitmentUnits)
        {
            commitedUnit._commitment.FrontlineCentrePoint = FrontlineCentrePoint;
        }
    }    

    private float CalculateChargeCommitment(Unit unit)
    {
        var unitCommit = unit._commitment;

        //Currently just using debug values to test.
        return 0f + unitCommit.DebugReduceChargeCommitment;
    }

    public Vector3 CalculateCommitmentPosition(Unit unit, List<Unit> unitlist) 
    {
        if(unitlist == null || unitlist.Count <= 0)
        {
            Debug.LogWarning("Commitment cannot be found! Postion is set to (0,0,0) " + unit);
            return Vector3.zero;
        }
       
        Vector3 commitmentPosition = Vector3.zero;

        foreach (Unit targetUnit in unitlist)
        {
            commitmentPosition += targetUnit.UnitPosition;
        }

        commitmentPosition /= unitlist.Count;

        return commitmentPosition;
    }    


    private int DefineMaxFrontline()
    {   
        FrontlineSubUnitNum = 0;

        if (ChargeCommitmentEnemyUnits.Count <= 0 
            || ChargeCommitmentEnemyUnits == null)
        {
            Debug.LogWarning(_unit + " Is trying to define the max frontline length but can't find any enemies!");
            return 0;
        }

        Vector3 centrePoint = FrontlineCentrePoint;
        float alliedFrontlineLength = 0;
        float enemyFrontlineLength = 0;

        foreach(Unit ally in ChargeCommitmentAlliedUnits)
        {
            var allyCommit = ally._commitment;
            if(!allyCommit.IsReserveUnit)
            {
                if(ally.IsUnitFrontlineWidth(centrePoint))
                {
                    alliedFrontlineLength += ally.SubUnitWidth; 
                    foreach(SubUnit allySubUnit in ally.SubUnits)
                    {
                        if(!AlliedFrontlineSubUnits.Contains(allySubUnit))
                        {
                            AlliedFrontlineSubUnits.Add(allySubUnit);
                        }
                    }
                    
                }
                else
                {
                    alliedFrontlineLength += ally.SubUnitDepth;
                    foreach(SubUnit allySubUnit in ally.SubUnits)
                    {
                        if(!AlliedFrontlineSubUnits.Contains(allySubUnit))
                        {
                            AlliedFrontlineSubUnits.Add(allySubUnit);
                        }
                    }
                }  
            }
        }

        foreach(Unit enemy in ChargeCommitmentEnemyUnits)
        {
            var enemyCommit = enemy._commitment;
            if(!enemyCommit.IsReserveUnit)
            {
                if(enemy.IsUnitFrontlineWidth(centrePoint))
                {
                    enemyFrontlineLength += enemy.SubUnitWidth; 
                    foreach(SubUnit enemySubUnit in enemy.SubUnits)
                    {
                        if(!EnemyFrontlineSubUnits.Contains(enemySubUnit))
                        {
                            EnemyFrontlineSubUnits.Add(enemySubUnit);
                        }
                    }
                }
                else
                {
                    enemyFrontlineLength += enemy.SubUnitDepth;
                    foreach(SubUnit enemySubUnit in enemy.SubUnits)
                    {
                        if(!EnemyFrontlineSubUnits.Contains(enemySubUnit))
                        {
                            EnemyFrontlineSubUnits.Add(enemySubUnit);
                        }
                    }                    
                } 
            }  
        }

        float length = (((alliedFrontlineLength + enemyFrontlineLength) / 2) + 0.01f);
        FrontlineSubUnitNum = (int)Mathf.Round(length);

        if(FrontlineSubUnitNum >= AlliedFrontlineSubUnits.Count)
        {
            FrontlineSubUnitNum = AlliedFrontlineSubUnits.Count;
        }

        if(FrontlineSubUnitNum >= EnemyFrontlineSubUnits.Count)
        {
            FrontlineSubUnitNum = EnemyFrontlineSubUnits.Count;
        }        

        foreach(Unit committedUnit in ChargeCommitmentUnits)
        {
            committedUnit._commitment.FrontlineSubUnitNum = FrontlineSubUnitNum;
        }

        return FrontlineSubUnitNum;
    }     

    private void StartCharge()
    {               
        if(ChargeCommitmentUnits == null || ChargeCommitmentUnits.Count <= 0)
        {
            Debug.LogError(_unit + " is trying to start a charge but no units are committed to it!");
            return;
        }
        
        foreach(Unit targetUnit in ChargeCommitmentUnits)
        {
            var targetCommit = targetUnit._commitment;
            targetCommit.IsInChargeCommitment = true; //CHARGE!
        }
    }  

    public void RemoveCommitment(Unit unit) //Clears all unit commitment info.
    {
        var unitCommit = unit._commitment;
        unitCommit.IsChargeCommitmentLeader = false;     
        unitCommit.PreparedToCharge = false;    
        unitCommit.IsInChargeCommitment = false;
        unitCommit.IsReserveUnit = false;
        unitCommit.ChargeHalted = false;
        unitCommit.IsReacting = false;
        unitCommit.IsFlanking = false;   

        unitCommit.FrontlineObject = null;
        ClearChargeCommitmentUnits(unit);
        ClearChargeCommitmentSubUnits(unit);
    } 


    //PUSHING FUNCTIONS
    public void SetSubUnitCombatDistance(bool ResetVariance)
    {
        foreach(SubUnit subUnit in _unit.SubUnits)
        {
            float combatDist = 0f;
            // if combat distance is negative, opposing subunit should get pushed back.
            if(subUnit._chargeTarget != null)
            {
                //If not set before, create some variance and set combat distance per subunit.
                if(ResetVariance)
                {
                    float randomDist = UnityEngine.Random.Range(CombatDistance - 1f, CombatDistance + 1f);
                    subUnit._combatDistanceVariance = randomDist - CombatDistance;
                    combatDist = randomDist;
                }
                else
                {
                    combatDist = CombatDistance + subUnit._combatDistanceVariance;
                }
                
                float enemyCombatDist = subUnit._chargeTarget._combatDistance;
                float minDist = enemyCombatDist * -1;
                if(CombatDistance < 0 || minDist > CombatDistance)
                {
                    if(enemyCombatDist < combatDist)
                    {
                        subUnit._combatDistance = minDist;
                    }
                    else
                    {
                        subUnit._chargeTarget._combatDistance = combatDist * -1;
                        subUnit._combatDistance = combatDist;
                    }

                }
                else
                {
                    subUnit._combatDistance = combatDist;
                }
            }
        }
    }


    // OLD FUNCTIONS 
    private void CalculateUnitCommitment(Unit unit) //Need to rethink this.
    {
        var target = unit._commitment;
        float powerdifference = target.AlliedCommitmentPower + (target.AlliedCommitmentPower - target.EnemyCommitmentPower);
        float maxCommitmentPower = target.AlliedCommitmentPower * MaxPowerLevelCommitmentMod;
        float minCommitmentPower = target.AlliedCommitmentPower * MinPowerLevelCommitmentMod;
        target.UnitCommitment = Mathf.InverseLerp(minCommitmentPower, maxCommitmentPower, powerdifference) * MaxCommitmentModifier;
    }  

    public bool IsChargeCommitmentBlocked(Unit unit)
    {
        Vector3 alliedTarget = unit._commitment.AlliedChargeTarget; 
        Vector3 enemyTarget = unit._commitment.EnemyChargeTarget; 
        float distance = FrontlineSubUnitNum * (GridManager.s_instance.GridSquareSize * 0.5f);

        // Get the list of sub-units that fall within the bounding box
        SubUnit subUnit = unit.SubUnits[0];
        List<SubUnit> subUnitsInBox = subUnit._grid.GetEntsInBox(alliedTarget, enemyTarget, distance);

        foreach (SubUnit targetSubUnit in subUnitsInBox)
        {
            Unit subUnitParent = targetSubUnit._parentUnit;

            if (subUnitParent.UnitState == Unit.UnitStateMachine.Combat || subUnitParent._commitment.IsInChargeCommitment)
            {
                return true; // Charge is blocked
            }
        }

        return false; // No blockages found
    }     

    public void ClearChargeCommitmentUnits(Unit unit)
    {       
        var unitCommit = unit._commitment;
        unitCommit.ChargeCommitmentUnits.Clear();
        unitCommit.ChargeCommitmentAlliedUnits.Clear();
        unitCommit.ChargeCommitmentEnemyUnits.Clear();
    } 

    public void ClearChargeCommitmentSubUnits(Unit unit)
    {
        var unitCommit = unit._commitment;
        unitCommit.AlliedFrontlineSubUnits.Clear();
        unitCommit.EnemyFrontlineSubUnits.Clear();
    }    
}
