using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Faction : MonoBehaviour
{
    public Color FactionColour;
    public List<Unit> Units;

    // Key is an enemy subunit, and value is the list of our charge tasks charging that subunit
    public Dictionary<SubUnit, List<SubUnit.ChargeTask>> ChargeAllocations;
    public List<GameObject> Frontlines;
    public Unit_SpatialPartitionGrid FactionUnitGrid;

    public static List<Faction> AllFactions = new List<Faction>();

    void Start()
    {
        Units = new List<Unit>();
        ChargeAllocations = new Dictionary<SubUnit, List<SubUnit.ChargeTask>>();
        Frontlines = new List<GameObject>();
        FactionUnitGrid = this.gameObject.AddComponent<Unit_SpatialPartitionGrid>();

        AllFactions.Add(this);
    }

    // I don't know why the Faction Units list keeps clearing when units are finished spawning? All the debugging tells me its being added correctly? 
    public void UpdateEmptyList() 
    {
        if(Units.Count == 0)
        {
            GameObject unitCollection = GameObject.FindGameObjectWithTag("Unit Collection");
            foreach (Transform child in unitCollection.transform)
            {
                Unit unit = child.GetComponent<Unit>();
                if(!Units.Contains(unit) && unit.FriendlyFaction.IsAllied(this))
                {
                    Units.Add(unit);
                }
            }
        }
    }

    public bool IsHostile(Faction other)
    {
        if(this == other)
        {
            return false;
        }
      
        return true;
    }

    public bool IsAllied(Faction other)
    {
        if(this == other)
        {
            return true;
        }
        
        return false;
    }

    public List<SubUnit.ChargeTask> GetChargeAllocations(SubUnit target)
    {
        if(!ChargeAllocations.ContainsKey(target))
        {
            ChargeAllocations.Add(target, new List<SubUnit.ChargeTask>());
        }

        return ChargeAllocations[target];
    }

    public void AddFrontlines(GameObject frontline)
    {
        if(!Frontlines.Contains(frontline))
        {
            Frontlines.Add(frontline);
        }
    }

    public List<GameObject> GetFrontlines(Unit unit)
    {
        return Frontlines;
    }
}
