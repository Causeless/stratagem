﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectArrowHitSide : MonoBehaviour
{
    //probably can't use this script because its for side detection on a cube collider, but could be inspiration
    //for script that just detects direction from target unit to archer unit minus target unit orientation to see which 
    //side they were hit in
    /************************************************************
         ** Make sure to add rigidbodies to your objects.
         ** Place this script on your object not object being hit
         ** this will only work on a Cube being hit, which means that if later I replace the cubes with animated men they will still have to have a box collider around them for this to work
         ** it does not consider the direction of the Cube being hit
         ************************************************************/


    //I made the entities taller so that the ratio sidehits : tophits would go up
    //I decreased the single entity box collider size from 3,1,3 to 1,1,1, (then scaled it up when I increased the entity height)
    //I don't know if that will have some effect on other scripts though, I imagine it was set that size for a reason?

    //the collider should be at that size so it is easy to click on the unit and select it

    #region Can be removed
    void OnCollisionEnter(Collision collision)//I added non-gravity, yes-kinematic rigidbodies to the projectiles, along with colliders, this may cause them to be non-performant?
    {//originally this script used this oncollision, but i changed it to the trigger, although I'm not sure which is better to use here

        //Debug.Log(ReturnDirection(collision.gameObject, this.gameObject));
        //Debug.Log("collision");
    }
    #endregion



    private void OnTriggerEnter(Collider other)//the trigger is setting off not the collider, still a little bit confused about these and which I should use
    {
        //if (other.GetComponent<Entity>().parentFormation.isEnemy)
        //{
        //    transform.parent.GetComponent<Projectile>().isFlying = false;

        //    //     Debug.Log(ReturnDirection(other.gameObject, this.gameObject));//this is the original line

        //    if (ReturnDirection(other.gameObject, this.gameObject) != HitDirection.None)
        //    {
        //        //parent arrow to object
        //        transform.parent.SetParent(other.transform);
        //    }
        //}


        //Debug.Log(ReturnDirection(this.gameObject, other.gameObject));//but I thought maybe it's supposed to be reversed like this? so that this.gameObject(the arrow) is Object in ReturnDirection
        //and other.gameObject (the entity) is ObjectHit in ReturnDirection? I'm not sure, it's working as it is so probably leave this commented out
        //Debug.Log("trigger");
    }

    private enum HitDirection { None, Top, Bottom, Forward, Back, Left, Right }
    private HitDirection ReturnDirection(GameObject objectWithCollider, GameObject projectile)
    {

        HitDirection hitDirection = HitDirection.None;
        RaycastHit MyRayHit;
        Vector3 direction = (objectWithCollider.transform.position - projectile.transform.position).normalized;
        Ray MyRay = new Ray(projectile.transform.position, direction);

        if (Physics.Raycast(MyRay, out MyRayHit))
        {

            if (MyRayHit.collider != null /*&& Object.name != "Shield"*/)
            {
                //Debug.Log(ObjectHit.name);
                //Debug.Log(Object.name);
                //Debug.Log(Object.transform.GetChild(3).name); //there's no reason to see if you hit the shield or not, because the whole purpose of this script is simply to detect if you hit the
                //front, back, left or right, and then make chance to hit lower on the front and left and higher on the right and especially back, simulating the fact that the shield is on the front and
                //left without having to actually detect the shield. Armor will provide protection all around, but a bit less in the back, whereas shield is lots of protection from arrows on the front
                //a bit less on the left, substantially less on the right and nothing in the back

                Vector3 MyNormal = MyRayHit.normal;
                //MyNormal = MyRayHit.transform.TransformDirection(MyNormal);//if i comment this line out I don't get the problems described
                //in the below comments on the debug.drawrays. I think maybe this line is supposed to do the distinguishing between back and forward or right and left, because it is trying 
                //to reference the transform direction of the object that was hit, but I'm not sure that it is working exactly correctlyl, or even necessary, the script appears to work without it

                //Debug.Log(MyRayHit.transform.forward - MyNormal);//trying to find the side of the cube that was hit by finding the orientation of the collision point to the hit object's forward
                //something like MyRayHit.transform.forward - MyNormal could potentially be another way of figuring out which side was hit.

                Debug.DrawRay(projectile.transform.position, direction, Color.red, 50f);//the vector of the arrow flight upon contact with a collider
                Debug.DrawRay(projectile.transform.position, MyNormal, Color.yellow, 50f);//the normal vector, if the line with MyNormal = MyRayHit.transform.TransformDirection(MyNormal); is not
                //commented out, then judging by these yellow debug rays I can see that if the cube is hit on the 
                //top the normal correctly goes up, but if it is hit on any side, front back left or right, MyNormal will always be in the forward direction, so something is a bit off there i think?
                //but commenting that line out seems to fix it

                if (MyNormal == MyRayHit.transform.up) { hitDirection = HitDirection.Top; }
                if (MyNormal == -1 * MyRayHit.transform.up) { hitDirection = HitDirection.Bottom; }
                if (MyNormal == MyRayHit.transform.forward) { hitDirection = HitDirection.Forward; }
                if (MyNormal == -1 * MyRayHit.transform.forward) { hitDirection = HitDirection.Back; }
                if (MyNormal == MyRayHit.transform.right) { hitDirection = HitDirection.Right; }
                if (MyNormal == -1 * MyRayHit.transform.right) { hitDirection = HitDirection.Left; }
            }
        }
        return hitDirection;
    }
}
