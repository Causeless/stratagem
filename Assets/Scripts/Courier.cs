﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Courier : MonoBehaviour
{
    public SubUnit ParentSubUnit;

    public Unit TargetUnit;
    public Unit.Command TargetUnitCommand;

    public float _orderDistance = 5f;
    /*float timeToGiveOrderMin = 2f;
    float timeToGiveOrderMax = 4f;*/
    public float _timeToGiveOrder = 3f;
    public float _speedBoost = 1.3f;

    private bool _givenOrders = false;
    private NavMeshAgent _agent;
    private Entity _ent;
    private float _originalEntitySpeed;

    // Use this for initialization 
    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _ent = GetComponent<Entity>();

        //_timeToGiveOrder = Random.Range(timeToGiveOrderMin, timeToGiveOrderMax);
        _originalEntitySpeed = _agent.speed;
    }

    // Todo: think about what happens if our subunit dies while we're out giving orders?

    // Update is called once per frame
    void FixedUpdate()
    {
        SubUnit subUnitToFollow = _givenOrders ? ParentSubUnit : TargetUnit.GetPrimarySubUnit();

        var distanceSqr = (_agent.nextPosition - subUnitToFollow.Position).sqrMagnitude;
        var distanceNeededSqr = _orderDistance * _orderDistance;

        _agent.SetDestination(subUnitToFollow.Position);
        _agent.speed = _originalEntitySpeed * _speedBoost;

        if (distanceSqr < distanceNeededSqr)
        {
            if (_givenOrders)
            {
                ParentSubUnit.AddEntity(_ent);

                _agent.speed = _originalEntitySpeed;
                _agent.ResetPath();

                Destroy(this); // Remove courier component from soldier
            }
            else
            {
                StartCoroutine(GiveOrders());
            }
        }
    }

    // This is quite a nice way of doing things, but it's really not very proper, is it?
    // I mean, this probably should work fine, it's just strange. 
    // In future, replace WaitForSeconds with a SimWaitForSeconds that only updates per each fixed tick, so it happens in a more predictable way, I guess.
    IEnumerator GiveOrders()
    {
        _agent.isStopped = true;

        // Disable us so we don't keep calling Update()
        enabled = false;

        // Immediately stop the target unit to give commands - TODO, allow this to also update the unit instead, to queue more waypoints, in the future
        TargetUnit.ClearQueuedCommands();
        TargetUnit.QueueCommand(new Unit.StopCommand());

        // wait for how long it takes to give the order
        yield return new WaitForSeconds(_timeToGiveOrder);

        TargetUnit.QueueCommand(TargetUnitCommand);

        // And return to our unit
        enabled = true;
        _givenOrders = true;
        _agent.isStopped = false;
    }
}
