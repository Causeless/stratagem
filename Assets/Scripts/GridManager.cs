using System.Collections;
using System.Collections.Generic;
using LinearAlgebra;
using Pathfinding;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public float GridSquareSize = 1.0f;
    public int IdleUnitPathPenalty = 30000;
    public int ErosionPathPenalty = 1000;

    // Hmm, maybe this stuff shouldn't be here. a bit of a combo of grid management and pathing... which isn't strictly required to use a grid
    // But. Eh.
    private const int nodeOccupiedByIdleUnitTagIndex = 1;
    private const int nodeOccupiedByCombatUnitTagIndex = 2;
    private const int nodeErosionTagIndex = 4;

    public const int nodeOccupiedByIdleUnitTag = (1 << nodeOccupiedByIdleUnitTagIndex);
    public const int nodeOccupiedByCombatUnitTag =(1 << nodeOccupiedByCombatUnitTagIndex);
    public const int nodeErosionTag =(1 << nodeErosionTagIndex);

    public static GridManager s_instance;

    public static int[] tagPenalties = new int[32];

    [HideInInspector]
    public static Vector2Int InvalidGridSquare = new Vector2Int(int.MaxValue, int.MaxValue);

    void Awake()
    {
        tagPenalties[nodeOccupiedByIdleUnitTagIndex] = IdleUnitPathPenalty;
        tagPenalties[nodeErosionTagIndex] = ErosionPathPenalty;

        s_instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        // Hack because I cbb doing proper rendering yet
        #region DrawGridLines
        
        const int StartDrawDebugLines = -5;
        const int EndDrawDebugLines = 100;

        // rows
        for (int i = StartDrawDebugLines; i < EndDrawDebugLines; i++)
        {
            Vector3 start = new Vector3(i * GridSquareSize, 0.0f, StartDrawDebugLines * GridSquareSize);
            Vector3 end = new Vector3(i * GridSquareSize, 0.0f, EndDrawDebugLines * GridSquareSize);
            //Debug.DrawLine(start, end, Color.yellow);
        }

        // columns
        for (int i = StartDrawDebugLines; i < EndDrawDebugLines; i++)
        {
            Vector3 start = new Vector3(StartDrawDebugLines * GridSquareSize, 0.0f, i * GridSquareSize);
            Vector3 end = new Vector3(EndDrawDebugLines * GridSquareSize, 0.0f, i * GridSquareSize);
            //Debug.DrawLine(start, end, Color.yellow);
        }

        /*for (int x = StartDrawDebugLines; x < EndDrawDebugLines; x++)
        {
            for (int y = StartDrawDebugLines; y < EndDrawDebugLines; y++)
            {
                Vector3 pos = new Vector3(x * GridSquareSize, 0.0f, y * GridSquareSize);
                pos = AliasToGridCorner(pos);
                Debug.DrawLine(pos, pos + Vector3.up, Color.yellow);
            }
        }*/

        #endregion
    }

    private void FixedUpdate()
    {
        UpdateNodeTags();
    }

    private void UpdateNodeTags()
    {
        // Feel really uneasy about this pathing around unmoving subunit stuff.
        // If we want something like that, we seriously need a proper design. We can't just do it from the path initially, as things move.
        // It makes things really very strange...
        // This is broken just now, anyways, as is resets all ground tags to 0 - which we can't do! We need to reset them to their initial state
        // And we're not double-buffering this, so... eh.
        return;

#pragma warning disable 0162
        System.Action<GraphNode, SubUnit.SubUnitState> updateNodeTags = (node, state) =>
        {
            switch (state)
            {
                case SubUnit.SubUnitState.Idle:
                    if (node.Tag == nodeOccupiedByCombatUnitTag)
                    {
                        return; // combat overrides idle
                    }
                    node.Tag = nodeOccupiedByIdleUnitTag;
                    break;
                case SubUnit.SubUnitState.Combat:
                    node.Tag = nodeOccupiedByCombatUnitTag;
                    break;
                default:
                    break; // Do nothing
            }
        };

        GameObject subUnits = GameObject.FindWithTag("SubUnit Collection");

        // Update the pathfinding graph so that subunits add a tag to the grid square that they stand on
        AstarPath.active.AddWorkItem(
            new AstarWorkItem(() => {
                var gg = AstarPath.active.data.gridGraph;

                // Clear all tags
                gg.GetNodes(node => node.Tag = 0);

                foreach (SubUnit subUnit in subUnits.GetComponentsInChildren<SubUnit>())
                {
                    var node = AstarPath.active.GetNearest(subUnit.Position).node;
                    updateNodeTags(node, subUnit.State);
                    //node.GetConnections(connectionNode => { updateNodeTags(connectionNode, subUnit.State); });
                }
            },
            force => { return true; })
        );
#pragma warning restore 0162
    }

    public Vector2Int GetGridSquareCoordinatesForPosition(Vector3 pos)
    {
        Vector3 aliased = pos / GridSquareSize;
        aliased.x = Mathf.Floor(aliased.x);
        aliased.z = Mathf.Floor(aliased.z);
        return new Vector2Int((int)aliased.x, (int)aliased.z);
    }

    public Vector3 GetNonRoundedGridSquareCoordinatesForPosition(Vector3 pos)
    {
        Vector3 aliased = pos / GridSquareSize;
        return new Vector3(aliased.x, pos.y, aliased.z);
    }

    public Vector3 AliasToGridCentre(Vector3 pos)
    {
        Vector3 aliased = pos / GridSquareSize;
        aliased.x = Mathf.Floor(aliased.x);
        aliased.y = Mathf.Floor(aliased.y);
        aliased.z = Mathf.Floor(aliased.z);
        aliased.x += 0.5f;
        aliased.z += 0.5f;
        return aliased * GridSquareSize;
    }

    public Vector3 AliasToGridCorner(Vector3 pos)
    {
        Vector3 aliased = pos / GridSquareSize;
        aliased.x = Mathf.Floor(aliased.x + 0.5f);
        aliased.y = Mathf.Floor(aliased.y + 0.5f);
        aliased.z = Mathf.Floor(aliased.z + 0.5f);
        return aliased * GridSquareSize;
    }

    public List<LineSegment> AliasToGridCorners(LineSegment segment, int minimumSegmentCount = -1)
    {
        List<LineSegment> lineSegments = new List<LineSegment>();

        // There's an off by one error otherwise... I ought to understand this better, but I'm tired.
        minimumSegmentCount++;

        LineSegment adjustedSegment = segment;

        Vector3 firstCoords = GetNonRoundedGridSquareCoordinatesForPosition(adjustedSegment.firstPos);
        Vector3 lastCoords = GetNonRoundedGridSquareCoordinatesForPosition(adjustedSegment.lastPos);

        Vector3 coordDifference = lastCoords - firstCoords;
        coordDifference.x = Mathf.Abs(coordDifference.x);
        coordDifference.z = Mathf.Abs(coordDifference.z);

        float coordStepsFlt = Mathf.Max(coordDifference.x, coordDifference.z);
        int coordSteps = (int)System.Math.Round(coordStepsFlt + 0.5f);;

        Vector3 pointDifferenceStep = (adjustedSegment.lastPos - adjustedSegment.firstPos) / coordStepsFlt;
        if (coordSteps < minimumSegmentCount)
        {
            adjustedSegment.lastPos += pointDifferenceStep * (minimumSegmentCount - coordSteps);
            coordSteps = minimumSegmentCount;
        }

        Vector3 currentPoint = AliasToGridCorner(segment.firstPos);
        for (int i = 1; i < coordSteps; i++)
        {
            Vector3 newPoint = AliasToGridCorner(segment.firstPos + pointDifferenceStep*i);

            LineSegment fromCurrentToNew = new LineSegment();
            fromCurrentToNew.firstPos = currentPoint;
            fromCurrentToNew.lastPos = newPoint;

            //Debug.DrawLine(currentPoint, newPoint);

            lineSegments.Add(fromCurrentToNew);

            currentPoint = newPoint;
        }

        return lineSegments;
    }
}
