using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BoundingBoxRenderer : MonoBehaviour
{
    // Allow devs to enable visualisation through the inspector.
    public bool VisualizeBoundingBoxes = false;  
    
    private GameObject _boundingBoxVisual;

    private BoundingBox _currentBoundingBox;

    public void InitializeBoundingBoxVisual()
    {
        if (_boundingBoxVisual == null)
        {
            _boundingBoxVisual = GameObject.CreatePrimitive(PrimitiveType.Cube);
            Destroy(_boundingBoxVisual.GetComponent<Collider>()); // Remove collider
            Renderer renderer = _boundingBoxVisual.GetComponent<Renderer>();
            renderer.material = new Material(Shader.Find("Unlit/Color"));
        }
        _boundingBoxVisual.SetActive(false); // Start inactive
    }

    public void UpdateBoundingBoxRender(Unit unit, Frontline frontline, float scaleFactor, Color color)
    {
        if (!VisualizeBoundingBoxes)
        {
            if (_boundingBoxVisual != null)
            {
                _boundingBoxVisual.SetActive(false);
            }
            return;
        }

        if (_boundingBoxVisual == null)
        {
            InitializeBoundingBoxVisual();
        }

        if(unit != null)
        {
            // Get the bounding box from the unit
            _currentBoundingBox = unit.GetBoundingBox(scaleFactor);
        }
        if(frontline != null)
        {
            _currentBoundingBox = frontline.GetBoundingBox(scaleFactor);
        }
        if(unit == null && frontline == null)
        {
            Debug.LogError("Bounding Box trying to render but all inputs are null");
            return;
        }

        // Update the existing bounding box visual
        _boundingBoxVisual.transform.position = _currentBoundingBox.center;
        _boundingBoxVisual.transform.rotation = _currentBoundingBox.rotation;
        _boundingBoxVisual.transform.localScale = _currentBoundingBox.size;
        _boundingBoxVisual.GetComponent<Renderer>().material.color = color;

        _boundingBoxVisual.SetActive(true); 
    }

    public BoundingBox GetCurrentBoundingBox()
    {
        return _currentBoundingBox;
    }
}