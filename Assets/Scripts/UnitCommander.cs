﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.AI;

public class UnitCommander : MonoBehaviour {
    public int MinimumMen = 4;

    public float ShoutImmediateRadius = 20.0f;
    public float ShoutMaxRadius = 50.0f;
    public float ShoutDelayAtMaxRadius = 5.0f;

    public float CourierDistanceToUnitToGiveCommand = 5.0f;
    public float CourierSpeedBoost = 1.3f;
    public float CourierTimeToGiveOrder = 3.0f;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private SubUnit GetSubUnitWithAvailableEntities()
    {
        foreach(SubUnit subUnit in GetComponent<Unit>().SubUnits)
        {
            if (CanSubUnitSendCourier(subUnit))
            {
                return subUnit;
            }
        }
        return null;
    }

    public bool CanSendCourier()
    {
        return GetSubUnitWithAvailableEntities() != null;
    }

    private bool CanSubUnitSendCourier(SubUnit subUnit)
    {
        return subUnit.Entities.Count > MinimumMen;
    }

    // Returns a bool for success
    public bool CommandUnit(Unit unit, Unit.Command targetUnitCommand)
    {
        if (unit.FriendlyFaction != GetComponent<Unit>().FriendlyFaction)
        {
            // Trying to command a unit that's not in our alliance...
            Assert.IsTrue(false);
            return false;
        }

        SubUnit subUnit = GetSubUnitWithAvailableEntities();
        if (subUnit == null)
        {
            return false;
        }

        float distance = (unit.GetPrimarySubUnit().Position - subUnit.Position).magnitude;

        if (distance > ShoutMaxRadius)
        {
            return SendCourier(unit, targetUnitCommand, subUnit);
        }
        else
        {
            float delayMultiplier = (distance - ShoutImmediateRadius) / (ShoutMaxRadius - ShoutImmediateRadius);
            delayMultiplier = Mathf.Clamp(delayMultiplier, 0.0f, 1.0f);

            StartCoroutine(ShoutOrders(unit, targetUnitCommand, ShoutDelayAtMaxRadius * delayMultiplier));
            return true;
        }
    }

    IEnumerator ShoutOrders(Unit unit, Unit.Command targetUnitCommand, float delayInSeconds)
    {
        // Immediately stop the target unit to give commands - TODO, allow this to also update the unit instead, to queue more waypoints, in the future
        unit.ClearQueuedCommands();
        unit.QueueCommand(new Unit.StopCommand());

        // wait for how long it takes to give the order
        yield return new WaitForSeconds(delayInSeconds);

        unit.QueueCommand(targetUnitCommand);
    }

    bool SendCourier(Unit targetUnit, Unit.Command targetUnitCommand, SubUnit sourceSubUnit)
    {
        Entity ent = GetClosestEntity(sourceSubUnit, targetUnit.GetPrimarySubUnit().Position);

        if (!CanSubUnitSendCourier(sourceSubUnit) || ent == null)
        {
            return false;
        }

        sourceSubUnit.RemoveEntity(ent);

        var courier = ent.gameObject.AddComponent<Courier>();

        courier._orderDistance = CourierDistanceToUnitToGiveCommand;
        courier._speedBoost = CourierSpeedBoost;
        courier._timeToGiveOrder = CourierTimeToGiveOrder;

        courier.ParentSubUnit = sourceSubUnit;
        courier.TargetUnit = targetUnit;
        courier.TargetUnitCommand = targetUnitCommand;

        return true;
    }

    public Entity GetClosestEntity(SubUnit subUnit, Vector3 position)
    {
        Entity closest = null;
        float minDistSqr = Mathf.Infinity;

        // Get distance to first corner that isn't directly on us
        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(subUnit.Position, position, NavMesh.AllAreas, path);
        if (path.status != NavMeshPathStatus.PathComplete)
        {
            // There's no path to them...
            return closest;
        }

        position = path.corners[1]; // the first corner that doesn't lie directly on us (so, the second corner)

        for (int i = 0; i < subUnit.Entities.Count; i++)
        {
            var ent = subUnit.Entities[i];

            float distSqr = (ent.GetPosition() - position).sqrMagnitude;
            if (distSqr < minDistSqr)
            {
                closest = ent;
                minDistSqr = distSqr;
            }
        }

        return closest;
    }
}
