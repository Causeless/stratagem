﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationSpot
{
    public Vector3 position;
    public Vector3 direction;
    public string animationOverride;
}

// A formation represents a list of points which a subunit uses to place its entities
public abstract class Formation : MonoBehaviour 
{
    public string _name = "Default";

    public List<FormationSpot> Spots = new List<FormationSpot>();

    public float _horizontalSpacing = 1f;
    public float _verticalSpacing = 1f;

    protected LinearAlgebra.LineSegment _line;
    protected Vector3 _averagePosition;

    public bool CanUseFormationDuringCharge = true;
    public bool CanUseFormationInCombat = true;
    public bool CanRunWhileInFormation = true;
    public float MinimumDisciplineToSustainFormation = -1f;

    public string AnimationOverride = "";

    public LinearAlgebra.LineSegment Line {
        get {
            return _line;
        }
    }

    public Vector3 Centre {
        get {
            return _averagePosition;
        }
    }

    public abstract List<FormationSpot> GetSpotsForLine(LinearAlgebra.LineSegment line, int count);
    public abstract void SetForLine(LinearAlgebra.LineSegment line, int count);

    protected void RecalculateCentre()
    {
        _averagePosition = Vector3.zero;
        var groupSize = 0;

        for (int i = 0; i < Spots.Count; i++)
        {
            if (Spots[i] != null)//might not be necessary
            {
                _averagePosition += Spots[i].position;
                groupSize++;
            }
        }

        if (groupSize != 0)
        {
            _averagePosition /= groupSize;
        }
    }
}