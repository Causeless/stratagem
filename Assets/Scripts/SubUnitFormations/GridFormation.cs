﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MyBox;

public class GridFormation : Formation
{
    public string FrontRowAnimationOverride = "";

    public int _minimumColumns = 1;
    public int _minimumRows = 1;

    // Used for testudo, where the front row needs to be closer to look nicer
    // Wee bit hacky, but eh. Can't think of a more elegant solution for now
    public bool _overrideFrontRowVerticalSpacing = false;

    [ConditionalField(nameof(_overrideFrontRowVerticalSpacing))]
    public float _frontRowVerticalSpacing = 1f;

    public float MaxFormationWidth {
        get {
            return Spots.Count * _horizontalSpacing / _minimumRows;
        }
    }

    private Vector3 PositionAtTerrainHeight(Vector3 pos)
    {
        //cast ray to get terrain height
        RaycastHit terrainHit;
        Vector3 rayOrigin = new Vector3(pos.x, pos.y + 1.0f, pos.z);
        Ray terrainRay = new Ray(rayOrigin, Vector3.down);

        if (Physics.Raycast(terrainRay, out terrainHit, 2.0f))
        {
            return new Vector3(pos.x, terrainHit.point.y, pos.z);
        }

        NavMeshHit navHit;
        return NavMesh.SamplePosition(rayOrigin, out navHit, 5.0f, NavMesh.AllAreas) ? navHit.position : pos;
    }

    public override List<FormationSpot> GetSpotsForLine(LinearAlgebra.LineSegment line, int count)
    {
        var spots = new List<FormationSpot>(count);
        if (count == 0)
        {
            return spots;
        }

        Vector3 directionAlongFront = line.GetDirection();

        Vector3 backwards = line.GetNormalVector();
        Vector3 forwards = backwards * -1;

        float length = line.GetLength();
        int spotsPerRow = Mathf.CeilToInt(length / _horizontalSpacing);
        spotsPerRow = Mathf.Clamp(spotsPerRow, _minimumColumns, count / _minimumRows);

        float realLength = _horizontalSpacing * spotsPerRow;
        float difference = length - realLength;

        // Offset so that the gap is equally distributed at the left and right edges of the formation, not just one side
        Vector3 offset = (directionAlongFront * difference * 0.5f) + (directionAlongFront * _horizontalSpacing * 0.5f);
        int row = 0;

        // While there are enough soldiers left for another complete row
        while (count >= spotsPerRow)
        {
            float frontVerticalSpacingOffet = 0f;
            if (_overrideFrontRowVerticalSpacing && row > 0)
            {
                frontVerticalSpacingOffet = _verticalSpacing - _frontRowVerticalSpacing;
            }

            for (int column = 0; column < spotsPerRow; column++)
            {
                count--;

                Vector3 currentPos = line.firstPos + offset;
                currentPos += directionAlongFront * _horizontalSpacing * column; // Add column spacing
                currentPos += backwards * _verticalSpacing * row; // Add row spacing

                // If not first row, offset
                currentPos -= backwards * frontVerticalSpacingOffet;

                FormationSpot currentSpot = new FormationSpot();
                currentSpot.position = PositionAtTerrainHeight(currentPos);
                currentSpot.direction = forwards;
                currentSpot.animationOverride = row == 0 ? FrontRowAnimationOverride : AnimationOverride;

                spots.Add(currentSpot);
            }
            row++;
        }

        // Now we must put the rest of the soldiers on the back row

        // Used to centre back row
        float backrowSlideAmount = (spotsPerRow - count) * 0.5f;
        Vector3 backrowSlide = directionAlongFront * backrowSlideAmount * _horizontalSpacing;

        for (int column = 0; column < count; column++)
        {
            Vector3 currentPos = line.firstPos + offset;
            currentPos += (directionAlongFront * _horizontalSpacing * column) + backrowSlide; // Add column spacing
            currentPos += backwards * _verticalSpacing * row; // Add row spacing

            FormationSpot currentSpot = new FormationSpot();
            currentSpot.position = PositionAtTerrainHeight(currentPos);
            currentSpot.direction = forwards;
            currentSpot.animationOverride = row == 0 ? FrontRowAnimationOverride : AnimationOverride;

            spots.Add(currentSpot);
        }

        return spots;
    }

    public override void SetForLine(LinearAlgebra.LineSegment line, int count)
    {
        if (line == _line && count == Spots.Count) // Optimization to simplify unit code without unnecessary recalculation
        {
            //return;
        }

        _line = line;
        Spots = GetSpotsForLine(line, count);

        RecalculateCentre();
    }
}