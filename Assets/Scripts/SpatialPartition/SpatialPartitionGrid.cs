﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LinearAlgebra;

public interface ICachedPosition
{
    Vector3 GetPosition();
    Vector3 GetOldPosition();
}

// Todo: maybe test performance compared to an intrusive linked list instead of HashSets
public class SpatialPartitionGrid<T> : MonoBehaviour where T : MonoBehaviour, ICachedPosition
{
    public int cellCount = 4096;
    public float cellSize = 5.0f;
    private float _invCellSize;

    private HashSet<T>[] _cells;

    // Primes used for hashing
    const int p1 = 5101;
    const int p2 = 10093;

    public SpatialPartitionGrid()
    {
        _invCellSize = 1f / cellSize;
        _cells = new HashSet<T>[cellCount];

        for (int i = 0; i < cellCount; i++)
        {
            _cells[i] = new HashSet<T>();
        }
    }

    void Awake()
    {

	}
	
	public void Add(T ent)
    {
        int i = GetCellForPosition(ent.GetOldPosition());
        _cells[i].Add(ent);
    }

    public void Remove(T ent)
    {
        // Remove from both grid squares, a little more expensive but safer
        int oldCell = GetCellForPosition(ent.GetOldPosition());
        int newCell = GetCellForPosition(ent.GetPosition());
        _cells[oldCell].Remove(ent);
        _cells[newCell].Remove(ent);
    }

    public void Move(T ent)
    {
        int oldCell = GetCellForPosition(ent.GetOldPosition());
        int newCell = GetCellForPosition(ent.GetPosition());

        if (oldCell == newCell)
        {
            return;
        }

        // Remove from old cell and add back to grid at new cell
        _cells[oldCell].Remove(ent);
        _cells[newCell].Add(ent);
    }

    public int GetCellForPosition(Vector3 position)
    {
        Vector3 positionCell = position * _invCellSize;
        int cellX = (int)positionCell.x;
        int cellY = (int)positionCell.z;

        int hashCode = cellX * p1 + cellY * p2;
        if (hashCode < 0)
        {
            hashCode *= -1;
        }

        return hashCode % cellCount;
    }

    public int GetCellForCellCoords(int cellX, int cellY)
    {
        int hashCode = cellX * p1 + cellY * p2;
        if (hashCode < 0)
        {
            hashCode *= -1;
        }

        return hashCode % cellCount;
    }

    public List<T> GetEntsInRange(Vector3 position, float radius)
    {
        List<T> entList = new List<T>();
        float radiusSqr = radius * radius;

        // Construct AABB to select cells
        Vector3 topLeftCorner = position + new Vector3(-radius, 0f, -radius);
        Vector3 bottomRightCorner = position + new Vector3(radius, 0f, radius);

        Vector3 topLeftCornerCell = topLeftCorner * _invCellSize;
        int topLeftCellX = (int)topLeftCornerCell.x;
        int topLeftCellY = (int)topLeftCornerCell.z;

        Vector3 bottomRightCornerCell = bottomRightCorner * _invCellSize;
        int bottomRightCellX = (int)bottomRightCornerCell.x;
        int bottomRightCellY = (int)bottomRightCornerCell.z;

        // Add all surrounding cells to list
        for (int x = topLeftCellX; x <= bottomRightCellX; x++)
        {
            for (int y = topLeftCellY; y <= bottomRightCellY; y++)
            {
                foreach(T ent in _cells[GetCellForCellCoords(x, y)])
                {
                    if ((ent.GetPosition() - position).sqrMagnitude < radiusSqr)
                        entList.Add(ent);
                }
            }
        }

        return entList;
    }

    public List<T> GetEntsInBox(Vector3 startPos, Vector3 endPos, float range)
    {
        List<T> entList = new List<T>();

        // Construct AABB to select cells
        Vector3 topLeftCorner = new Vector3(
            Mathf.Min(startPos.x, endPos.x) - range, 0f, Mathf.Min(startPos.z, endPos.z) - range
        );

        Vector3 bottomRightCorner = new Vector3(
            Mathf.Max(startPos.x, endPos.x) + range, 0f, Mathf.Max(startPos.z, endPos.z) + range
        );

        Vector3 topLeftCornerCell = topLeftCorner * _invCellSize;
        int topLeftCellX = (int)topLeftCornerCell.x;
        int topLeftCellY = (int)topLeftCornerCell.z;

        Vector3 bottomRightCornerCell = bottomRightCorner * _invCellSize;
        int bottomRightCellX = (int)bottomRightCornerCell.x;
        int bottomRightCellY = (int)bottomRightCornerCell.z;

        // Add all surrounding cells to list
        for (int x = topLeftCellX; x <= bottomRightCellX; x++)
        {
            for (int y = topLeftCellY; y <= bottomRightCellY; y++)
            {
                foreach(T ent in _cells[GetCellForCellCoords(x, y)])
                {
                    Vector3 entPosition = ent.GetPosition();
                    if (entPosition.x >= topLeftCorner.x && entPosition.x <= bottomRightCorner.x &&
                        entPosition.z >= topLeftCorner.z && entPosition.z <= bottomRightCorner.z)
                    {
                        entList.Add(ent);
                    }
                }
            }
        }

        return entList;
    }
}
