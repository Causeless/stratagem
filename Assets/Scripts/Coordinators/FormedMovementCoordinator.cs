﻿using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using LinearAlgebra;

public class FormedMovementCoordinator : Coordinator
{
    private int[] _assignment;
    private float _oldFormationWidth;
    private Formation _oldFormation;

    // We can't just use the subunit transform rotation, because the formation is aligned in world-space
    // So effectively we store a unit-space rotation and map from the subunit transform onto that rotation
    protected Quaternion _targetRotation;

    protected Vector3 _pivot;
    
    // a bit of a hack; I need to think about a nicer way of doing this..
    // Assignment should perhaps not be part of coordinator, but seperated out
    private int _oldSoldierCount = -1;

    protected override void UpdateCoordinator()
    {
        UpdateFormationRotation();
    }

    protected override void CoordinateEntity(Entity e, Vector3 destination)
    {
        var desiredPosition = GetCurrentDesiredLocationForEndLocation(destination);

        var formationDiff = desiredPosition - e.GetPosition();
        var toFormation = formationDiff;
        if (toFormation.sqrMagnitude > 1.0f)
        {
            toFormation.Normalize();
        }

        var endLocationDiff = GetRotatedFormationPoint(destination, e._parentSubUnit.GetMovementGoalPosition()) - e.GetPosition();
        var toEndLocation = endLocationDiff;
        if (toEndLocation.sqrMagnitude > 1.0f)
        {
            toEndLocation.Normalize();
        }

        float unitSpeed = e._parentSubUnit.CurrentMovementSpeed;

        // Move into formation
        Vector3 formationMovement = toFormation * e._parentSubUnit._subUnitOrder;

        // Move towards target
        Vector3 destinationMovement = toEndLocation * unitSpeed;
        destinationMovement = Vector3.ClampMagnitude(destinationMovement, endLocationDiff.magnitude);

        e._navMeshAgent.velocity = formationMovement + destinationMovement;
    }

    private Vector3 GetCurrentDesiredLocationForEndLocation(Vector3 endLocation, Vector3 pivot)
    {
        return GetRotatedFormationPoint(endLocation + (_subUnit.Position - GetFormation().Centre), pivot);
    }

    private Vector3 GetCurrentDesiredLocationForEndLocation(Vector3 endLocation)
    {
        return GetCurrentDesiredLocationForEndLocation(endLocation, _pivot);
    }

    private void UpdateAssignment()
    {
        // First, ensure we clear all animations so units don't stay in the wrong anim
        for (int j = 0; j < _subUnit.Entities.Count; j++)
        {
            var ent = _subUnit.Entities[j];
            ent.ClearAnimState();
        }

        float[,] multiDimensionalArrayOfAllListsOfPossibleRoutes = new float[_subUnit.Entities.Count, GetFormation().Spots.Count];

        for (int j = 0; j < _subUnit.Entities.Count; j++)
        {
            for (int i = 0; i < GetFormation().Spots.Count; i++)
            {
                Vector3 desiredPosition = GetCurrentDesiredLocationForEndLocation(GetFormation().Spots[i].position);
                float dist = (_subUnit.Entities[j].GetPosition() - desiredPosition).sqrMagnitude;

                if (float.IsNaN(dist))
                {
                    Assert.IsFalse(true);
                    return;
                }

                multiDimensionalArrayOfAllListsOfPossibleRoutes[j, i] = dist;
            }
        }

        TheHungarianAlgorithm hungarianAlgorithm = new TheHungarianAlgorithm(multiDimensionalArrayOfAllListsOfPossibleRoutes);
        _assignment = hungarianAlgorithm.Run();
    }

    private void UpdateEntityDestinations()
    {
        for (int j = 0; j < _subUnit.Entities.Count; j++)
        {
            var ent = _subUnit.Entities[j];

            FormationSpot destination = GetFormation().Spots[_assignment[j]];

            ent._lookRotation = Quaternion.LookRotation(destination.direction, Vector3.up);
            ent._finalDestination = destination.position;
            ent._animOverride = destination.animationOverride;
        }
    }

    protected override void ReassignEntities()
    {
        float newFormationWidth = GetFormation().Line.GetLength();

        float widthDifference = Math.Abs(_oldFormationWidth - newFormationWidth);
        if (widthDifference > 0.25f || // If our formation is wider or thinner than before... (on 0.25f - just a magic number. In future, perhaps base this off of the formation spacing instead)
            _subUnit.Entities.Count != _oldSoldierCount || // Or one of our soldiers died
            _oldFormation != GetFormation()) // or formation changed
        {
            UpdateFormationRotation();
            UpdateAssignment();
        }

        _oldFormation = GetFormation();
        _oldFormationWidth = newFormationWidth;
        _oldSoldierCount = _subUnit.Entities.Count;

        UpdateEntityDestinations();
    }

    public override LineSegment GetFrontLine()
    {
        LineSegment frontLine;
        frontLine.firstPos = GetCurrentDesiredLocationForEndLocation(GetFormation().Line.firstPos);
        frontLine.lastPos = GetCurrentDesiredLocationForEndLocation(GetFormation().Line.lastPos);

        return frontLine;
    }

    private void UpdateFormationRotation()
    {
        if (GetFormation() == null)
        {
            return;
        }

        var frontLine = GetFrontLine();
        var closestPointOnFrontLine = frontLine.ClosestPoint(GetFormation().Line);

        // Instead of wheeling around our centre, wheel around our closest point to our destination (typically our closest corner)
        // Why are we rotating by this particular point mirrored around the unit's average position? 
        // I'm not entirely sure.
        // I was about to sleep when I thought of trying this and it actually works really well.
        // I believe it may be something to do with the fact that our default rotation is actually the ultimate direction we'll be facing,
        // And so a fully applied rotation of 1 doesn't mean we've rotated into position: it instead means we haven't started rotating to our final destination line's orientation yet
        // Due to this, it made some sense to mirror the pivot point in the same way that the rotation amount is "mirrored"
        // Whether this is the actual reason for why this works well, or if it's just a coincidence, I'm not certain.
        _pivot = closestPointOnFrontLine + (_subUnit.Position - closestPointOnFrontLine);

        // Only use rotation if we are far enough from our final position to justify it
        float distanceToStartWheelingSqr = 4.0f;
        distanceToStartWheelingSqr *= distanceToStartWheelingSqr;

        float distanceSqr = (GetFormation().Centre - _subUnit.Position).sqrMagnitude;

        Vector3 targetDirection = distanceSqr < distanceToStartWheelingSqr ? GetEndDirection() : GetTravelDirection(); 

        float angle = Vector3.SignedAngle(_subUnit.transform.forward, targetDirection, Vector3.up);
        
        // If it's an extreme angle, then just reassign instead of wheeling
        const float angleLimit = 75.0f;
	    if (angle > angleLimit || angle < -angleLimit)
        {
            _subUnit.transform.rotation = Quaternion.LookRotation(targetDirection);
            _targetRotation = Quaternion.FromToRotation(GetFormation().Line.GetNormalVector(), _subUnit.transform.forward);
            UpdateAssignment();
            UpdateEntityDestinations();
            return;
        }

        float rotateSpeed = 15.0f * (MathF.PI/180f);

        Vector3 newFacing = Vector3.RotateTowards(_subUnit.transform.forward, targetDirection, rotateSpeed * Time.fixedDeltaTime, 0.0f);
        newFacing.y = 0.0f;

        _subUnit.transform.rotation = Quaternion.LookRotation(newFacing);

        // Map from world-space to a local rotation relative to our transform
        _targetRotation = Quaternion.FromToRotation(GetFormation().Line.GetNormalVector(), _subUnit.transform.forward);
    }

    // This function is what we can use to rotate positions by formation rotation
    public Vector3 GetRotatedFormationPoint(Vector3 point, Vector3 pivot)
    {
        return _targetRotation * (point - pivot) + pivot;
    }

    public Vector3 GetTravelDirection()
    {
        Vector3 towardsFinalPosition = _subUnit.Position - GetFormation().Centre;
        towardsFinalPosition.y = 0.0f;
        towardsFinalPosition.Normalize();
        return towardsFinalPosition;
    }

    public Vector3 GetEndDirection()
    {
        Vector3 forwardFacingVector = GetFormation().Line.GetNormalVector();
        forwardFacingVector.y = 0.0f;
        return forwardFacingVector;
    }

    public float GetAngleTowardsTargetLine()
    {
        return Vector3.SignedAngle(GetTravelDirection(), GetEndDirection(), Vector3.up);
    }
}