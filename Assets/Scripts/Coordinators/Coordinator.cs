﻿using UnityEngine;
using LinearAlgebra;

// A coordinator figures out how soldiers in a unit should move to achieve a given formation
public abstract class Coordinator : MonoBehaviour {
    protected SubUnit _subUnit;
    protected Entity_SpatialPartitionGrid _grid;

    // For randomness depending on unit discipline
    private Vector3[] RandomOffsetArray;

    private void Awake()
    {
        _subUnit = GetComponent<SubUnit>();
        _grid = GameObject.FindWithTag("SpatialPartitionGrid").GetComponent<Entity_SpatialPartitionGrid>();
    }

    private void Start()
    {   
        InitRandomness();
    }

    public Formation GetFormation()
    {
        return _subUnit.GetFormation();
    }

    private void InitRandomness()
    {
        RandomOffsetArray = new Vector3[_subUnit.Entities.Count];
        
        for (int i = 0; i < _subUnit.Entities.Count; i++)
        {
            Vector2 randomOffset = UnityEngine.Random.insideUnitCircle;
            RandomOffsetArray[i] = new Vector3(randomOffset.x, 0f, randomOffset.y);
        }   
    }

    private void FixedUpdate()
    {
        UpdateCoordinator();

        // In future, maybe scale differently on different axis
        Formation formation = GetFormation();
        float scaledRandomOffsetAmount = (formation._horizontalSpacing + formation._verticalSpacing) * 0.5f;

        for (int i = 0; i < _subUnit.Entities.Count; i++)
        {
            Entity e = _subUnit.Entities[i];

            // Add offset to expected position for each entity. This offset will get bigger when discipline is lower.
            float randomRadius = _subUnit._subUnitRandomness * scaledRandomOffsetAmount; 
            Vector3 randomOffset = RandomOffsetArray[i % RandomOffsetArray.Length] * randomRadius;

            Vector3 destination = e._finalDestination + randomOffset;
            CoordinateEntity(e, destination);
        }
    }


    public void ResetFormation()
    {
        ReassignEntities();
    }

    public abstract LineSegment GetFrontLine();

    protected abstract void UpdateCoordinator();
    protected abstract void ReassignEntities();
    protected abstract void CoordinateEntity(Entity e, Vector3 destination);
}