﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LinearAlgebra {
    public struct Rectangle
    {
        public Vector3 topLeftCorner;
        public Vector3 bottomRightCorner;
    }

    public struct LineSegment
    {
        public Vector3 firstPos;
        public Vector3 lastPos;

        public override bool Equals(System.Object obj)
        {
            return this == (LineSegment)obj;
        }

        public static bool operator ==(LineSegment l1, LineSegment l2)
        {
            return l1.firstPos == l2.firstPos && l1.lastPos == l2.lastPos;
        }

        public static bool operator !=(LineSegment l1, LineSegment l2)
        {
            return l1.firstPos != l2.firstPos || l1.lastPos != l2.lastPos;
        }

        public override int GetHashCode()
        {
            return firstPos.GetHashCode() * 7  + lastPos.GetHashCode() * 13;
        }

        public LineSegment FacingTowards(Vector3 position)
        {
            Vector3 midPoint = GetMidpoint();
            Vector3 normal = (position - midPoint).normalized;
            float deltaAngle = Vector3.SignedAngle(normal, GetNormalVector(), Vector3.up) * -1f + 180.0f;

            Vector3 toStart = firstPos - midPoint;
            toStart = Quaternion.Euler(0f, deltaAngle, 0f) * toStart;

            LineSegment newLine = new LineSegment();
            newLine.firstPos = midPoint + toStart;
            newLine.lastPos = midPoint - toStart;

            return newLine;
        }

        public enum PointOrientation
        {
            COLINEAR,
            CLOCKWISE,
            ANTICLOCKWISE
        }

        public PointOrientation Orientation(Vector3 point)
        {
            float val = (point.z - firstPos.z) * (lastPos.x - firstPos.x) -
                        (lastPos.z - firstPos.z) * (point.x - firstPos.x);

            if (val > float.Epsilon)
            {
                return PointOrientation.CLOCKWISE;
            }
            else if (val < float.Epsilon*-1.0f)
            {
                return PointOrientation.ANTICLOCKWISE;
            }

            return PointOrientation.COLINEAR;
        }

        public bool Intersects(Vector3 point)
        {
            return point.x <= Math.Max(firstPos.x, lastPos.x) &&
                   point.x >= Math.Min(firstPos.x, lastPos.x) &&
                   point.y <= Math.Max(firstPos.z, lastPos.z) &&
                   point.y >= Math.Min(firstPos.z, lastPos.z);
        }

        public bool Intersects(LineSegment other)
        {
            PointOrientation o1 = Orientation(other.firstPos);
            PointOrientation o2 = Orientation(other.lastPos);
            PointOrientation o3 = other.Orientation(firstPos);
            PointOrientation o4 = other.Orientation(lastPos);

            // General case 
            if (o1 != o2 && o3 != o4)
                return true;

            // Special Cases 
            if (o1 == PointOrientation.COLINEAR && Intersects(other.firstPos)) return true;
            if (o2 == PointOrientation.COLINEAR && Intersects(other.lastPos)) return true;
            if (o3 == PointOrientation.COLINEAR && other.Intersects(firstPos)) return true;
            if (o4 == PointOrientation.COLINEAR && other.Intersects(lastPos)) return true;

            return false; // Doesn't fall in any of the above cases 
        }

        private Vector3 IntersectionPoint(LineSegment other)
        {
            // This is private, not public, because it does intersection of the line, not line segment.
            float a1 = lastPos.z - firstPos.z;
            float b1 = firstPos.x - lastPos.x;
            float c1 = a1*firstPos.x + b1*firstPos.z;

            float a2 = other.lastPos.z - other.firstPos.z;
            float b2 = other.firstPos.x - other.lastPos.x;
            float c2 = a2*other.firstPos.x + b2*other.firstPos.z;

            float delta = a1 * b2 - a2 * b1;
            if (delta == 0.0f)
            {
                // Lines are parallel, doesn't really matter what we do
                return firstPos;
            }

            float x = (b2 * c1 - b1 * c2) / delta;
            float z = (a1 * c2 - a2 * c1) / delta;

            return new Vector3(x, 0, z);
        }

        public Vector3 ClosestPoint(Vector3 point)
        {
            Vector3 fromFirst = point - firstPos;
            Vector3 dir = GetDirection();

            float lineLength = GetLength();
            float t = Vector3.Dot(dir, fromFirst);

            if (t <= 0f)
                return firstPos;

            if (t >= lineLength)
                return lastPos;

            Vector3 project = dir * t;
            return firstPos + project;
        }

        public Vector3 FurthestPoint(Vector3 point)
        {
            Vector3 fromFirst = point - firstPos;
            Vector3 fromLast = point - lastPos;

            Vector3 furthest = fromFirst.sqrMagnitude > fromLast.sqrMagnitude ? firstPos : lastPos;
            return furthest;
        }

        public Vector3 ClosestPoint(LineSegment other)
        {
            // If we intersect the other line, return our point of intersection directly
            if (Intersects(other))
            {
                return IntersectionPoint(other);
            }

            // Otherwise find closest point
            Vector3 a = ClosestPoint(other.firstPos);
            Vector3 b = ClosestPoint(other.lastPos);

            Vector3 a2 = other.ClosestPoint(a);
            Vector3 b2 = other.ClosestPoint(b);

            Vector3 closer = (a - a2).sqrMagnitude < (b - b2).sqrMagnitude ? a : b;
            return closer;
        }

        public Vector3 FurthestPoint(LineSegment other)
        {
            // Furthest must always be at one of the extents of the line segment
            Vector3 a = FurthestPoint(other.firstPos);
            Vector3 b = FurthestPoint(other.lastPos);

            Vector3 further = (a - other.lastPos).sqrMagnitude > (b - other.firstPos).sqrMagnitude ? a : b;
            return further;
        }

        public Vector3 Lerp(float t)
        {
            return (firstPos + (lastPos - firstPos) * t);
        }

        public Vector3 GetMidpoint()
        {
            return Lerp(0.5f);
        }

        public float GetLength()
        {
            return Vector3.Distance(firstPos, lastPos);
        }

        public float GetLengthSqr()
        {
            return (firstPos - lastPos).sqrMagnitude;
        }

        public Vector3 GetDirection()
        {
            Vector3 dir = lastPos - firstPos;
            return dir.normalized;
        }

        public Vector3 GetNormalVector()
        {
            return Vector3.Cross(Vector3.up, GetDirection());
        }

    }
}
