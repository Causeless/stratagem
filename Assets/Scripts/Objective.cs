using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyBox;

public class Objective : MonoBehaviour
{
    [ReadOnly]
    public Faction capturedBy;
    private Camera mainCamera;
    private Unit_SpatialPartitionGrid _grid;
    
    [Header("Capture Settings")]
    public float CaptureRange;
    public bool CapturableByAllies;
    public bool CapturableByEnemies;
    public float CaptureTime;
    public float MinCaptureTime;
    public int unitDifferenceRequired;
    public int maxUnitCaptureCount = 10;

    [Header("UI")]
    public Transform progressBarTransform; 
    public Transform CaptureZone; 
    public Slider captureBar;
    [ReadOnly]
    public bool beingCaptured;
    [ReadOnly]
    public float captureProgress;
    [ReadOnly]
    public float CaptureSpeed;

    private Dictionary<Faction, int> factionsInZone = new Dictionary<Faction, int> {};
    public List<Unit> unitsInZone = new List<Unit>();
    
    [Serializable]
    public class FactionObjectiveInfo
    {
        public Faction faction;
        public int count;

        public FactionObjectiveInfo(Faction addedFaction, int newCount)
        {
            faction = addedFaction;
            count = newCount;
        }        
    }




    void Start()
    {
        _grid = GameObject.FindWithTag("SpatialPartitionGrid").GetComponent<Unit_SpatialPartitionGrid>();
        if(_grid == null)
        {
            Debug.LogWarning($"Objective ({name}) cannot attach its spatial partition grid");
        }

        mainCamera = Camera.main; 
        GameObject alliances = GameObject.Find("Alliances");
        if(alliances == null)
        {
            Debug.LogWarning($"Objective ({name}) cannot find Alliances GameObject!");
        }

        if(CaptureZone != null)
        {
            float size = CaptureRange * 2f; //The Capture Diameter
            CaptureZone.localScale = new Vector3(size, size, 1);
        }

        foreach(Transform child in alliances.transform)
        {
            Faction faction = child.GetComponent<Faction>();
            if(faction != null)
            {
                factionsInZone.Add(faction, 0);
                FactionObjectiveInfo factionInfo = new FactionObjectiveInfo(faction, 0);
            }
        }
    }

    private void Update()
    {
        if (progressBarTransform != null)
        {
            // Progress Bar should always face the camera
            progressBarTransform.LookAt(mainCamera.transform);
            progressBarTransform.Rotate(0, 180, 0); 
        }
    }

    private void FixedUpdate()
    {
        UnitsInCaptureRange(CaptureRange);
        
        if (beingCaptured)
        {
            UpdateCaptureProgress();
        }
    }

    private void UnitsInCaptureRange(float captureRange)
    {
        List<Unit> nearbyUnits = new List<Unit>();
        nearbyUnits = _grid.GetEntsInRange(this.transform.position, captureRange);

        //If no units nearby, reset all.
        if(nearbyUnits.Count == 0)
        {
            beingCaptured = false;        
            unitsInZone.Clear();
            foreach (var key in factionsInZone.Keys.ToList()) 
            {
                factionsInZone[key] = 0;
            }
            return;
        }

        //If units nearby, if new, add it to the list.
        foreach(Unit nearbyUnit in nearbyUnits)
        if(!unitsInZone.Contains(nearbyUnit))
        {
            unitsInZone.Add(nearbyUnit);
            factionsInZone[nearbyUnit.FriendlyFaction]++;
        }

        //If there are units nearby, make sure we remove units that have left.
        RemoveUnitsNoLongerNearby(nearbyUnits);
        beingCaptured = true; 
    }

    private void RemoveUnitsNoLongerNearby(List<Unit> nearbyUnits)
    {
        if(unitsInZone.Count > nearbyUnits.Count)
        {
            List<Unit> temporaryList = new List<Unit>(unitsInZone);
            foreach(Unit unit in temporaryList)
            {
                if(!nearbyUnits.Contains(unit))
                {
                    unitsInZone.Remove(unit);
                    factionsInZone[unit.FriendlyFaction]--;
                }
            }
        }
    }  

    private void UpdateCaptureProgress()
    {
        if (factionsInZone.Count == 0) return;

        Faction leadingFaction = null;
        Faction secondFaction = null;
        int leadingCount = 0;
        int secondCount = 0;

        // Get leading and second highest faction
        foreach (var pair in factionsInZone)
        {
            if (pair.Value > leadingCount)
            {
                secondFaction = leadingFaction;
                secondCount = leadingCount;

                leadingFaction = pair.Key;
                leadingCount = pair.Value;
            }
            else if (pair.Value > secondCount)
            {
                secondFaction = pair.Key;
                secondCount = pair.Value;
            }
        }

        if (leadingFaction == null || leadingCount == 0) return; 

        Faction capturingFaction = capturedBy ?? leadingFaction;

        // Calculate unit difference
        int unitDifference = leadingCount - secondCount;

        // If contested with only 1 extra unit, use capture time
        float effectiveCaptureTime = CaptureTime;

        // If more than 1 unit difference, scale capture time toward min capture time
        if (unitDifference > unitDifferenceRequired)
        {
            float factor = Mathf.Clamp01((float)unitDifference / maxUnitCaptureCount);
            effectiveCaptureTime = Mathf.Lerp(CaptureTime, MinCaptureTime, factor);
        }

        CaptureSpeed = 1 / effectiveCaptureTime;

        // If another faction controls objective, reduce capture progress
        if (capturedBy != null && capturedBy != leadingFaction)
        {
            captureProgress -= CaptureSpeed * Time.deltaTime;
            if (captureProgress <= 0)
            {
                capturedBy = null; // Remove faction control when capture progress reaches 0
                captureProgress = 0;
            }
        }
        else
        {
            // if not controlled by another faction, increase capture progress
            captureProgress += CaptureSpeed * Time.deltaTime;
            if (captureProgress >= 1)
            {
                capturedBy = leadingFaction;
                captureProgress = 1;
            }
        }

        // Animate UI
        captureBar.value = captureProgress;
        captureBar.fillRect.GetComponent<Image>().color = capturingFaction.FactionColour;

        // Set zone ring color
        CaptureZone.GetComponent<Renderer>().material.color = capturedBy != null ? capturedBy.FactionColour : Color.white;
    }
}
