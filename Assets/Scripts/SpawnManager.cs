using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject spawnPoint1, spawnPoint2;
    private GameObject infantryPrefab, skirmisherPrefab, cavalryPrefab;
    GameObject unitCollection;
    public float UnitSpacing = 15f;
    public List<FactionSpawnList> allfactionSpawnLists = new List<FactionSpawnList>{};

    private void Start()
    {
        SpawnUnits("Alliance1", spawnPoint1);
        SpawnUnits("Alliance2", spawnPoint2);
        DisableSpawnPointMesh(spawnPoint1);
        DisableSpawnPointMesh(spawnPoint2);
        ActivateUnits(); //wait until both alliances have set up all spawn units, then activate them with spawn function.
    }

    private void DisableSpawnPointMesh(GameObject spawnPoint)
    {
        MeshRenderer mesh = spawnPoint.GetComponent<MeshRenderer>();
        mesh.enabled = false;
    }

    private void SpawnUnits(string alliance, GameObject spawnPoint)
    {
        string allianceName = PlayerPrefs.GetString(alliance);
        int infantryCount = PlayerPrefs.GetInt(alliance + "_Unit0");
        int skirmisherCount = PlayerPrefs.GetInt(alliance + "_Unit1");
        int cavalryCount = PlayerPrefs.GetInt(alliance + "_Unit2");

        // Find the Faction GameObject in the scene
        GameObject factionObject = GameObject.Find(allianceName);

        if (factionObject == null)
        {
            Debug.LogError($"Faction '{allianceName}' not found in the scene!");
            return; 
        }

        Faction unitFaction = factionObject.GetComponent<Faction>();
        if (unitFaction == null)
        {
            Debug.LogError($"Faction Object '{allianceName}' does not have Faction script attached!");
            return; 
        }

        Debug.Log($"Spawning for {alliance} ({allianceName}): Infantry={infantryCount}, Skirmishers={skirmisherCount}, Cavalry={cavalryCount}");

        foreach(FactionSpawnList spawnList in allfactionSpawnLists)
        {
            if(spawnList.faction.name == unitFaction.name)
            {
                infantryPrefab = spawnList.infantryPrefab;
                skirmisherPrefab = spawnList.skirmisherPrefab;
                cavalryPrefab = spawnList.cavalryPrefab;
            }
        }

        Vector3 spawnPosition = spawnPoint.transform.position;
        Quaternion spawnRotation = spawnPoint.transform.rotation;
        Vector3 skirmPoint = spawnPosition + (spawnRotation * Vector3.back * 25f);
        Vector3 cavPoint = spawnPosition + (spawnRotation * Vector3.back * 50f);       

        SpawnUnitType(infantryPrefab, infantryCount, spawnPosition, spawnRotation, unitFaction);
        SpawnUnitType(skirmisherPrefab, skirmisherCount, skirmPoint, spawnRotation, unitFaction);
        SpawnUnitType(cavalryPrefab, cavalryCount, cavPoint, spawnRotation, unitFaction);
    }


    private void SpawnUnitType(GameObject prefab, int count, Vector3 spawnPosition, Quaternion spawnRotation, Faction unitFaction)
    {

        for (int i = 0; i < count; i++)
        {
            Vector3 positionOffset = spawnRotation * new Vector3(i * UnitSpacing, 0, 0);
            GameObject unitprefab = Instantiate(prefab, GameObject.FindWithTag("Unit Collection").transform, false);
            Unit unit = unitprefab.GetComponent<Unit>();

            if (unit != null)
            {
                unit.FriendlyFaction = unitFaction;
            }
            else
            {
                Debug.LogError($"Unit prefab '{prefab.name}' does not have a Unit script!");
            }

            unitprefab.transform.position = spawnPosition + positionOffset;
            unitprefab.transform.rotation = spawnRotation;
        }
    }

    private void ActivateUnits()
    {
        unitCollection = GameObject.FindGameObjectWithTag("Unit Collection");
        if (unitCollection != null)
        {
            foreach (Transform child in unitCollection.transform)
            {
                Unit unit = child.GetComponent<Unit>(); 
                if (unit != null)
                {
                    if(!unit.UnitActive)
                    {
                        unit.Spawn(); 
                    }
                }
                else
                {
                    Debug.LogWarning($"No Unit script found on {child.name}");
                }
            }
        }
        else
        {
            Debug.LogError("Unit Collection object not found! Make sure it is tagged correctly.");
        }    
    }
}