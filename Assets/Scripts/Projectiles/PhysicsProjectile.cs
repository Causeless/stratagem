﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using MyBox;

public class PhysicsProjectile : Projectile
{
    public Vector3 _target;
    private Vector3 _groundUnderTarget;
    
    public float directFireRange = 35f;
    private float currentFiringAngle;

    // Firing angle if the target is too far away to hit
    // 45f is the closest to max range
    public float firingAngleBeyondMaxRange = 45f;

    public bool enableRandomSpeed = true;
    [ConditionalField(nameof(enableRandomSpeed))]
    public float randomSpeedMin = 8f;
    [ConditionalField(nameof(enableRandomSpeed))]
    public float randomSpeedMax = 20f;

    // Conditional read-only attribute is broken in MyBox.
    // I've made a pull request for them to fix this, so waiting on that
    //[ReadOnly(nameof(enableRandomSpeed))]
    public float speed = 10f;

    public Transform arrowModel;

    float elapsedTimeOnGround;
    public float flightDuration { get; private set; }

    Vector3 velocity;
    Vector3 lastPosition;

    public bool enableRandomTargetArea = true;
    [ConditionalField(nameof(enableRandomTargetArea))]
    // For every metre of distance, how much the random radius increases by
    public float randomRadiusPerMetre = 0.2f;

    [ReadOnly]
    public bool isFlying;
    public float visibleTime = 5;

    public float gravity = 9.81f;

    public override void Initialize(Vector3 targetPosition, Vector3 targetVelocity)
    {
        isFlying = true;

        float distanceToTarget = (transform.position - targetPosition).magnitude;
        if (enableRandomTargetArea)
        {
            Vector2 randomCircle = Random.insideUnitCircle * randomRadiusPerMetre * distanceToTarget;
            Vector3 finalCircle = new Vector3(randomCircle.x, 0f, randomCircle.y);
            _target = targetPosition + finalCircle;
        }

        speed = GetSpeed();
        
        Vector3 lowSolution, highSolution;
        int numSolutions = fts.solve_ballistic_arc(transform.position, speed, _target, targetVelocity, gravity, out lowSolution, out highSolution);

        if (numSolutions == 0)
        {
            // Impossible to hit the target with our speed
            // So just aim towards our target and throw at a 45 degree angle regardless, let's hope we hit something
            Vector3 towardsTarget = (_target - transform.position).normalized;
            Vector3 perp = Vector3.Cross(towardsTarget, Vector3.up);
            towardsTarget = Quaternion.AngleAxis(firingAngleBeyondMaxRange, perp) * towardsTarget;
            velocity = towardsTarget * speed;
        }
        else
        {
            velocity = lowSolution;
            if (numSolutions > 1)
            {
                if (distanceToTarget > directFireRange)
                {
                    velocity = highSolution;
                }
            }
        }

        // Todo, this ground correction is taking place at the current location, right?
        // It ought to be the predicted target location

        RaycastHit hit;
        int groundLayerMask = 1 << 8;
        if (Physics.Raycast(_target, Vector3.down, out hit, 10f, groundLayerMask))
        {
            _groundUnderTarget = hit.point;
        }
        else
        {
            _groundUnderTarget = _target;
        }

        elapsedTimeOnGround = 0f;
        UpdatePositionAndRotation(0f);
    }

    public float GetSpeed()
    {
        if (enableRandomSpeed) 
        {
            speed = Random.Range(randomSpeedMin, randomSpeedMax);
        }

        return speed;
    }

    private void Update()
    {
        UpdatePositionAndRotation(Time.deltaTime);
    }

    private void UpdatePositionAndRotation(float dt)
    {
        isFlying = velocity.y > 0f || transform.position.y > _groundUnderTarget.y;
        if (isFlying)
        {
            velocity += Vector3.down * gravity * dt;
            transform.Translate(velocity * dt, Space.World);
        }
        else if (elapsedTimeOnGround > visibleTime)
        {
            Destroy(gameObject);
        }
        else
        {
            elapsedTimeOnGround += dt;
        }

        Vector3 dir = transform.position - lastPosition;
        if (dir != Vector3.zero) 
        {
            arrowModel.rotation = Quaternion.LookRotation(dir);
        }

        lastPosition = transform.position;
    }
}