﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using MyBox;

public class SimProjectile : MonoBehaviour
{
    SubUnit _sourceSubUnit;
    SubUnit _targetSubUnit;
    float _spawnDelay;
    int _remainingMissileAttacks = 0;
    int _missileAttacksPerTick = 0;
    float _missileAttackInterval;
    
    private Vector3 _groundUnderTarget;
    
    Vector3 velocity;

    [ReadOnly]
    public bool isFlying;

    // Rename these to not be similar you lazy fuck
    public GameObject ProjectilePrefab;
    private PhysicsProjectile _projectilePrefab;

    public void Initialize(SubUnit source, SubUnit target, float spawnDelay, float missileAttackInterval)
    {
        _projectilePrefab = ProjectilePrefab.GetComponent<PhysicsProjectile>();

        _sourceSubUnit = source;
        _targetSubUnit = target;
        _spawnDelay = spawnDelay;
        _missileAttackInterval = missileAttackInterval;

        isFlying = true;

        Vector3 targetPosition = target.GetPosition();
        Vector3 targetVelocity = target.Velocity;

        float speed = _projectilePrefab.GetSpeed();

        Vector3 lowSolution, highSolution;
        int numSolutions = fts.solve_ballistic_arc(transform.position, speed, targetPosition, targetVelocity, _projectilePrefab.gravity, out lowSolution, out highSolution);

        if (numSolutions == 0)
        {
            // Impossible to hit the target with our speed
            // So just aim towards our target and throw at a 45 degree angle regardless, let's hope we hit something
            Vector3 towardsTarget = (targetPosition - transform.position).normalized;
            Vector3 perp = Vector3.Cross(towardsTarget, Vector3.up);
            towardsTarget = Quaternion.AngleAxis(_projectilePrefab.firingAngleBeyondMaxRange, perp) * towardsTarget;
            velocity = towardsTarget * speed;
        }
        else
        {
            velocity = lowSolution;
            if (numSolutions > 1)
            {
                float distanceToTargetSqr = (transform.position - targetPosition).sqrMagnitude;
                if (distanceToTargetSqr > _projectilePrefab.directFireRange*_projectilePrefab.directFireRange)
                {
                    velocity = highSolution;
                }
            }
        }

        RaycastHit hit;
        int groundLayerMask = 1 << 8; // :(
        if (Physics.Raycast(targetPosition, Vector3.down, out hit, 10f, groundLayerMask))
        {
            _groundUnderTarget = hit.point;
        }
        else
        {
            _groundUnderTarget = targetPosition;
        }
    }

    private void FixedUpdate()
    {
        // Because fixedupdate is much less frequent than the render update, the trajectory can go out of sync
        // So although we updated in the fixed update cycle, we simulate a 60fps trajectory by doing multi sub-steps during our update
        float stepsPerSecond = 1f / Time.fixedDeltaTime;
        int numberOfUpdates = (int)(60f / stepsPerSecond);
        float adjustedDt = Time.fixedDeltaTime / numberOfUpdates;
        
        for (int i = 0; i < numberOfUpdates; i++)
        {
            if (_spawnDelay > 0.0f)
            {
                _spawnDelay -= adjustedDt;
                continue;
            }

            if (!isFlying)
            {
                break;
            }

            if (UpdatePosition(adjustedDt))
            {
                StartMissileDamage();
            };
        }

        if (!isFlying)
        {
            // Apply missile damage
            bool finished = ApplyDamage();
            if (finished)
            {
                // Destroy ourself
                Destroy(gameObject);
            }
        }
    }

    private void StartMissileDamage()
    {
        // Perform 1 attack for each soldier (i.e hp) our subunit has
        _remainingMissileAttacks = _sourceSubUnit._healthPoints;

        // And figure out exactly how many attacks per tick that is
        float stepsPerSecond = 1f / Time.fixedDeltaTime;
        float totalSteps = _missileAttackInterval * stepsPerSecond;
        _missileAttacksPerTick = Mathf.Max((int)(_remainingMissileAttacks / totalSteps), 1);
    }

    // Returns whther or not we've just finished flying (so we can start the missile damage)
    private bool UpdatePosition(float dt)
    {
        // Check whether or not we're still flying
        bool test = velocity.y > 0f || transform.position.y > _groundUnderTarget.y;

        // Move ourself
        velocity += Vector3.down * _projectilePrefab.gravity * dt;
        transform.Translate(velocity * dt, Space.World);

        bool oldIsFlying = isFlying;
        isFlying = test;
        return oldIsFlying != isFlying;
    }

    // Returns whether we've finished applying damage
    private bool ApplyDamage()
    {
        bool finished = false;
        if (_missileAttacksPerTick >= _remainingMissileAttacks)
        {
            // Only apply the remaining amount
            _missileAttacksPerTick = _remainingMissileAttacks;
            finished = true;
        }

        // just in case...
        if (_sourceSubUnit == null || _targetSubUnit == null)
        {
            return true;
        }

        for (int i = 0; i < _missileAttacksPerTick; i++)
        {
            // We've hit our target, apply damage etc
            _remainingMissileAttacks--;
            _targetSubUnit.ReceiveSingleMissileAttackFrom(_sourceSubUnit);
        }

        return finished;
    }
}