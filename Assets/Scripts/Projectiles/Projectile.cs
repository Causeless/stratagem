using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    public abstract void Initialize(Vector3 targetPosition, Vector3 targetVelocity);
}
