using UnityEngine;
using System.Collections;
using MyBox;

// Faked projectile that moves at a consistent lateral speed instead of according to physics
public class LateralSpeedProjectile : Projectile
{
    public Vector3 _target;
    private Vector3 _groundUnderTarget;

    public float highFiringAngle = 45.0f;
    public float lowFiringAngle = 7.0f;
    public float directFireRange = 35;

    private float currentFiringAngle;

    public Transform arrowModel;

    float elapsedTime = 0f;
    float elapsedTimeOnGround = 0f;
    public float flightDuration { get; private set; }

    Vector2 velocity;
    Vector3 lastPosition;

    public bool enableRandomTargetArea = true;

    [ConditionalField(nameof(enableRandomTargetArea))]
    public float randomRadius = 2f;

    public bool enableRandomSpeed = true;

    [ConditionalField(nameof(enableRandomSpeed))]
    public float randomSpeedMin = 8f;
    [ConditionalField(nameof(enableRandomSpeed))]
    public float randomSpeedMax = 20f;

    //[ReadOnly(nameof(enableRandomSpeed))]
    public float speed = 10f;

    [ReadOnly]
    public bool isFlying;
    
    public float visibleTime = 5;

    public override void Initialize(Vector3 targetPosition, Vector3 targetVelocity)
    {
        _target = targetPosition;
        if (enableRandomTargetArea)
        {
            Vector2 randomCircle = Random.insideUnitCircle * randomRadius;
            Vector3 finalCircle = new Vector3(randomCircle.x, 0f, randomCircle.y);
            _target += finalCircle;
        }

        float distanceToTargetSqr = (transform.position - _target).sqrMagnitude;

        currentFiringAngle = lowFiringAngle;
        if (distanceToTargetSqr > directFireRange*directFireRange)
        {
            currentFiringAngle = highFiringAngle;
        }

        if (enableRandomSpeed) 
        {
            speed = Random.Range(randomSpeedMin, randomSpeedMax);
        }

        //complications might arise here if a unit is in combat, and so perhaps hasPath is true, but it is not moving because it is engaged, all depending on how combat movement works out
        //see both the 'Formation' script in method 'ShootProjectiles', and where 'ShootProjectiles' is called in 'Entity Selector'

        const int predictIterations = 5;
        Vector3 predictTarget = _target;
        for (int i = 0; i < predictIterations; i++)
        {   
            //calculate distance to target
            float dist = Vector3.Distance(transform.position, predictTarget);

            float vel = Mathf.Sqrt((dist * speed) / Mathf.Sin(2 * currentFiringAngle * Mathf.Deg2Rad));

            velocity.x = vel * Mathf.Cos(currentFiringAngle * Mathf.Deg2Rad);
            velocity.y = vel * Mathf.Sin(currentFiringAngle * Mathf.Deg2Rad);

            //calculate flight time
            flightDuration = dist / velocity.x;
            predictTarget = _target + targetVelocity * flightDuration;
        }

        isFlying = true;        //rotate projectile to face the target
        
        Vector3 dir = predictTarget - transform.position;
        if (dir != Vector3.zero) 
        {
            transform.rotation = Quaternion.LookRotation(dir);
        }

        RaycastHit hit;
        int groundLayerMask = 1 << 8;
        if (Physics.Raycast(_target, Vector3.down, out hit, 10f, groundLayerMask))
        {
            _groundUnderTarget = hit.point;
        }
        else
        {
            _groundUnderTarget = _target;
        }

        elapsedTime = 0f;
        lastPosition = transform.position;
    }

    private void Update()
    {
        isFlying = transform.position.y > _groundUnderTarget.y || elapsedTime < flightDuration;
        if (isFlying)
        {
            //translate the x component on the z-axis => forward vector (rotation)
            transform.Translate(0, (velocity.y - (speed * elapsedTime)) * Time.deltaTime, velocity.x * Time.deltaTime);
            elapsedTime += Time.deltaTime;
        }
        else if (elapsedTimeOnGround > visibleTime)
        {
            Destroy(gameObject);
        }
        else
        {
            elapsedTimeOnGround += Time.deltaTime;
        }

        Vector3 dir = transform.position - lastPosition;
        if (dir != Vector3.zero) 
        {
            arrowModel.rotation = Quaternion.LookRotation(dir);
        }

        lastPosition = transform.position;
    }
}