﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlaceHolderManager : MonoBehaviour
{
    public GameObject placeHolderPrefab = null;

    public int PoolSize = 10000;//this number may have to go way up for placeholders later

    private GameObject[] PlaceHolderArray;
    private int _activePlaceholders;

    public static PlaceHolderManager PlaceHolderManagerSingleton = null;

    private void Start()
    {
        PlaceHolderArray = new GameObject[PoolSize];
        PlaceHolderManagerSingleton = this;

        for (int i = 0; i < PoolSize; i++)
        {
            PlaceHolderArray[i] = Instantiate(placeHolderPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            Transform ObjTransform = PlaceHolderArray[i].GetComponent<Transform>();
            ObjTransform.parent = transform;
            PlaceHolderArray[i].SetActive(false);
        }
    }

    private Transform GetNewPlaceholder()
    {
        Transform newPlaceholder = PlaceHolderArray[_activePlaceholders].transform;

        newPlaceholder.gameObject.SetActive(true);
        _activePlaceholders++;

        return newPlaceholder;
    }

    public static Transform SpawnPlaceholder(Vector3 Position, Quaternion Rotation)
    {
        Transform SpawnedPlaceholder = PlaceHolderManagerSingleton.GetNewPlaceholder();

        SpawnedPlaceholder.position = Position;
        SpawnedPlaceholder.rotation = Rotation;

        return SpawnedPlaceholder;

    }

    public static void SetPlaceHoldersInactive()
    {
        if (!PlaceHolderManagerSingleton)
        {
            return;
        }

        for (int i = 0; i < PlaceHolderManagerSingleton._activePlaceholders; i++)
        {
            Transform SpawnedPlaceholder = PlaceHolderManagerSingleton.PlaceHolderArray[i].transform;
            SpawnedPlaceholder.gameObject.SetActive(false);
        }

        PlaceHolderManagerSingleton._activePlaceholders = 0;
    }
}
