using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingBox
{
    public Vector3 center;
    public Vector3 size;
    public Quaternion rotation;

    public BoundingBox(Vector3 center, Vector3 size, Quaternion rotation)
    {
        this.center = center;
        this.size = size;
        this.rotation = rotation;
    }
}