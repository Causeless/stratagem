﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainDetector : MonoBehaviour
{
    float timeToGo;
    // Use this for initialization
    void Start()
    {
        timeToGo = Time.time + .10f;
    }
    [HideInInspector]
    public int surfaceIndex;

    // Update is called once per frame
    void Update()
    {

        if (Time.time >= timeToGo)//should do this every second
        {
            surfaceIndex = TerrainSurface.GetMainTexture(transform.position);
            timeToGo = Time.time + .10f;//happens once a second
            //Debug.Log("Terrain type: " + surfaceIndex);
        }

        //Debug.Log("Terrain type: " + surfaceIndex);
    }
}


/*cold use this to greatly reduce rate of calculations
  float timeToGo;
 void Start() {
     timeToGo = Time.fixedTime + 1.0f;//i think this means its going to  happen every seconds
 }
 void FixedUpdate() {
     if (Time.fixedTime >= timeToGo) {
         // Do your thang
         timeToGo = Time.fixedTime + 1.0f;
     }
 }
     */
