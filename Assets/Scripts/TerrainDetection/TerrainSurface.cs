﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainSurface : MonoBehaviour
{
    //there is also a way to get the terrain type using getPixel and raycasts which may or may not
    //be better or more performant than this method, I'm not sure. I have a non-working attempt at that in
    //GetPixelTerrainDetector script

    //where this script comes from (modified a bit to account for some scripting changes since 2010):
    //http://answers.unity3d.com/questions/34328/terrain-with-multiple-splat-textures-how-can-i-det.html

    //this script may not be performant on a large scale, perhaps one, invincible cube will have the
    //terrain detector script attached, and it will get pixels from a substantial area underneath it
    //and take the dominant terrain type for that area, so still providing a pretty accurate approximation
    //for the current terrain type of the whole unit

    public static float[] GetTextureMix(Vector3 worldPos)
    {
        // returns an array containing the relative mix of textures
        // on the main terrain at this world position.
        // The number of values in the array will equal the number
        // of textures added to the terrain.
        Terrain terrain = Terrain.activeTerrain;
        TerrainData terrainData = terrain.terrainData;
        Vector3 terrainPos = terrain.transform.position;

        // calculate which splat map cell the worldPos falls within (ignoring y)
        int mapX = (int)(((worldPos.x - terrainPos.x) / terrainData.size.x) * terrainData.alphamapWidth);
        int mapZ = (int)(((worldPos.z - terrainPos.z) / terrainData.size.z) * terrainData.alphamapHeight);

        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = terrainData.GetAlphamaps(mapX, mapZ, 1, 1);

        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];
        for (int n = 0; n < cellMix.Length; ++n)
        {
            cellMix[n] = splatmapData[0, 0, n];
        }
        return cellMix;
    }

    public static int GetMainTexture(Vector3 worldPos)
    {
        // returns the zero-based index of the most dominant texture
        // on the main terrain at this world position.
        float[] mix = GetTextureMix(worldPos);
        float maxMix = 0;
        int maxIndex = 0;
        // loop through each mix value and find the maximum
        for (int n = 0; n < mix.Length; ++n)
        {
            if (mix[n] > maxMix)
            {
                maxIndex = n;
                maxMix = mix[n];
            }
        }
        return maxIndex;
    }
}
