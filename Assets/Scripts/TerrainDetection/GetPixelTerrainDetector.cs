﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPixelTerrainDetector : MonoBehaviour
{

    //this is an alternative attempt at getting the terrain type using
    //a raycast and GetPixel, but I don't have it working quite yet, I think there
    //is an issue with the hit.transform...line which is not the proper way to get
    //terrain textures, i have commented it out for now because it throws errors


    // Use this for initialization
    void Start()
    {

    }

    //some helpful links on this getpixel method:
    //http://answers.unity3d.com/questions/163662/get-pixel-colour-on-a-ray-hit.html
    //https://docs.unity3d.com/ScriptReference/Texture2D.GetPixel.html
    //http://answers.unity3d.com/questions/537783/changing-footstep-sound-when-on-different-surface.html
    //https://forum.unity3d.com/threads/how-to-detect-terrain-type.5952/
    //http://answers.unity3d.com/questions/456973/getting-the-texture-of-a-certain-point-on-terrain.html
    //http://answers.unity3d.com/questions/14998/how-can-i-perform-some-action-based-on-the-terrain.html
    //https://forum.unity3d.com/threads/detecting-terrain-texture-at-position.94723/
    //https://forum.unity3d.com/threads/what-terrain-texture-am-i-standing-on.44959/
    //after researching for awhile those were a few of the threads that were most helpful

    /*commented out all of it because it throws errors
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, transform.TransformDirection(Vector3.down), out hit, 50f))
        {
            Texture2D TextureMap = (Texture2D)hit.transform.GetComponent<Renderer>().material.mainTexture;//i think this line is wrong for terrain textures
            //also the fact that i have to use an explicit cast means something proabably went wrong
            //Texture2D TextureMap = hit.transform.renderer.material.mainTexture;//original
            var pixelUV = hit.textureCoord;
            pixelUV.x *= TextureMap.width;
            pixelUV.y *= TextureMap.height;

            int x = (int)pixelUV.x;//added to make integers
            int y = (int)pixelUV.y;//same

            Debug.Log("x=" + pixelUV.x + ",y=" + pixelUV.y + ". Texture map pixel: " + TextureMap.GetPixel(x,y));//replaced (pixelUV.x, pixelUV.y) with (x,y)
            //GetPixels32 is more performant but requires miplevel as input?
            //or could use GetPixels(x,y,1,1)), not sure what last two integers should be for height and width


            //var tex = hit.transform.
        }
    }*/
}
