﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
//this script reads the terrain areas from the navmaesh, as opposed to the other terrainDetector which is reading the texture types painted on the terrain
[RequireComponent(typeof(NavMeshAgent))]
public class ReadTerrainAreaFromNavMesh : MonoBehaviour
{

    private NavMeshAgent agent;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    void FixedUpdate()
    {
        int waterMask = 1 << NavMesh.GetAreaFromName("Water");// 1 << means shift 1 to the left by the number of bits specified by navmesh.getareafromname("water")
        //Debug.Log("area from name: " + NavMesh.GetAreaFromName("Water"));
        int denseForestMask = 1 << NavMesh.GetAreaFromName("Dense Forest");
        //Debug.Log("watermask " + waterMask);
        NavMeshHit hit;
        agent.SamplePathPosition(-1, 0.0f, out hit);
        //Debug.Log("hit mask: " + hit.mask);
        if (hit.mask == waterMask) { }
        //Debug.Log("In The Water");
        else if (hit.mask == denseForestMask) { }
        //Debug.Log("In the forest");
        else { }
        //Debug.Log("On normal land");

    }
}



