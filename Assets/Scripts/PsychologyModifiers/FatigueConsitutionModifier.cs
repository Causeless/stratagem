using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Constitution Modifiers
[System.Serializable]
public class FatigueConsitutionModifier : PsychologyModifier
{
    public float FatigueConsitutionModMin= -0.55f;
    public float FatigueConsitutionModMax= 0f;
    public float FatigueConsitutionMod = 0f;

    public FatigueConsitutionModifier() : base("Constitution decrease from Fatigue") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.Fatigue >= 100f)
        {
            return 0f;
        }

        var t = unit.Fatigue/100f;
        FatigueConsitutionMod = Mathf.Lerp(FatigueConsitutionModMin, FatigueConsitutionModMax, t);
        return unit.UnitStats.BaseConsitution * FatigueConsitutionMod;
    }
}