using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[System.Serializable]
public class DisciplineModifierLowMorale : PsychologyModifier
{
    [SerializeField]
    public float DisciplineDecayMoraleLowMin = -1f;
    public float DisciplineDecayMoraleLowMax = 0f;
    public float DisciplineRestoreMoraleLow = 2f;
    [ReadOnly]
    public float DisciplineDecayMoraleLow = 0f;

    public float StartsApplyingFromLowMorale = 25.0f;

    public DisciplineModifierLowMorale() : base("Discipline Decay as Morale Trends Towards 0") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if(unit.Discipline <= 0 || unit.Morale > StartsApplyingFromLowMorale)
        {
            DisciplineDecayMoraleLow += DisciplineRestoreMoraleLow * Time.fixedDeltaTime;
            DisciplineDecayMoraleLow = Mathf.Clamp(DisciplineDecayMoraleLow, DisciplineDecayMoraleLow, 0f);
            return DisciplineDecayMoraleLow;
        }
        
        float min = 0f;
        float max = StartsApplyingFromLowMorale / 100f;
        float mult = 100f / StartsApplyingFromLowMorale;
        float value = unit.Morale / 100f;
        value = Mathf.Clamp(value, min, max);

        var t = value * mult;

        float DisciplineDecayMoraleLowRate = Mathf.Lerp(DisciplineDecayMoraleLowMin, DisciplineDecayMoraleLowMax, t);
        DisciplineDecayMoraleLow += DisciplineDecayMoraleLowRate * Time.fixedDeltaTime;
        return DisciplineDecayMoraleLow;
    }
}