using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MyBox;

[System.Serializable]
public abstract class PsychologyModifier
{
    [HideInInspector]
    public string Description;

    [ReadOnly]
    public float Value;

    protected PsychologyModifier(string description)
    {
        Description = description;
    }

    public virtual float GetValue(Unit unit) 
    { 
        Value = GetValueImpl(unit); 
        return Value; 
    }

    protected virtual float GetValueImpl(Unit unit) { return 0f; }
}