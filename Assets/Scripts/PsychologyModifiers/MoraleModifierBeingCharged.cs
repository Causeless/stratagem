using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[System.Serializable]
public class MoraleModifierBeingCharged : DisciplineAffectedModifier
{
    [SerializeField]
    public float MoraleModChargedMin = 0f;
    public float MoraleModChargedMax = -25f;
    public float MoraleModChargedDecayRate = 2f;
    public float MoraleModChargedRestoreRate = 3f;
    public float MoraleChargedDisciplinePower = 0.1f;
    public float MoraleChargedMoralePower = 0.2f;

    [ReadOnly]
    public float ChargeBonusAgainst = 0f;

    private float _currentMoraleModCharged;

    public MoraleModifierBeingCharged() : base("Morale Decline when Charged")
    {
        _currentMoraleModCharged = MoraleModChargedMin;
    }

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.UnitState == Unit.UnitStateMachine.Braced)
        {
            _currentMoraleModCharged += (Time.fixedDeltaTime * MoraleModChargedRestoreRate);
            _currentMoraleModCharged = Mathf.Clamp(_currentMoraleModCharged, MoraleModChargedMax, MoraleModChargedMin);
        }
        
        ChargeBonusAgainst = unit.CalculateChargePowerAgainst(MoraleChargedDisciplinePower, MoraleChargedMoralePower);
        _currentMoraleModCharged -= (Time.fixedDeltaTime * (MoraleModChargedDecayRate * ChargeBonusAgainst));
        _currentMoraleModCharged = Mathf.Clamp(_currentMoraleModCharged, MoraleModChargedMax, MoraleModChargedMin);
        
        return _currentMoraleModCharged;
    }
}