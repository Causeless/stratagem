using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Hit Chance Modifier
[System.Serializable]
public class FatigueHitChanceModifier : PsychologyModifier
{
    public float FatigueHitChanceModMin= -0.55f;
    public float FatigueHitChanceModMax= 0f;
    public float FatigueHitChanceMod = 1f;

    public FatigueHitChanceModifier() : base("Hit Chance decrease from Fatigue") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.Fatigue >= 100f)
        {
            return 0f;
        }

        var t = unit.Fatigue/100f;
        FatigueHitChanceMod = Mathf.Lerp(FatigueHitChanceModMin, FatigueHitChanceModMax, t);
        return unit.UnitStats.BaseHitChance * FatigueHitChanceMod;
    }
}