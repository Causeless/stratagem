using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[System.Serializable]
public class DisciplineModifierHighMorale : PsychologyModifier
{
    [SerializeField]
    public float DisciplineDecayMoraleHighMin = 0f;
    public float DisciplineDecayMoraleHighMax = -1f;
    public float DisciplineRestoreMoraleHigh = 2f;
    [ReadOnly]
    public float DisciplineDecayMoraleHigh = 0f;

    public float StartsApplyingFromMorale = 75.0f;

    public DisciplineModifierHighMorale() : base("Discipline Decay as Morale Trends Towards 100") {}

    protected override float GetValueImpl(Unit unit)
    {
        if(unit.Discipline <= 0f || unit.Morale < StartsApplyingFromMorale)
        {
            DisciplineDecayMoraleHigh += DisciplineRestoreMoraleHigh * Time.fixedDeltaTime;
            DisciplineDecayMoraleHigh = Mathf.Clamp(DisciplineDecayMoraleHigh, DisciplineDecayMoraleHigh, 0f);
            return DisciplineDecayMoraleHigh;
        }

        float min = 0f;
        float max = 100f - StartsApplyingFromMorale;
        float value = unit.Morale - StartsApplyingFromMorale;
        value = Mathf.Clamp(value, min, max);

        var t = value / max;

        float DisciplineDecayMoraleHighRate = Mathf.Lerp(DisciplineDecayMoraleHighMin, DisciplineDecayMoraleHighMax, t);
        DisciplineDecayMoraleHigh += DisciplineDecayMoraleHighRate * Time.fixedDeltaTime;
        return DisciplineDecayMoraleHigh;
    }
}