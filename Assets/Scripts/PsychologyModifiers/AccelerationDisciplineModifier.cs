using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[System.Serializable]
public class AccelerationDisciplineModifier : PsychologyModifier
{
    [SerializeField]
    public float DisciplineModAccelerationMin = -30f;
    public float DisciplineModAccelerationMax = 0f;
    public float DisciplineModAccelerationDecay = -15f;
    public float DisciplineModAccelerationRestoreRate = 1f;

    [ReadOnly]
    public float AverageSpeed = 0f;
    public float ResponseTimeSpeedDiff = 1f;

    private float _currentDisciplineModAcceleration;

    public AccelerationDisciplineModifier() : base("Discipline Decay when accelerating") 
    {
        _currentDisciplineModAcceleration = DisciplineModAccelerationMax;
    }

    //Every time speed increases, decrease discipline. Over time it should begin to stabilize.

    protected override float GetValueImpl(Unit unit)
    {
        float acceleration = (unit.AverageUnitSpeed() - AverageSpeed) * Time.fixedDeltaTime;
        if (acceleration > ResponseTimeSpeedDiff)
        {
            // We've accelerated
            _currentDisciplineModAcceleration = DisciplineModAccelerationDecay * acceleration;
        }

        _currentDisciplineModAcceleration += Time.fixedDeltaTime * DisciplineModAccelerationRestoreRate;
        _currentDisciplineModAcceleration = Mathf.Clamp(_currentDisciplineModAcceleration, DisciplineModAccelerationMin, DisciplineModAccelerationMax);

        AverageSpeed = unit.AverageUnitSpeed();
        return _currentDisciplineModAcceleration;
    }
}