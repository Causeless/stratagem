using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

// Public facing modifiers by Psychology stats against other unit stats.
public partial class PsychologyMods : MonoBehaviour
{
    private Unit _target;
    public List<StatModConfig> StatMods = new List<StatModConfig>();


    //TODO implement effect of statMod over time (animation curve). 
    // Implement Stat Mult on tracked stats (stats changing based on other stats).
    // Implement coroutine on event triggers.
    private void ExampleEvent()
    {
        foreach(StatModConfig statMod in StatMods)
        {
            foreach(UnitEventManager.UnitEventTriggers trigger in statMod.eventTriggerList)
            {
                if(!_target._events.ReturnEventBool(trigger))
                {
                    return;
                }

                _target._events.ApplyStatModifier(statMod.ModifiedStat, statMod.endValue);
            }
        }
    }

    [Header("Psychology Formation Rules")]
    // When discipline decreases, formation order should decrease while increasing entity drag. 
    public float DisciplineFormationModMin = 0.01f;
    public float DisciplineFormationModMax = 1f;
    public float DisciplineRandomnessModMin = 0f;
    public float DisciplineRandomnessModMax = 1.5f;

    public float DisciplineMoraleChangeModMin = 2.0f;
    public float FatigueResistanceRate = 1f;
    public float WoundedConstitutionMod = 1f;
    
    [SerializeReference]
    public List<PsychologyModifier> MoraleModifiers = new List<PsychologyModifier>();

    [SerializeReference]
    public List<PsychologyModifier> DisciplineModifiers = new List<PsychologyModifier>();

    [SerializeReference]
    public List<PsychologyModifier> FatigueModifiers = new List<PsychologyModifier>();

    [SerializeReference]
    public List<PsychologyModifier> WeaknessToCavalryModifiers = new List<PsychologyModifier>();
    
    [SerializeReference]
    public List<PsychologyModifier> AttackSpeedModifiers = new List<PsychologyModifier>(); 

    [SerializeReference]
    public List<PsychologyModifier> MissileAttackIntervalModifiers = new List<PsychologyModifier>();  

    [SerializeReference] 
    public List<PsychologyModifier> HitChanceModifiers = new List<PsychologyModifier>();

    [SerializeReference]
    public List<PsychologyModifier> DefenceChanceModifiers = new List<PsychologyModifier>();

    [SerializeReference]
    public List<PsychologyModifier> CriticalHitChanceModifiers = new List<PsychologyModifier>();

    [SerializeReference]
    public List<PsychologyModifier> ConstitutionModifiers = new List<PsychologyModifier>();

    [SerializeReference]
    public List<PsychologyModifier> UnitSpeedModifiers = new List<PsychologyModifier>();

    private void AddModifierIfNew(List<PsychologyModifier> list, PsychologyModifier modifier)
    {
        var match = list.FirstOrDefault(modToCheck => modToCheck.GetType() == modifier.GetType());
        if (match == null)
        {
            list.Add(modifier);
        }
    }

    //Add active Modifiers to Modifier Lists.
    public void AddMoraleMods()
    {
        AddModifierIfNew(MoraleModifiers, new MoraleModifierTotalCasualties());
        AddModifierIfNew(MoraleModifiers, new MoraleModifierCharging());
        AddModifierIfNew(MoraleModifiers, new MoraleModifierBeingCharged());
        AddModifierIfNew(MoraleModifiers, new MoraleModifierTotalWounds());
        AddModifierIfNew(MoraleModifiers, new MoraleModifierRecentCasualties());
    }

    public void AddDisciplineMods()
    {
        AddModifierIfNew(DisciplineModifiers, new FatigueDisciplineModifier());
        AddModifierIfNew(DisciplineModifiers, new ChargingDisciplineModifier());
        AddModifierIfNew(DisciplineModifiers, new DisciplineModifierHighMorale());
        AddModifierIfNew(DisciplineModifiers, new DisciplineModifierLowMorale()); 
        AddModifierIfNew(DisciplineModifiers, new DisciplineModifierBeingCharged());
        AddModifierIfNew(DisciplineModifiers, new AccelerationDisciplineModifier());
    }

    public void AddFatigueMods()
    {
        AddModifierIfNew(FatigueModifiers, new FatigueRateWalking());
        AddModifierIfNew(FatigueModifiers, new FatigueRateRunning());
        AddModifierIfNew(FatigueModifiers, new FatigueRateCombat());
        AddModifierIfNew(FatigueModifiers, new FatigueRateIdle());
        AddModifierIfNew(FatigueModifiers, new FatigueRateCharging());
    } 

    public void AddWeaknessToCavalryMods()
    {
    } 

    public void AddAttackSpeedMods()
    {
        AddModifierIfNew(AttackSpeedModifiers, new MoraleAttackSpeedModifier());
    }

    public void AddMissileAttackIntervalMods()
    {
        AddModifierIfNew(MissileAttackIntervalModifiers, new DisciplineAttackIntervalModifier());
    }

    public void AddHitChanceMods()
    {
        AddModifierIfNew(HitChanceModifiers, new FatigueHitChanceModifier());
    }

    public void AddDefenceChanceMods()
    {
        AddModifierIfNew(DefenceChanceModifiers, new FatigueDefenceChanceModifier());
        AddModifierIfNew(DefenceChanceModifiers, new DisciplineDefenceChanceModifier());
    }

    public void AddCritChanceMods()
    {
        AddModifierIfNew(CriticalHitChanceModifiers, new FatigueCriticalChanceModifier());
    }

    public void AddConstitutionMods()
    {
        AddModifierIfNew(ConstitutionModifiers, new FatigueConsitutionModifier());
        AddModifierIfNew(ConstitutionModifiers, new WoundsConsitutionModifier());
    }  

    public void AddUnitSpeedMods()
    {
        AddModifierIfNew(UnitSpeedModifiers, new FatigueSpeedModifier());
        AddModifierIfNew(UnitSpeedModifiers, new CommitmentSpeedModifier());
    }  
    //Adding Modifiers end.

    private void Awake()
    {
        _target = GetComponent<Unit>();
    }

    private void FixedUpdate()
    {
        if(!_target.UnitActive)
        {
            return;
        }

        ExampleEvent();
        
        
        _target.Morale = CalculateUnitMorale(_target);
        _target.Morale = Mathf.Clamp(_target.Morale, 0f, 100f);

        _target.Discipline = CalculateUnitDiscipline(_target);
        _target.Discipline = Mathf.Clamp(_target.Discipline, 0f, 100f);

        _target.Fatigue = CalculateUnitFatigue(_target);
        _target.Fatigue = Mathf.Clamp(_target.Fatigue, 0f, 100f);

        _target.WeaknessToCavalry = CalculateUnitWeaknessToCavalry(_target);
        _target.WeaknessToCavalry = Mathf.Clamp(_target.WeaknessToCavalry, 0f, 10f);

        _target.AttackSpeed = CalculateUnitAttackSpeed(_target);
        _target.AttackSpeed = Mathf.Clamp(_target.AttackSpeed, 0f, _target.UnitStats.MaxAttackSpeed);

        _target.MissileAttackInterval = CalculateUnitMissileAttackInterval(_target); //We don't have any psych mods that currently edit reload speed, we'll need this for supression?
        _target.MissileAttackInterval = Mathf.Clamp(_target.MissileAttackInterval, 0f, _target.ReloadTime); 

        _target.HitChance = CalculateUnitHitChance(_target);
        _target.HitChance = Mathf.Clamp(_target.HitChance, 0f, 100f);

        _target.MissileHitChance = CalculateUnitMissileHitChance(_target);
        _target.MissileHitChance = Mathf.Clamp(_target.MissileHitChance, 0f, 100f);

        _target.DefenceChance = CalculateDefenceChance(_target);
        _target.DefenceChance = Mathf.Clamp(_target.DefenceChance, 0f, 99f);

        _target.MissileDefenceChance = CalculateMissileDefenceChance(_target);
        _target.MissileDefenceChance = Mathf.Clamp(_target.MissileDefenceChance, 0f, 99f);

        _target.CriticalHitChance = CalculateCriticalHitChance(_target);
        _target.CriticalHitChance = Mathf.Clamp(_target.CriticalHitChance, 0f, 100f);

        _target.MissileCriticalHitChance = CalculateMissileCriticalHitChance(_target);
        _target.MissileCriticalHitChance = Mathf.Clamp(_target.MissileCriticalHitChance, 0f, 100f);

        _target.Consitution = CalculateConsitution(_target);
        _target.Consitution = Mathf.Clamp(_target.Consitution, 0f, 99f);

        _target.WalkSpeed = CalculateUnitWalkSpeed(_target);
        _target.WalkSpeed = Mathf.Clamp(_target.WalkSpeed, _target.UnitStats.BaseWalkSpeed*0.2f, _target.UnitStats.BaseWalkSpeed*3f);
        _target.RunSpeed = CalculateUnitRunSpeed(_target);
        _target.RunSpeed = Mathf.Clamp(_target.RunSpeed, _target.UnitStats.BaseRunSpeed*0.2f, _target.UnitStats.BaseRunSpeed*3f);
        _target.ChargeSpeed = CalculateUnitChargeSpeed(_target);
        _target.ChargeSpeed = Mathf.Clamp(_target.ChargeSpeed, _target.UnitStats.BaseChargeSpeed*0.2f, _target.UnitStats.BaseChargeSpeed*3f);
    }

    float CalculateValueFromModifiers(Unit unit, float baseValue, List<PsychologyModifier> modifiers)
    {
        foreach (PsychologyModifier mod in modifiers)
        {
            baseValue += mod.GetValue(unit);
        }

        return baseValue;
    }

    //Calcuate updated Unit stats from Modifier Lists.
    float CalculateUnitMorale(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseMorale, MoraleModifiers);
    }

    float CalculateUnitDiscipline(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseDiscipline, DisciplineModifiers);
    }

    float CalculateUnitFatigue(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.Fatigue, FatigueModifiers);
    }

    float CalculateUnitWeaknessToCavalry(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.BaseWeaknessToCavalry, WeaknessToCavalryModifiers);
    }

    float CalculateUnitAttackSpeed(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseAttackSpeed, AttackSpeedModifiers);
    }

    float CalculateUnitMissileAttackInterval(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseMissileAttackInterval, MissileAttackIntervalModifiers);
    }

    float CalculateUnitHitChance(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseHitChance, HitChanceModifiers);
    }

    float CalculateUnitMissileHitChance(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseMissileHitChance, HitChanceModifiers);
    }

    float CalculateDefenceChance(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseDefenceChance, DefenceChanceModifiers);
    }

    float CalculateMissileDefenceChance(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseMissileDefenceChance, DefenceChanceModifiers);
    }

    float CalculateCriticalHitChance(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseCriticalHitChance, CriticalHitChanceModifiers);
    }

    float CalculateMissileCriticalHitChance(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseMissileCriticalHitChance, CriticalHitChanceModifiers);
    }

    float CalculateConsitution(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseConsitution, ConstitutionModifiers);
    }

    // TODO, this unit walk speed and run speed stuff doesn't really work well because it's linear
    // If for example base walkspeed = 2 and base runspeed = 4, runspeed couldn't possibly go lower than 2 without the walk becoming fully stationary
    float CalculateUnitWalkSpeed(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseWalkSpeed, UnitSpeedModifiers);
    }

    float CalculateUnitRunSpeed(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseRunSpeed, UnitSpeedModifiers);
    }

    float CalculateUnitChargeSpeed(Unit unit)
    {
        return CalculateValueFromModifiers(unit, unit.UnitStats.BaseChargeSpeed, UnitSpeedModifiers);
    }
    //Calcuations end. 
}