using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

//Morale Modifiers
[System.Serializable]
public class MoraleModifierTotalCasualties : DisciplineAffectedModifier
{
    public float MoraleModCasualties = 1.3f;

    [ReadOnly]
    public float SustainedCasualties; 

    public MoraleModifierTotalCasualties() : base("Morale Impact from Total Casualties") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        float casualtyPercentage = (unit.HealthPoints/(float)unit.UnitStats.BaseHealthPoints) - 1f; // % of casualties - 1.       
        if (casualtyPercentage >= 0)
        {
            return 0f;
        }

        SustainedCasualties = unit.UnitStats.BaseHealthPoints - unit.GetUnitHealth();
        return unit.UnitStats.BaseMorale * casualtyPercentage * MoraleModCasualties; // % of casualties - 1 * Casualty Morale Mod.
    }
}