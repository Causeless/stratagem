using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChargingDisciplineModifier : PsychologyModifier
{
    [SerializeField]
    public float DisciplineModChargingMin = 0f;
    public float DisciplineModChargingMax = -100f;
    public float DisciplineModChargingDecayRate = 1f;
    public float DisciplineModChargingRestoreRate = 0.2f;

    private float _currentDisciplineModCharging;

    public ChargingDisciplineModifier() : base("Discipline Decay during Charge") 
    {
        _currentDisciplineModCharging = DisciplineModChargingMin;
    }

    //THIS WILL GET REPLACED BY THE CHARGE COMMITMENT SYSTEM!
    protected override float GetValueImpl(Unit unit)
    {                     
        if(!unit.IsAnySubUnitInState(SubUnit.SubUnitState.Charging))
        {
            _currentDisciplineModCharging += (Time.fixedDeltaTime * DisciplineModChargingRestoreRate);
            _currentDisciplineModCharging = Mathf.Clamp(_currentDisciplineModCharging, DisciplineModChargingMax, DisciplineModChargingMin);
        }

        float proportion = unit.GetProportionOfSubUnitsInState(SubUnit.SubUnitState.Charging);
        _currentDisciplineModCharging -= (Time.fixedDeltaTime * DisciplineModChargingDecayRate * proportion);
        _currentDisciplineModCharging = Mathf.Clamp(_currentDisciplineModCharging, DisciplineModChargingMax, DisciplineModChargingMin); 
        return _currentDisciplineModCharging;
    }
}