using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DisciplineAttackIntervalModifier : PsychologyModifier
{
    public float DisciplineAttackIntervalModMin = 8f;
    public float DisciplineAttackIntervalModMax = 0f;
    public float DisciplineAttackIntervalChargeReduction = 0.5f;

    public DisciplineAttackIntervalModifier() : base("Missile Attack Interval Increase from Discipline") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        var t = unit.Discipline/100f;
        float DisciplineAttackIntervalMod = Mathf.Lerp(DisciplineAttackIntervalModMin, DisciplineAttackIntervalModMax, t);

        if(unit.IsAnySubUnitInState(SubUnit.SubUnitState.Charging))
        {
            return DisciplineAttackIntervalMod * DisciplineAttackIntervalChargeReduction;
        }

        return DisciplineAttackIntervalMod;
    }
}