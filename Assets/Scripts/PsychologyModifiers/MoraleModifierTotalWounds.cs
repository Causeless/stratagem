using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Morale Modifiers
[System.Serializable]
public class MoraleModifierTotalWounds : DisciplineAffectedModifier
{
    public float MoraleModWounds = -0.3f;

    public MoraleModifierTotalWounds() : base("Morale Impact from Total Wounds") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        float woundsPercentage = (unit.Wounds / (float)unit.GetUnitHealth()); //% of wounded soldiers
        if (woundsPercentage <= 0)
        {
            return 0f;
        }

        return unit.UnitStats.BaseMorale * (woundsPercentage * MoraleModWounds); // % of casualties - 1 * Casualty Morale Mod.
    }
}