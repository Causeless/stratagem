using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Discipline Modifiers
[System.Serializable]
public class FatigueDisciplineModifier : PsychologyModifier
{
    public float FatigueDisciplineModMax = 0f;        
    public float FatigueDisciplineModMin = -0.55f;
    public float FatigueDisciplineMod = 0f;

    public FatigueDisciplineModifier() : base("Discipline decrease from Fatigue") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.Fatigue >= 100f)
        {
            return 0f;
        }

        var t = unit.Fatigue/100f;
        FatigueDisciplineMod = Mathf.Lerp(FatigueDisciplineModMin, FatigueDisciplineModMax, t);
        return unit.UnitStats.BaseDiscipline * FatigueDisciplineMod;
    }
}