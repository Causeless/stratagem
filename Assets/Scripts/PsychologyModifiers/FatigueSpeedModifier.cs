using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Move Speed Modifiers
[System.Serializable]
public class FatigueSpeedModifier : PsychologyModifier
{
    public float FatigueMoveSpeedModMax = 0f;
    public float FatigueMoveSpeedModMin = -0.55f;
    public float FatigueMoveSpeedMod = 0f;

    public FatigueSpeedModifier() : base("Unit Speed Reduction from Fatigue") {}

    protected override float GetValueImpl(Unit unit)
    {   
        if(unit.Fatigue <= 100f && !unit._isRunning)
        {
            return 0f;
        }

        var t = unit.Fatigue/100f;
        FatigueMoveSpeedMod = Mathf.Lerp(FatigueMoveSpeedModMin, FatigueMoveSpeedModMax, t);
        return unit.UnitStats.BaseWalkSpeed * FatigueMoveSpeedMod;
    }
}