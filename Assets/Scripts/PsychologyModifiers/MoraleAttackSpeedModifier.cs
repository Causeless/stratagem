using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Attack Speed Modifier
[System.Serializable]
public class MoraleAttackSpeedModifier : PsychologyModifier
{
    public float MoraleAttackSpeedModMin= 0f;
    public float MoraleAttackSpeedModMax= 1f;
    public float MoraleAttackSpeedMod= 1f;

    public MoraleAttackSpeedModifier() : base("Attack Speed increase from Morale") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.Morale <= 0f)
        {
            return 0f;
        }

        var t = unit.Morale/100f;
        MoraleAttackSpeedMod = Mathf.Lerp(MoraleAttackSpeedModMin, MoraleAttackSpeedModMax, t);
        return unit.UnitStats.BaseAttackSpeed * MoraleAttackSpeedMod;
    }
}