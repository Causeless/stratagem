using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoraleModifierCharging : DisciplineAffectedModifier
{
    [SerializeField]
    public float MoraleModCharging = 8f;
    public float MoraleModChargingDecayRate = 0.2f;
    public float MoraleModChargingRestoreRate = 0.2f;

    private float _currentMoraleModCharging;

    public MoraleModifierCharging() : base("Morale Boost when Charging") 
    {
        _currentMoraleModCharging = MoraleModCharging;
    }

    protected override float GetValueImpl(Unit unit)
    {                     
        if(!unit.IsAnySubUnitInState(SubUnit.SubUnitState.Charging))
        {
            _currentMoraleModCharging += (Time.fixedDeltaTime * MoraleModChargingRestoreRate);
            return 0f;
        }

        _currentMoraleModCharging -= (Time.fixedDeltaTime * MoraleModChargingDecayRate); // Base modifier from charging - decay rate over time until reaches 0.
        _currentMoraleModCharging = Mathf.Clamp(_currentMoraleModCharging, 0f, MoraleModCharging);
        return _currentMoraleModCharging;
    }
}