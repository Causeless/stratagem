using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FatigueRateCharging : FatigueResistanceModifier
{
    public float FatigueChargeRateMod = -0.5f;

    public FatigueRateCharging() : base("Fatigue Rate from Charging") {}

    protected override float GetValueImpl(Unit unit)
    {
        float proportion = unit.GetProportionOfSubUnitsInState(SubUnit.SubUnitState.Charging);
        return proportion * FatigueChargeRateMod * Time.fixedDeltaTime;
    }
}