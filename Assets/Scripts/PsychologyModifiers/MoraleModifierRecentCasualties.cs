using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

//Morale Modifiers
[System.Serializable]
public class MoraleModifierRecentCasualties : DisciplineAffectedModifier
{
    public float MoraleModCasualtiesRecent = - 1f;
    public float RecentCasualtiesPeriod = 30f;
    public float MinimumCasualtiesRequired = 1f;

    [ReadOnly]
    public float RecentlySustainedCasualties = 0f;

    private int _healthPointsLastTick = -1;

    private float _avgCasualtiesOverCooldownPeriod = 0f;

    public MoraleModifierRecentCasualties() : base("Morale Impact from Recent Casualties") {}

    protected override float GetValueImpl(Unit unit)
    {
        UpdateRecentCasualties(unit);

        if(RecentlySustainedCasualties < MinimumCasualtiesRequired)
        {
            return 0f;
        }
        
        return RecentlySustainedCasualties * MoraleModCasualtiesRecent; 
    }

    private void UpdateRecentCasualties(Unit unit)
    {
        int currentHealthPoints = unit.GetUnitHealth();

        int casualtiesLastTick = Mathf.Max(_healthPointsLastTick - currentHealthPoints, 0);

        // Add on our recently sustained casualties
        RecentlySustainedCasualties += (float)casualtiesLastTick;

        // Update our rolling average
        _avgCasualtiesOverCooldownPeriod = exponentialMovingAverage(_avgCasualtiesOverCooldownPeriod, RecentlySustainedCasualties);

        // And then reduce by our avg casualties over the cooldown period, so this (approximately!) hits zero after the cooldown
        RecentlySustainedCasualties -= _avgCasualtiesOverCooldownPeriod / RecentCasualtiesPeriod;
        RecentlySustainedCasualties = Mathf.Max(RecentlySustainedCasualties, 0f);

        // If our recently sustained casualties hits zero, restore our moving average down to zero too
        // Just makes things slightly more accurate
        if (RecentlySustainedCasualties == 0f) // comparison without epsilon is safe due to Mathf.Max above
        {
            _avgCasualtiesOverCooldownPeriod = 0f;
        }

        _healthPointsLastTick = currentHealthPoints;
    }

    private float exponentialMovingAverage (float avg, float input) 
    {
        float ticksPerCooldownPeriod = RecentCasualtiesPeriod / Time.fixedDeltaTime;
        avg -= avg / ticksPerCooldownPeriod;
        avg += input / ticksPerCooldownPeriod;
        return avg;
    }
}