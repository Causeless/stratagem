using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FatigueRateIdle : FatigueResistanceModifier
{
    public float FatigueIdleRateMod = 1f;

    public FatigueRateIdle() : base("Fatigue Rate from Resting Idle") {}

    protected override float GetValueImpl(Unit unit)
    {
        if (unit.AreAllSubUnitsInState(SubUnit.SubUnitState.Idle))
        {
            return Time.fixedDeltaTime * FatigueIdleRateMod;
        }

        return 0f;
    }
}