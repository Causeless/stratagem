using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[System.Serializable]
public class DisciplineModifierBeingCharged : PsychologyModifier
{
    [SerializeField]
    public float DisciplineModChargedMin = 0f;
    public float DisciplineModChargedMax = -50f;
    public float DisciplineModChargedDecayRate = 2f;
    public float DisciplineModChargedRestoreRate = 4f;
    public float DisciplineChargedDisciplinePower = 0.1f;
    public float DisciplineChargedMoralePower = 0.2f;

    [ReadOnly]
    public float ChargeBonusAgainst = 0f;

    private float _currentDisciplineModCharged;

    public DisciplineModifierBeingCharged() : base("Discipline Decline when Charged")
    {
       _currentDisciplineModCharged = DisciplineModChargedMin; 
    }

    //THIS WILL GET REPLACED BY THE CHARGE COMMITMENT SYSTEM!
    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.UnitState == Unit.UnitStateMachine.Braced)
        {
            _currentDisciplineModCharged += (Time.fixedDeltaTime * DisciplineModChargedRestoreRate);
            _currentDisciplineModCharged = Mathf.Clamp(_currentDisciplineModCharged, DisciplineModChargedMax, DisciplineModChargedMin);
        }
        
        ChargeBonusAgainst = unit.CalculateChargePowerAgainst(DisciplineChargedDisciplinePower, DisciplineChargedMoralePower);
        _currentDisciplineModCharged -= (Time.fixedDeltaTime * (DisciplineModChargedDecayRate * ChargeBonusAgainst));
        _currentDisciplineModCharged = Mathf.Clamp(_currentDisciplineModCharged, DisciplineModChargedMax, DisciplineModChargedMin);
        
        return _currentDisciplineModCharged;
    }
}