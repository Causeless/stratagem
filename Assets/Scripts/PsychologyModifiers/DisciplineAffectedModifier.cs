using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public abstract class DisciplineAffectedModifier : PsychologyModifier
{
    public bool IsDisciplineAffected = true; 

    protected DisciplineAffectedModifier(string description) : base(description)
    { }

    public override float GetValue(Unit unit) 
    { 
        Value = GetValueImpl(unit); 

        
        if (IsDisciplineAffected)
        {
            float t = unit.Discipline/ 100f;

            Value *= Mathf.Lerp(unit._psychology.DisciplineMoraleChangeModMin, 1, t);
        }

        return Value; 
    }
}