using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CommitmentSpeedModifier : PsychologyModifier
{
    public float CommitMoveSpeedModMax = 0.5f;
    public float CommitMoveSpeedModMin = -0.5f;
    public float CommitMoveSpeedMod = 0f;

    public CommitmentSpeedModifier() : base("Unit Speed change from Charge Commitment") {}

    protected override float GetValueImpl(Unit unit)
    {   
      if(!unit._commitment.IsInChargeCommitment)
        {
            return 0f;
        }

        var t = unit._commitment.UnitCommitment / unit._commitment.MaxCommitmentModifier;
        CommitMoveSpeedMod = Mathf.Lerp(CommitMoveSpeedModMin, CommitMoveSpeedModMax, t);
        return unit.UnitStats.BaseWalkSpeed * CommitMoveSpeedMod;
    }
}