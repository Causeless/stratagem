using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Defence Chance Modifiers
[System.Serializable]
public class FatigueDefenceChanceModifier : PsychologyModifier
{
    public float FatigueDefenceChanceModMin= -0.55f;
    public float FatigueDefenceChanceModMax= 0f;
    public float FatigueDefenceChanceMod = 1f;

    public FatigueDefenceChanceModifier() : base("Defence Chance decrease from Fatigue") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.Fatigue >= 100f)
        {
            return 0f;
        }

        var t = unit.Fatigue/100f;
        FatigueDefenceChanceMod = Mathf.Lerp(FatigueDefenceChanceModMin, FatigueDefenceChanceModMax, t);
        return unit.UnitStats.BaseDefenceChance * FatigueDefenceChanceMod;
    }
}