using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DisciplineDefenceChanceModifier : PsychologyModifier
{
    public float DisciplineDefenceChanceModMin= 0f;
    public float DisciplineDefenceChanceModMax= 1f;
    public float DisciplineDefenceChanceMod = 0f;

    public DisciplineDefenceChanceModifier() : base("Defence Chance increase from Discipline") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.Discipline <= 0)
        {
            return 0f;
        }

        var t = unit.Discipline/100;
        DisciplineDefenceChanceMod = Mathf.Lerp(DisciplineDefenceChanceModMin, DisciplineDefenceChanceModMax, t);
        return unit.UnitStats.BaseDefenceChance * DisciplineDefenceChanceMod;
    }
}