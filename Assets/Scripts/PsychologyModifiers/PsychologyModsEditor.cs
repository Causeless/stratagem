using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PsychologyMods))]
[CanEditMultipleObjects]
public class PsychologyModsEditor : Editor
{
    void OnEnable()
    {
        serializedObject.Update();
        PsychologyMods mods = target as PsychologyMods;
        AddAllMods(mods);
    }

    private void AddAllMods(PsychologyMods mods)
    {
        mods.AddMoraleMods();
        mods.AddDisciplineMods();
        mods.AddFatigueMods();
        mods.AddWeaknessToCavalryMods();
        mods.AddAttackSpeedMods();
        mods.AddMissileAttackIntervalMods();
        mods.AddHitChanceMods();
        mods.AddDefenceChanceMods();
        mods.AddCritChanceMods();
        mods.AddConstitutionMods();
        mods.AddUnitSpeedMods();
    }
}