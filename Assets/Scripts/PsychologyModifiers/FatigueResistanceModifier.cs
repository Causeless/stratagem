using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MyBox;

[System.Serializable]
public abstract class FatigueResistanceModifier : PsychologyModifier
{
    public bool FatigueResistanceModified = true;

    public bool FatigueModifiedByWounds = true;

    [ReadOnly]
    public float WoundsFatigueResistanceMod;


    protected FatigueResistanceModifier(string description) : base(description)
    { }

    public override float GetValue(Unit unit) 
    { 
        Value = GetValueImpl(unit);

        if (FatigueResistanceModified)
        {
            Value *= unit._psychology.FatigueResistanceRate;
        }

        float woundsPercentage = (unit.Wounds / (float)unit.GetUnitHealth()); //% of wounded soldiers
        if (woundsPercentage > 0f)
        {
            WoundsFatigueResistanceMod = 1f;
            WoundsFatigueResistanceMod += (woundsPercentage * unit._psychology.WoundedConstitutionMod);

            // Todo, figure out how to do this per-idle-subunit instead
            // Probably get the proportion and then do a weighted average between the two possible Values?
            if (FatigueModifiedByWounds)
            {
                Value = unit.AreAllSubUnitsInState(SubUnit.SubUnitState.Idle) ? Value /= WoundsFatigueResistanceMod : Value *= WoundsFatigueResistanceMod;
            }
        }

        return Value; 
    }
}