using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Critical Chance Modifier
[System.Serializable]
public class FatigueCriticalChanceModifier : PsychologyModifier
{
    public float FatigueCriticalHitChanceModMin= -0.55f;
    public float FatigueCriticalHitChanceModMax= 0f;
    public float FatigueCriticalHitChanceMod = 0f;

    public FatigueCriticalChanceModifier() : base("Critical Chance decrease from Fatigue") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit.Fatigue >= 100f)
        {
            return 0f;
        }

        var t = unit.Fatigue/100f;
        FatigueCriticalHitChanceMod = Mathf.Lerp(FatigueCriticalHitChanceModMin, FatigueCriticalHitChanceModMax, t);
        return unit.UnitStats.BaseCriticalHitChance * FatigueCriticalHitChanceMod;
    }
}