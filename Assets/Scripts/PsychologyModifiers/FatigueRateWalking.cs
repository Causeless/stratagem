using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Fatigue Modifiers
[System.Serializable]
public class FatigueRateWalking : FatigueResistanceModifier
{
    public float FatigueWalkRateMod = -0.1f;

    public FatigueRateWalking() : base("Fatigue Rate from Walking") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (unit._isRunning)
        {
            return 0.0f;
        }

        float proportion = unit.GetProportionOfSubUnitsInState(SubUnit.SubUnitState.Moving);
        return proportion * FatigueWalkRateMod * Time.fixedDeltaTime;
    }
}