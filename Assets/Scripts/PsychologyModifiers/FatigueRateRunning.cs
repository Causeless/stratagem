using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FatigueRateRunning : FatigueResistanceModifier
{
    public float FatigueRunRateMod = -0.25f;

    public FatigueRateRunning() : base("Fatigue Rate from Running") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        if (!unit._isRunning)
        {
            return 0.0f;
        }

        float proportion = unit.GetProportionOfSubUnitsInState(SubUnit.SubUnitState.Moving);
        return proportion * FatigueRunRateMod * Time.fixedDeltaTime;
    }
}