using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FatigueRateCombat : FatigueResistanceModifier
{
    public float FatigueCombatRateMod = -0.25f;

    public FatigueRateCombat() : base("Fatigue Rate from Combat") {}

    protected override float GetValueImpl(Unit unit)
    {
        float proportion = unit.GetProportionOfSubUnitsInState(SubUnit.SubUnitState.Combat);
        return proportion * FatigueCombatRateMod * Time.fixedDeltaTime;
    }
}