using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WoundsConsitutionModifier : PsychologyModifier
{
    public float WoundedConstitutionMod = -0.75f;
    public float WoundsConsitutionMod = 0f;

    public WoundsConsitutionModifier() : base("Constitution decrease from Wounds") {}

    protected override float GetValueImpl(Unit unit)
    {                     
        float woundsPercentage = (unit.Wounds / (float)unit.GetUnitHealth()); //% of wounded soldiers
        if (woundsPercentage <= 0f)
        {
            return 0f;
        }

        WoundsConsitutionMod = woundsPercentage * WoundedConstitutionMod;
        return unit.UnitStats.BaseConsitution * WoundsConsitutionMod;
    }
}