﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineSegmentsForUnit
{
    public LinearAlgebra.LineSegment endFrontLine;
    public List<LinearAlgebra.LineSegment> segments;
}

// A unit formation represents a list of lines which a unit uses to place its sub units
public abstract class UnitFormation : MonoBehaviour {
    public LineSegmentsForUnit LineSegments;

    protected LinearAlgebra.LineSegment _line;

    protected UnitFormation()
    {
        LineSegments = new LineSegmentsForUnit();
        LineSegments.segments = new List<LinearAlgebra.LineSegment>();
    }

    public abstract LineSegmentsForUnit GetForLine(LinearAlgebra.LineSegment line, List<SubUnit> subUnitList);
    public abstract LineSegmentsForUnit StartForLine(LinearAlgebra.LineSegment line, int count);
    public abstract void SetForLine(LinearAlgebra.LineSegment line, int count);
}