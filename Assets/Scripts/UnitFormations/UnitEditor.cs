using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Unit))]
[CanEditMultipleObjects]
public class UnitEditor : Editor
{
    void OnEnable()
    {
        
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        Unit unit = target as Unit;
        if (GUILayout.Button("Spawn"))
        {
            unit.Spawn();
        }

        DrawDefaultInspector();
    }
}