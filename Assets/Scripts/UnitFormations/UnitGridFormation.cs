﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitGridFormation : UnitFormation
{
    public float MaxFormationWidthInGridSquares {
        get {
            return LineSegments.segments.Count;
        }
    }

    private Vector3 PositionAtTerrainHeight(Vector3 pos)
    {
        //cast ray to get terrain height
        RaycastHit terrainHit;
        Vector3 rayOrigin = new Vector3(pos.x, pos.y + 1.0f, pos.z);
        Ray terrainRay = new Ray(rayOrigin, Vector3.down);

        if (Physics.Raycast(terrainRay, out terrainHit, 2.0f))
        {
            return new Vector3(pos.x, terrainHit.point.y, pos.z);
        }

        NavMeshHit navHit;
        return NavMesh.SamplePosition(rayOrigin, out navHit, 5.0f, NavMesh.AllAreas) ? navHit.position : pos;
    }

    public override LineSegmentsForUnit StartForLine(LinearAlgebra.LineSegment line, int count)
    {
        GridManager gridManager = GridManager.s_instance;

        var lineSegments = gridManager.AliasToGridCorners(line, 2);

        // Remove extraneous lines at the end
        if (lineSegments.Count > count)
        {
            lineSegments.RemoveRange(count, lineSegments.Count - count);
        }

        LineSegmentsForUnit lineSegmentsForUnit = new LineSegmentsForUnit();
        lineSegmentsForUnit.endFrontLine.firstPos = lineSegments[0].firstPos;
        lineSegmentsForUnit.endFrontLine.lastPos = lineSegments[lineSegments.Count - 1].lastPos;

        // Add new rows behind if needed
        Vector3 back = gridManager.AliasToGridCorner(line.GetNormalVector() * gridManager.GridSquareSize);
        int rowSize = lineSegments.Count;
        int i = 0;
        while (lineSegments.Count < count)
        {
            int lineInFrontIndex = i % rowSize;
            LinearAlgebra.LineSegment lineInFront = lineSegments[lineInFrontIndex];

            int rowCount = (i / rowSize) + 1;
            lineSegments.Add(new LinearAlgebra.LineSegment
            {
                firstPos = lineInFront.firstPos + back * rowCount,
                lastPos = lineInFront.lastPos + back * rowCount
            });

            i++;
        }

        lineSegmentsForUnit.segments = lineSegments;
        return lineSegmentsForUnit;
    }

    public override LineSegmentsForUnit GetForLine(LinearAlgebra.LineSegment line, List<SubUnit> subUnitList)
    {
        GridManager gridManager = GridManager.s_instance;
        var lineSegments = gridManager.AliasToGridCorners(line, 2);
        int count = subUnitList.Count;

        // Remove extraneous lines at the end
        if (lineSegments.Count > count)
        {
            lineSegments.RemoveRange(count, lineSegments.Count - count);
        }

        LineSegmentsForUnit lineSegmentsForUnit = new LineSegmentsForUnit();
        lineSegmentsForUnit.endFrontLine.firstPos = lineSegments[0].firstPos;
        lineSegmentsForUnit.endFrontLine.lastPos = lineSegments[lineSegments.Count - 1].lastPos;

        // Add new rows behind if needed
        Vector3 back = gridManager.AliasToGridCorner(line.GetNormalVector() * gridManager.GridSquareSize);
        int rowSize = lineSegments.Count;
        int i = 0;
        while (lineSegments.Count < count)
        {
            int lineInFrontIndex = i % rowSize;
            LinearAlgebra.LineSegment lineInFront = lineSegments[lineInFrontIndex];

            int rowCount = (i / rowSize) + 1;
            lineSegments.Add(new LinearAlgebra.LineSegment
            {
                firstPos = lineInFront.firstPos + back * rowCount,
                lastPos = lineInFront.lastPos + back * rowCount
            });

            i++;
        }

        float[,] multiDimensionalArrayOfAllListsOfPossibleRoutes = new float[count, lineSegments.Count];

        //Get position of each subunit in the provided list, and all segments.
        for (int j = 0; j < count; j++)
        {
            Vector3 subUnitPosition = subUnitList[j].GetPosition(); 
            for (int k = 0; k < lineSegments.Count; k++)
            {
                Vector3 segmentMidpoint = lineSegments[k].GetMidpoint();
                float dist = (subUnitPosition - segmentMidpoint).sqrMagnitude;
                multiDimensionalArrayOfAllListsOfPossibleRoutes[j, k] = dist;
            }
        }

        //Run Hungarian algo to sort all subunits and segments based on distance.
        TheHungarianAlgorithm hungarianAlgorithm = new TheHungarianAlgorithm(multiDimensionalArrayOfAllListsOfPossibleRoutes);
        int[] optimalAssignment = hungarianAlgorithm.Run();
        List<LinearAlgebra.LineSegment> sortedLineSegments = new List<LinearAlgebra.LineSegment>();

        for (int j = 0; j < count; j++)
        {
            int segmentOrder = optimalAssignment[j];
            sortedLineSegments.Add(lineSegments[segmentOrder]);
        }

        lineSegmentsForUnit.segments = sortedLineSegments;
        return lineSegmentsForUnit;
    }

    public override void SetForLine(LinearAlgebra.LineSegment line, int count)
    {
        if (line == _line && count == LineSegments.segments.Count) // Optimization to simplify unit code without unnecessary recalculation
        {
            return;
        }

        _line = line;
        LineSegments = StartForLine(line, count);
    }
}