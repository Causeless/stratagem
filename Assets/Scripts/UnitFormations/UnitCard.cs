﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UnitCard : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Unit _unit;
    public Image background;
    public Text count;
    public Text morale;
    public Text discipline;
    public Text fatigue;
    public Text unitType;
    public Image _runIcon;

    private int _morale = 0;
    private int _discipline = 0;
    private int _fatigue = 0;

    DoubleClickDetector dcd;
    StrateCam sc;
    ScrollRect _scroll;

    bool isOverIU;

    private void Start()
    {
        dcd = Camera.main.GetComponent<DoubleClickDetector>();
        sc = Camera.main.GetComponent<StrateCam>();
        _scroll = GetComponentInParent<ScrollRect>();
        _runIcon.enabled = false;
        unitType.text = _unit.UnitName;
    }

    private void Update()
    {
        UpdateUnitStatUI();

        if (isOverIU)
        {
            if (dcd.IsDoubleClickLeft())
            {
                sc.GoTo(_unit.GetPrimarySubUnit().Position);
            }
            else if (Input.GetMouseButtonDown(0))
            {
                Select();
            }
        }

        if(_unit._commitment.IsInChargeCommitment)
        {
            if(_unit._commitment.IsReserveUnit)
            {
                ChangeUnitCardColour(Color.blue);
            }
            else
            {
                ChangeUnitCardColour(Color.red);
            }
            return;
        }

        if(_unit._commitment.IsReacting)
        {
            ChangeUnitCardColour(Color.yellow);
            return;
        }        

        if(background.color != Color.white)
        {   
            ChangeUnitCardColour(Color.white);
        }
    }

    public void UpdateUnitStatUI()
    {
        _morale = (int)Math.Round(_unit.Morale);
        _discipline = (int)Math.Round(_unit.Discipline);
        _fatigue = (int)Math.Round(_unit.Fatigue);

        count.text = "HP: " + _unit.HealthPoints;
        morale.text = "Morale: " + _morale;
        discipline.text = "Discipline: " + _discipline;
        fatigue.text = "Fatigue: " + _fatigue;
    }

    public void SnapToMe()
    {
        Vector2 newPosition = (Vector2)_scroll.transform.InverseTransformPoint(_scroll.content.position)
            - (Vector2)_scroll.transform.InverseTransformPoint(transform.position); ;

        float diffX = newPosition.x - _scroll.content.anchoredPosition.x;
        if (Mathf.Abs(diffX) < _scroll.viewport.rect.width * 0.5f)
        {
            // We're already in the viewport, don't do anything
            return;
        }

        // make sure we don't skip past the edges of the viewport
        float halfContentSize = _scroll.content.sizeDelta.x * 0.5f;
        newPosition.x = Mathf.Clamp(newPosition.x, -halfContentSize, halfContentSize);
        newPosition.y = _scroll.content.anchoredPosition.y;

        Canvas.ForceUpdateCanvases();
        _scroll.content.anchoredPosition = newPosition;
    }

    public void ChangeUnitCardColour(Color color)
    {
        background.color = color; 
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOverIU = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isOverIU = false;
    }

    public void Select()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            if (EntitySelector._selectedUnits.Contains(_unit))
            {
                EntitySelector.ClearSelection(_unit);
            }
            else
            {
                EntitySelector.SelectUnit(_unit);
            }
        }
        else
        {
            EntitySelector.ClearSelections();
            EntitySelector.SelectUnit(_unit);
        }
    }
}
