﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPanel : MonoBehaviour {

    public GameObject unitCardPrefab;

    public void Reset()
    {
        foreach (Transform child in transform)
        {
            Destroy(child);
        }
    }

    public void AddUnitCard(Unit unit)
    {
        GameObject card = Instantiate(unitCardPrefab, transform, false);
        card.GetComponent<UnitCard>()._unit = unit;
    }
}
