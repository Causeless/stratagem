using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

// Public facing modifiers by Psychology stats against other unit stats.
public partial class FormationMods : MonoBehaviour
{
    public string _modName = "Default";
    public float FormationModDiscipline = 0f;
    public float FormationModDefenceChance = 0f;
    public float FormationModMissileDefenceChance = 0f;
    public float FormationModConstitution = 0f;
    public float FormationModWeaknessToCavalry = 0f;

    public void ApplyFormationMods(Unit unit)
    {
        unit.Discipline += FormationModDiscipline;
        unit.DefenceChance += FormationModDefenceChance;
        unit.MissileDefenceChance += FormationModMissileDefenceChance;
        unit.Consitution += FormationModConstitution;
        unit.WeaknessToCavalry += FormationModWeaknessToCavalry;
    }
}