using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEditorInternal;
using System;
using UnityEngine.Rendering;
using System.Linq;

// A unit is a collection of subunits.
// A unit is the atomic controllable gameplay element for a battle,
// Whereas a subunit is the atomic simulation element,
// And entities are purely dumb followers that have no simulation impact
// As such soldier count can be indefinitely scaled without changing the gameplay
public partial class Unit : MonoBehaviour, ICachedPosition
{
    private List<Command> _commandQueue = new List<Command>();
    [ReadOnly]
    public bool UnitActive = false;
    public Faction FriendlyFaction;
    public Unit CommandUnit;
    private string FactionString;
    public GameObject SubUnitPrefab;
    [ReadOnly]
    public string UnitName;
    [ReadOnly]
    public string OfficerName;
    [Header("Unit UI")]
    [SerializeField] private SpriteRenderer UnitIndicator;
    [SerializeField] private SpriteRenderer UnitBackground;
    [SerializeField] private SpriteRenderer UnitIcon;
    public UnitIconsConfig UnitIconList;
    [Header("Scriptable Objects")]
    public OfficerNames OfficerNames;
    public UnitStatsConfig UnitStats;

    public float SubUnitIndependentMovementDistance = 60f;
    public float SubUnitRallyDistance = 32f;

    [HideInInspector]   
    public UnitFormation _formation;

    [HideInInspector]
    public PsychologyMods _psychology;

    [HideInInspector]
    public UnitPowerLevels _unitPower;    

    [HideInInspector]
    public UnitChargeCommitment _commitment;
    [HideInInspector]
    public UnitStateManager _state;
    [HideInInspector]
    public UnitEventManager _events;
    private UnitMergingManager _merge;
    [HideInInspector]
    public UnitAggression _aggression;    
    [HideInInspector] 
    public Unit_SpatialPartitionGrid _grid;

    [HideInInspector]
    public BoundingBoxRenderer _boundingBoxRender;
    public bool UnitSelected = false;

    public float BaseWeaknessToCavalry; //Should replace this later with bonuses from one unit type versus other unit types. Cav should be weak against spears ect.

    [Header("Current Values")]
    public int HealthPoints;
    public int Wounds;
    public int UnitAmmo;
    public float ReloadTime;
    public float MissileAttackInterval;
    public float Morale;
    public float Discipline;
    public float Fatigue;
    public float WeaknessToCavalry;
    public float AttackSpeed;
    public float HitChance;
    public float MissileHitChance;
    public float DefenceChance;
    public float MissileDefenceChance;
    public float CriticalHitChance;
    public float MissileCriticalHitChance;
    public float Consitution;
    public float WalkSpeed;
    public float RunSpeed;
    public float ChargeSpeed;
    public float SprintTime; //Units can only sprint for a time, fatigue reduces this.
    [ReadOnly]
    public float _sprintTimer = 0f;
    [HideInInspector]
    public bool _sprintExpired = false;

    [Header("Power Levels")]
    public float PowerLevelMeleeAttack;
    public float PowerLevelMeleeDefense;
    public float PowerLevelMissileAttack;
    public float PowerLevelMissileDefense;
    public float PowerLevelPsych;

    // Using an enum for this isn't great, we'll want to automatically generate this from the subunit formations
    public enum UnitSpecialFormation
    {
        Default, //Normal formation.
        Testudo, //Testudo for use against missiles and sieges.
        Shieldwall, //For Germanic "Phalanxes" mentioned in Caesar's annals.
    }

    [Header("Formation")]
    public UnitSpecialFormation SpecialFormation;
    public float FormationOrder = 0.7f;
    public float FormationRandomness = 0f;

    public enum UnitChargeBehaviour //Will probably be replaced by aggression
    {
        Hold, //Don't Charge. Hold position. -morale, +discipline
        Charge, //Charge with restrictions, +morale, -discipline
        Fallback //Don't charge, fall back from enemy units. --morale, --discipline
    }

    public enum UnitAggressionLevel
    {
        VeryCautious,
        Cautious,
        Steady,
        Aggressive,
        VeryAggressive,
    }

    public enum UnitStateMachine
    {
        Default,
        Ready,
        Braced,
        Charging,
        Combat,
        Disengage,
        Retreat,
        Rout,
    }

    [Header("Unit Behaviours")]
    public UnitAggressionLevel Aggression;
    [ReadOnly]
    public UnitStateMachine UnitState;
    private UnitStateMachine OldUnitState;
    public UnitChargeBehaviour ChargeBehaviour;
    private UnitChargeBehaviour OldChargeBehaviour;
    public UnitShootingBehaviour ShootingBehaviour;
    private UnitShootingBehaviour OldShootingBehaviour;
    public float ChargeRange = 60f;
    public float ShootRange = 25f;
    public float ChargeCommitmentRange = 60f;
    [ReadOnly]
    public float _distanceToFrontline = 0f;
    [ReadOnly]
    public float _chargeOrder = 0f;

    [Header("Unit Positional Info")]
    [ReadOnly]
    public Vector3 UnitPosition;
    [ReadOnly]
    private Vector3 _oldPosition;
    [ReadOnly]
    public Quaternion UnitRotation;
    private bool AddedToFactionSpatialGrid = false;
    [ReadOnly]
    public bool HostileInChargeRange = false;    

    // ICachedPosition stuff
    public Vector3 GetPosition()
    {
        return UnitPosition;
    }

    public Quaternion GetRotation()
    {
        return UnitRotation;
    }

    public Vector3 GetOldPosition()
    {
        return _oldPosition;
    }

    [ReadOnly]
    public int SubUnitWidth;
    [ReadOnly]
    public int SubUnitDepth;
    //Positional Debugging
    private float MinWidth;
    private float MaxWidth;
    private float MinDepth;
    private float MaxDepth;
    
    public enum UnitShootingBehaviour
    {
        HoldFire, //Don't Fire
        FireAtWill, //Fire
        ChargePrecursor, // Only Fire when charging
    }

    [HideInInspector]
    public bool _isRunning = false;

    public bool IsRunning
    {
        get
        { 
            return _isRunning; 
        }
    }

    [HideInInspector]
    public bool _isSprinting = false;

    public bool IsSprinting
    {
        get
        { 
            return _isSprinting; 
        }
    }    

    private List<SubUnit> _subUnits = new List<SubUnit>();
    public List<SubUnit> SubUnits
    {
        get
        {
            return _subUnits;
        }
    }

    private void AddComponents()
    {
        _formation = GetComponent<UnitFormation>();
        _psychology = GetComponent<PsychologyMods>();
        _unitPower = GetComponent<UnitPowerLevels>();
        _commitment = GetComponent<UnitChargeCommitment>();
        _aggression = GetComponent<UnitAggression>();
        _events = GetComponent<UnitEventManager>();
        _state = GetComponent<UnitStateManager>();
        _merge = GetComponent<UnitMergingManager>();
        _grid = GameObject.FindWithTag("SpatialPartitionGrid").GetComponent<Unit_SpatialPartitionGrid>();
        _boundingBoxRender = GetComponent<BoundingBoxRenderer>();

        Debug.Assert(_formation != null, "Formation data missing for unit!");
        Debug.Assert(_psychology != null, "Psychology data missing for unit!");
        Debug.Assert(_unitPower != null, "Power level data missing for unit!");
        Debug.Assert(_commitment != null, "Charge commimtent data missing for unit!");
        Debug.Assert(_grid != null, "Unit Spatial Partition Grid missing!");
        Debug.Assert(_boundingBoxRender != null, "Bounding box data missing for unit!");
    }

    public void Spawn()
    {        
        AddComponents();
        RemovePreviousSubUnits();

        UnitName = UnitStats.unitName;
        SubUnitPrefab = UnitStats.subUnitPrefab;
        int numberOfSubunits = UnitStats.BaseHealthPoints / UnitStats.BaseHealthPointsPerSubUnit;
        UnitStats.HealthPointsPerEntity = 4;

        HealthPoints = UnitStats.BaseHealthPoints;
        Morale = UnitStats.BaseMorale;
        Discipline = UnitStats.BaseDiscipline;
        Fatigue = UnitStats.BaseFatigue;
        WeaknessToCavalry = BaseWeaknessToCavalry;
        Wounds = 0;
        ChargeSpeed = UnitStats.BaseChargeSpeed;
        SprintTime = UnitStats.BaseSprintTime;
        UnitAmmo = UnitStats.BaseUnitAmmo * numberOfSubunits;
        ReloadTime = UnitStats.BaseReloadTime;

        _subUnits.Clear();
        _commandQueue.Clear();

        // This should be different from HP, to allow scaling of entities
        int entityCount = UnitStats.BaseHealthPointsPerSubUnit / UnitStats.HealthPointsPerEntity;

        // Pick an front line based on our direction/facing
        int numberOfRows = 2;
        float frontLineLength = Mathf.Max(numberOfSubunits/numberOfRows, 1) * GridManager.s_instance.GridSquareSize;

        Vector3 toSide = new Vector3(frontLineLength * 0.5f, 0.0f, 0.0f);
        toSide = transform.TransformDirection(toSide);

        LinearAlgebra.LineSegment frontLine = new LinearAlgebra.LineSegment
        {
            firstPos = transform.position - toSide,
            lastPos = transform.position + toSide
        };

        var unitFormation = GetComponent<UnitFormation>();
        var unitLines = unitFormation.StartForLine(frontLine, numberOfSubunits);

        for (int i = 0; i < numberOfSubunits; i++)
        {
            GameObject subUnitGameObject = Instantiate(SubUnitPrefab, GameObject.FindWithTag("SubUnit Collection").transform, false);
            subUnitGameObject.transform.position = unitLines.segments[i].GetMidpoint();

            SubUnit subUnit = subUnitGameObject.GetComponent<SubUnit>();
            subUnit._parentUnit = this;

            // As to why I need to set this subunit as enabled, even though I do not need to do the same with entity...
            // I have no clue.
            // Leave me alone.
            subUnit.enabled = true;
            subUnit.Spawn(entityCount, unitLines.segments[i]);

            AddSubUnit(subUnit);
        }

        UpdateFormation();
        ChargeSpeed = UnitStats.BaseChargeSpeed;
        UpdateUnitPosition();

        GameObject.FindWithTag("Unit Panel").GetComponent<UnitPanel>().AddUnitCard(this);    
        
        DetermineFaction();
        OfficerName = GetOfficerName();
        initialiseUnitBehaviours();
        ColourIconByFaction();
        SetUnitIcon();

        if (_boundingBoxRender != null)
        {
            _boundingBoxRender.InitializeBoundingBoxVisual();
        }

        AddUnitToFaction();
        UnitActive = true;
        Debug.Log($"{UnitName} under officer {OfficerName} spawned in Faction {FactionString}");
    }

    private void RemovePreviousSubUnits()
    {
        if (_subUnits.Count > 0)
        {
            foreach (SubUnit subUnit in _subUnits)
            {
                foreach (Entity entity in subUnit.Entities)
                {
                    entity.DestroyEntity();
                } 
                subUnit.Entities.Clear();

                subUnit.DestroySubUnit();               
            }
            _subUnits.Clear();
        }
    }

    private void AddUnitToFaction()
    {
        if(FriendlyFaction.Units == null)
        {
            Debug.LogError("Units table doesn't exist for faction yet!");
        }
        FriendlyFaction.Units.Add(this);
        _grid.Add(this);    
    }

    public void RemoveSubUnit(SubUnit subUnit)
    {
        _subUnits.Remove(subUnit);
    }

    public void AddSubUnit(SubUnit subUnit)
    {
        _subUnits.Add(subUnit);
    }

    private void ColourIconByFaction()
    {
        if(FriendlyFaction == null)
        {
            Debug.LogWarning("Unit faction colour returns null");                        
        }
        
        UnitBackground.color = FriendlyFaction.FactionColour;
    }

    private void DetermineFaction()
    {
        if (FriendlyFaction != null)
        {
            FactionString = FriendlyFaction.name;
        }
        else
        {
            Debug.Assert(FriendlyFaction != null, "Faction is not set for this unit!");
            FactionString = "Neutral"; 
        }
    } 
    private string GetOfficerName()
    {
        if (OfficerNames.officerNameList.Count == 0) 
        {
            return "Ignotus"; //Latin for unknown.
        }
        return OfficerNames.officerNameList[UnityEngine.Random.Range(0, OfficerNames.officerNameList.Count)];
    }   

    private void SetUnitIcon()
    {
        if(UnitStats.Type == UnitStatsConfig.UnitType.Infantry)
        {
            if(FriendlyFaction != null && FriendlyFaction.name == "Romans")
            {
                UnitIcon.sprite = UnitIconList.romanIcon;
            }
            else
            {
                UnitIcon.sprite = UnitIconList.infantryIcon;
            }
        }

        if(UnitStats.Type == UnitStatsConfig.UnitType.Cavalry)
        {
            UnitIcon.sprite = UnitIconList.cavalryIcon;
        }

        if(UnitStats.Type == UnitStatsConfig.UnitType.InfantrySkirmisher)
        {
            UnitIcon.sprite = UnitIconList.infantrySkirmisherIcon;
        }   

        if(UnitStats.Type == UnitStatsConfig.UnitType.CavalrySkirmisher)
        {
            UnitIcon.sprite = UnitIconList.cavalrySkirmisherIcon;
        }     
    }


    //Update stats based on Psychology Modifiers
    private void UpdateStats()
    {
        // Update Modified Stats
        UpdateFormation();
        ChargeSpeed = UnitStats.BaseChargeSpeed;
        SprintTime = UnitStats.BaseSprintTime;
        UpdatePowerLevels();
    }

    public int GetUnitHealth()
    {
        int healthPoints = 0;

        foreach (SubUnit subUnit in _subUnits)
        {
            healthPoints += subUnit._healthPoints;
        }
        return healthPoints;
    }

    private void UpdateFormation()
    {
        var t = Discipline/100f;

        float inverseDiscipline = 1f - t;

        FormationOrder = Mathf.Lerp(_psychology.DisciplineFormationModMin, _psychology.DisciplineFormationModMax, t);

        float initialFormationRandomness = Mathf.Lerp(_psychology.DisciplineRandomnessModMax, _psychology.DisciplineRandomnessModMin, t);

        FormationRandomness = initialFormationRandomness * inverseDiscipline; // To make it non-linear

        SetSubUnitFormationStats(_subUnits);
    }

    private void UpdatePowerLevels()
    {
        PowerLevelMeleeAttack = _unitPower.CalculateMeleeAttackPower();
        PowerLevelMissileAttack = _unitPower.CalculateMissileAttackPower();
        PowerLevelMeleeDefense = _unitPower.CalculateMeleeDefencePower();
        PowerLevelMissileDefense = _unitPower.CalculateMissileDefencePower();
        PowerLevelPsych = _unitPower.CalculatePsychPower();

        foreach (SubUnit subUnit in _subUnits)
        {
            subUnit.SubUnitPowerLevelMeleeAttack = PowerLevelMeleeAttack / _subUnits.Count;
            subUnit.SubUnitPowerLevelMissileAttack = PowerLevelMissileAttack / _subUnits.Count;
            subUnit.SubUnitPowerLevelMeleeDefense = PowerLevelMeleeDefense / _subUnits.Count;
            subUnit.SubUnitPowerLevelMissileDefense = PowerLevelMissileDefense / _subUnits.Count;
            subUnit.SubUnitPowerLevelPsych = PowerLevelPsych / _subUnits.Count;
        }
    }

    public float AverageUnitSpeed()
    {
        float averagespeed = 0f;
        foreach (var subUnit in _subUnits)
        {
            averagespeed += subUnit.Velocity.magnitude; 
        }

        return averagespeed / _subUnits.Count;
    }

    public bool IsAnySubUnitInState(SubUnit.SubUnitState state)
    {
        foreach (var subUnit in _subUnits)
        {
            if (subUnit.State == state)
            {
                return true;
            }
        }

        return false;
    }

    public bool AreAllSubUnitsInState(SubUnit.SubUnitState state)
    {
        foreach (var subUnit in _subUnits)
        {
            if (subUnit.State != state)
            {
                return false;
            }
        }

        return true;
    }

    public float GetProportionOfSubUnitsInState(SubUnit.SubUnitState state)
    {
        int num = 0;

        foreach (var subUnit in _subUnits)
        {
            if (subUnit.State == state)
            {
                num++;
            }
        }

        float proportion = (float)num / (float)SubUnits.Count;
        return proportion;
    }

    public float GetProportionOfSubUnitsBeingCharged()
    {
        int num = 0;

        foreach (var subUnit in _subUnits)
        {
            if (subUnit.IsBeingCharged())
            {
                num++;
            }
        }

        float proportion = (float)num / (float)SubUnits.Count;
        return proportion;
    }

    public float CalculateChargePowerAgainst(float chargePowerDisciplineMultiplier, float chargePowerMoraleMultiplier)
    {
        float chargePowerDiscipline = 0f;
        float chargePowerMorale = 0f;
         
        foreach(SubUnit subUnit in _subUnits)
        {
            if (subUnit.IsBeingCharged())
            {
                foreach(Faction faction in Faction.AllFactions)
                {
                    foreach(SubUnit.ChargeTask chargeTask in faction.GetChargeAllocations(subUnit))
                    {
                        Unit attacker = chargeTask._subUnit._parentUnit;
                        SubUnit subAttacker = chargeTask._subUnit;
                        float chargePowerCavalry = 1f;

                        if (subAttacker.Type == SubUnit.SubUnitType.Cavalry)
                        {
                            chargePowerCavalry += CalculateCavalryChargePowerAgainst(subAttacker); 
                        }

                        chargePowerDiscipline *= chargePowerCavalry;
                        chargePowerMorale *= chargePowerCavalry;    
                        chargePowerDiscipline += (attacker.Discipline * 0.1f / attacker.SubUnits.Count);
                        chargePowerMorale += (attacker.Morale * 0.1f / attacker.SubUnits.Count);
                    }
                }
            }
        }

        return (chargePowerDiscipline * chargePowerDisciplineMultiplier + chargePowerMorale * chargePowerMoraleMultiplier) / 2f;
    }

    public float CalculateCavalryChargePowerAgainst(SubUnit attacker)
    {
        float chargePower = 0f;

        if (attacker.Role == SubUnit.SubUnitRole.Melee)
        {
            chargePower += WeaknessToCavalry;
        }
        if (attacker.Role == SubUnit.SubUnitRole.Missile)
        {
            chargePower += WeaknessToCavalry * 0.75f; // This shouldn't be a magic number really, but I've no idea where to define it.
        }
        return chargePower;
    }

    private void SetSubUnitFormationStats(List<SubUnit> SubUnitList)
    {
        foreach(var subUnit in SubUnitList)
        {
            subUnit._subUnitOrder = FormationOrder;
            subUnit._subUnitRandomness = FormationRandomness;
        }
    }

    public void SetRun(bool enabled)
    {
        _isRunning = enabled;
        // todo, we shouldn't really do this; subunits should move in a particular formation and not 100% independently
        foreach(SubUnit subUnit in _subUnits)
        {
            subUnit.SetRun(enabled);
        }
    }

    public void SetSprint(bool enabled)
    {
        _isSprinting = enabled;
        foreach(SubUnit subUnit in _subUnits)
        {
            subUnit.SetSprint(enabled);
        }
    }    
    
    public SubUnit GetPrimarySubUnit()
    {
        if (_subUnits.Count == 0)
        {
            return null;
        }

        return _subUnits[0];
    }

    private void FixedUpdate()
    {
        if(!UnitActive)
        {
            return;
        }
        FriendlyFaction.UpdateEmptyList();
        UpdateUnitPosition();
        UpdateUnitRotation();
        CalculateSubUnitFormation();
        RoutCheck();
        SetSpecialFormations();
        UpdateCommands();
        UpdateStats();
        HealthPoints = GetUnitHealth();
        UpdateUnitIcon();

        ChangeUnitStateEvent(); //IF WE'RE CALLING THESE FOR OTHER SCRIPTS, WHY FIXEDUPDATE HERE? THEY MIGHT BE DUPLICATED.
        ChangeChargeBehaviourEvent();
        ChangeShootBehaviourEvent();
        
        //Create Visual Bounding box based on faction
        _boundingBoxRender.UpdateBoundingBoxRender(this, null, 0f, FriendlyFaction.FactionColour);
        UpdateUnitSpatialGrids();

        RunSprintTimer();
    }

    private void UpdateUnitIcon()
    {
        if(UnitIndicator == null)
        {
            return;
        }
        
        if(UnitSelected)
        {
            UnitIndicator.color = Color.yellow;
            return;
        }

        if(_commitment.IsInChargeCommitment)
        {
            if(_commitment.IsReserveUnit)
            {
                UnitIndicator.color = Color.blue;
            }
            else
            {
                UnitIndicator.color = Color.red;
            }
            return;
        }

        if(_commitment.IsReacting)
        {
            UnitIndicator.color = Color.green;
            return;
        }        

        if(UnitIndicator.color != Color.white)
        {   
            UnitIndicator.color = Color.white;
        }
    }

    private void RunSprintTimer()
    {
        if(_isSprinting)
        {
            _sprintTimer += Time.deltaTime;
            _sprintExpired = false;

            if(_sprintTimer > SprintTime)
            {
                SetSprint(false);
                _sprintExpired = true;
                _sprintTimer = 0f;
            }
        }
    }

    public bool HostileUnitInRange(float range)
    {
        foreach(Faction faction in Faction.AllFactions)
        {
            //Only check spatial grid for enemy factions
            if(FriendlyFaction.IsHostile(faction))
            {
                // If any enemy units in range, return true
                if(faction.FactionUnitGrid.GetEntsInRange(UnitPosition, range).Any())
                {
                    return true;
                }
            }
        }

        return false;
    }
    
    public bool AlliedUnitInRange(float range)
    {
        foreach(Faction faction in Faction.AllFactions)
        {
            //Only check spatial grid for enemy factions
            if(FriendlyFaction.IsAllied(faction))
            {
                // Find all units in range - if in range, bool is true, change unit state!
                List<Unit> nearbyUnits = faction.FactionUnitGrid.GetEntsInRange(UnitPosition, range);
                if(nearbyUnits.Count > 1)
                {
                    return true;
                }
            }
        }

        return false;
    }    

    public void SetInCombatInterrupt()
    {
        // This is called when the subunit is attacked in melee
        // TODO, properly use a state machine for this shit...
        foreach(SubUnit subUnit in _subUnits)
        {
            if (subUnit.TaskQueue.Count == 0 ||
                (subUnit.TaskQueue.Count > 0 && 
                !(subUnit.TaskQueue[0] is SubUnit.CombatTask) &&
                !(subUnit.TaskQueue[0] is SubUnit.ChargeTask)) )
            {
                subUnit.ClearQueuedTasks();
                subUnit.QueueTask(new SubUnit.CombatTask());
            }
        }
    }

    public void UpdateUnitPosition()
    {
        // If subunits are null or 0, return.
        if (_subUnits == null || _subUnits.Count == 0)
        {
            UnitPosition = Vector3.zero;
            return;
        }
        
        Vector3 sumPosition = Vector3.zero;
        int subUnitCount = _subUnits.Count;

        foreach (SubUnit subUnit in _subUnits)
        {
            sumPosition += subUnit.GetPosition();
        }

        UnitPosition = sumPosition / subUnitCount;

        HasUnitMoved(); //Start check to see if unit has moved (for spatial grid)
    }

    public bool HasUnitMoved()
    {
        if(Vector3.Distance(_oldPosition, UnitPosition) > GridManager.s_instance.GridSquareSize / 2)
        {
            if(_grid == null)
            {
                _oldPosition = UnitPosition;
                return true;
            }
            
            _grid.Move(this);
            this.transform.position = UnitPosition;
            _oldPosition = UnitPosition;
            return true;
        }

        return false;
    }

    public void UpdateUnitSpatialGrids()
    {
        if (FriendlyFaction.FactionUnitGrid == null)
        {
            return;
        }

        if(!AddedToFactionSpatialGrid)
        {
            AddedToFactionSpatialGrid = true;
            FriendlyFaction.FactionUnitGrid.Add(this);
        }

        if(_oldPosition != UnitPosition)
        {
            FriendlyFaction.FactionUnitGrid.Move(this);
        }
    }

    private void UpdateUnitRotation()
    {
        if (_subUnits == null || _subUnits.Count == 0)
        {
            UnitRotation = Quaternion.identity;
            return;
        }

        //Set all values to 0
        Vector3 averageAxis = Vector3.zero;
        float averageAngle = 0f;
        int count = _subUnits.Count;

        foreach (SubUnit subUnit in _subUnits)
        {
            Quaternion rotation = subUnit.GetRotation();
            rotation.ToAngleAxis(out float angle, out Vector3 axis); //converts quat to an axis-angle
            
            //Need to average out the axis and angle otherwise the angles get messed up due to different subunit positions
            axis.Normalize();
            averageAxis += axis * angle; 
            averageAngle += angle;
        }

        if (averageAxis.sqrMagnitude > 0.0001f)
        {
            averageAxis.Normalize();
            averageAngle /= count; 
            
            UnitRotation = Quaternion.AngleAxis(averageAngle, averageAxis); //convert axis and angles back to quat
        }
        else
        {
            UnitRotation = Quaternion.identity;
        }
    }

    private void CalculateSubUnitFormation()
    {
        if (_subUnits == null || _subUnits.Count == 0)
        {
            SubUnitWidth = 0;
            SubUnitDepth = 0;
            return;
        }

        float _gridThreshold = 0.6f;

        Quaternion inverseRotation = Quaternion.Inverse(UnitRotation);

        MinWidth = float.MaxValue; 
        MaxWidth = float.MinValue;
        MinDepth = float.MaxValue;
        MaxDepth = float.MinValue;

        // Go through each subunit get their position and rotate to match the above.
        foreach (SubUnit subUnit in _subUnits)
        {
            Vector3 subUnitPosition = subUnit.GetPosition();

            Vector3 localPosition = inverseRotation * (subUnitPosition - UnitPosition);

            if (localPosition.x < MinWidth) MinWidth = localPosition.x;
            if (localPosition.x > MaxWidth) MaxWidth = localPosition.x;

            if (localPosition.z < MinDepth) MinDepth = localPosition.z;
            if (localPosition.z > MaxDepth) MaxDepth = localPosition.z;
        }

        // Calculate total length and depth based on localized subunit positions
        float unitWidth = MaxWidth - MinWidth;
        float unitDepth = MaxDepth - MinDepth;    

        // Convert length and depth to grid units (divide by grid size)
        SubUnitWidth = (int)Mathf.Round((unitWidth / GridManager.s_instance.GridSquareSize) + _gridThreshold);
        SubUnitDepth = (int)Mathf.Round((unitDepth / GridManager.s_instance.GridSquareSize) + _gridThreshold);
    }

    public bool IsUnitFrontlineWidth(Vector3 targetPosition)
    {
        Quaternion inverseRotation = Quaternion.Inverse(UnitRotation);
        Vector3 localPosition = inverseRotation * (targetPosition - UnitPosition);

        if (Mathf.Abs(localPosition.x) > Mathf.Abs(localPosition.z))
        {
            // Closer to the X axis, so the unit should use depth in formation
            return false; 
        }
        else
        {
            // Closer to the Z axis, so the unit should use width in formation
            return true; 
        }
    }        

    public BoundingBox GetBoundingBox(float scaleFactor)
    {
        float unitWidth = (SubUnitWidth * GridManager.s_instance.GridSquareSize)
        + (scaleFactor * GridManager.s_instance.GridSquareSize);
        float unitDepth = (SubUnitDepth * GridManager.s_instance.GridSquareSize)
        + (scaleFactor * GridManager.s_instance.GridSquareSize);

        Vector3 size = new Vector3(unitWidth, 1, unitDepth);

        // Return the bounding box with unit position, size, and rotation
        return new BoundingBox(UnitPosition, size, UnitRotation);
    }     

    private void RoutCheck()
    {
        if(Morale < 1f && Discipline < 1f)
        {
            ChargeBehaviour = UnitChargeBehaviour.Fallback;
            ShootingBehaviour = UnitShootingBehaviour.HoldFire;
        }    
    }

    public void SetCombatReadyState(bool enable)
    {
        var width = enable ? SubUnit.SubUnitFormationWidth.StretchedGrid : SubUnit.SubUnitFormationWidth.Square;
        
        foreach (SubUnit subUnit in _subUnits)
        {
            if (subUnit._subUnitFormationWidth != width)
            {
                subUnit._subUnitFormationWidth = width;
                subUnit.MarkFormationDirty();
            }
            
            subUnit.SetCombatState(enable);
        }        
    }

    public void initialiseUnitBehaviours()
    { 
        OldUnitState = UnitState;
        OldChargeBehaviour = ChargeBehaviour;
        OldShootingBehaviour = ShootingBehaviour; 
    }
    public bool ChangeUnitStateEvent()
    {
        if(UnitState != OldUnitState)
        {
            OldUnitState = UnitState;
            return true;
        }

        return false;
    }

    public bool ChangeChargeBehaviourEvent()
    {
        if(OldChargeBehaviour != ChargeBehaviour)
        {
            OldChargeBehaviour = ChargeBehaviour;
            return true;
        }

        return false;
    }   

    public bool ChangeShootBehaviourEvent()
    {
        if(OldShootingBehaviour != ShootingBehaviour)
        {
            OldShootingBehaviour = ShootingBehaviour;
            return true;
        }

        return false;
    }         

    public void SetSpecialFormations() //Should add a way to make formations that the unit doesn't have greyed out and unavailable to select.
    {
        if(SpecialFormation == UnitSpecialFormation.Default)
        {
            foreach(SubUnit subUnit in _subUnits)
            {
                subUnit.ChangeIntoFormation("Default");     
            }  
        }

        if(SpecialFormation == UnitSpecialFormation.Testudo)
        {
            foreach(SubUnit subUnit in _subUnits)
            {
                subUnit.ChangeIntoFormation("Testudo");
            }  
        }

        if(SpecialFormation == UnitSpecialFormation.Shieldwall)
        {
            foreach(SubUnit subUnit in _subUnits)
            {
                subUnit.ChangeIntoFormation("Shieldwall");     
            }  
        }
    }

    public void EnableSelectionHighlight(bool enable)
    {
        foreach (SubUnit subUnit in _subUnits)
        {
            subUnit.EnableSelectionHighlight(enable);
        }
    }

    // Todo; create a generic CommandQueue/TaskQueue that can be used for both Unit/SubUnit
    private void UpdateCommands()
    {
        if (_commandQueue.Count == 0)
        {
            return;
        }

        // update our current command
        if (!_commandQueue[0].IsComplete)
        {
            _commandQueue[0].Update();
        }
        else // or pop it if it's complete
        {
            _commandQueue.RemoveAt(0);

            // If there's still commands left, apply it
            if (_commandQueue.Count > 0)
            {
                _commandQueue[0].Apply();
            }
        }
    }

    public void QueueCommand(Command command)
    {
        command._unit = this;
        _commandQueue.Add(command);

        // we need to immediately apply if it's the only command
        if (_commandQueue.Count == 1)
        {
            _commandQueue[0].Apply();
        }
    }

    public void ClearQueuedCommands()
    {
        _commandQueue.Clear();
        foreach (SubUnit subUnit in _subUnits)
        {
            subUnit.ClearQueuedTasks();
        }
    }
}
