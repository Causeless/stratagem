﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using LinearAlgebra;
using MyBox;

public partial class SubUnit : MonoBehaviour, ICachedPosition
{
    private List<Task> _taskQueue = new List<Task>();

    public List<Task> TaskQueue
    {
        get 
        {
            return _taskQueue;
        }
    }

    [ReadOnlyAttribute]
    public Vector2Int CurrentGridSquare;

    public Unit _parentUnit;
    public float _subUnitOrder = 0.7f;
    public float _subUnitRandomness = 0f;
    public int _healthPoints;
    public float _combatDistance = 1;
    public float _combatDistanceVariance = 0;

    [ReadOnly]
    public SubUnit _chargeTarget;

    [Header("Power Levels")]
    public float SubUnitPowerLevelMeleeAttack;
    public float SubUnitPowerLevelMeleeDefense;
    public float SubUnitPowerLevelMissileAttack;
    public float SubUnitPowerLevelMissileDefense;
    public float SubUnitPowerLevelPsych;
    
    [HideInInspector]
    public bool _isRunning = false;

    [HideInInspector]
    public bool _isSprinting = false;
    private float _currentMovementSpeed;
    public float CurrentMovementSpeed
    {
        get
        {
            return _currentMovementSpeed;
        }
    }

    public enum SubUnitState
    {
        Idle,
        Moving,
        Combat,
        Charging,
    }
    public SubUnitState State { get; set; }

    public enum SubUnitFormationWidth
    {
        Square, // Move in a square block
        StretchedGrid, // Stretch to match grid square width
        StretchedGoal, // Stretch to match the width of our goal line
        Invalid // Used as an internal code default value for overrides
    }

    public SubUnitFormationWidth _subUnitFormationWidth = SubUnitFormationWidth.Square;

    public enum SubUnitType
    {
        Cavalry,
        Infantry,
    }
    
    public SubUnitType Type;

    public enum SubUnitRole
    {
        Melee,
        Missile,
    }
    
    public SubUnitRole Role;

    [HideInInspector] // Goddamn I'm a shit coder
    public SubUnit_SpatialPartitionGrid _grid;

    private Vector3 _oldPosition;

    public Vector3 Position {
        get {
            return transform.position;
        }
    }

    public Quaternion Rotation {
        get {
            return transform.rotation;
        }
    }    

    private Vector3 _velocity;
    public Vector3 Velocity {
        get {
            return _velocity;
        }
    }

    private Formation _formation;
    private bool _formationDirty; // Determines if the subunit's formation is dirty (i.e from entity being added/removed) and needs to be recalculated

    private Coordinator _coordinator;

    public LineSegment _endGoalLine;

    public Transform _markerPrefab;

    public GameObject EntityPrefab;
    private List<Entity> _entities = new List<Entity>();
    public List<Entity> Entities
    {
        get
        {
            return _entities;
        }
    }

    private float _reloadCooldown = 1f;
    private float _attackCooldown = 0f;

    private CombatLog combatLog = CombatLog.instance;

    private float _projectileSpawnDelay = 0.0f;

    public GameObject SimProjectilePrefab;

    // ICachedPosition stuff
    public Vector3 GetPosition()
    {
        return Position;
    }

    public Quaternion GetRotation()
    {
        return Rotation;
    }

    public Vector3 GetOldPosition()
    {
        return _oldPosition;
    }

    private void Awake()
    {
        _grid = GameObject.FindWithTag("SpatialPartitionGrid").GetComponent<SubUnit_SpatialPartitionGrid>();
    }

    private void Start()
    {
        Init();
        _grid.Add(this);
    }

    public void Init()
    {
        _formationDirty = true;

        _projectileSpawnDelay = GetProjectileSpawnDelay();

        SetRun(false);
        SetSprint(false);
        ChangeIntoFormation("Default");
        CleanFormation();
        UpdatePositionAndVelocity();
    }

    public Formation GetFormation()
    {
        return _formation;
    }

    public void ChangeIntoFormation(string formationName)
    {
        var formations = GetComponents<Formation>();
        var formationMods = GetComponents<FormationMods>();
        if (formations.Length == 0)
        {
            Debug.Assert(false, "Subunit needs at least one formation!");
            return;
        }

        Formation chooseFormation = null;
        foreach (var formation in formations)
        {
            if (formation._name == formationName)
            {                
                chooseFormation = formation;
            }
        }

        foreach (var formationMod in formationMods)
        {
            if (formationMod._modName == formationName)
            {
                formationMod.ApplyFormationMods(_parentUnit);
            }
        }

        if (chooseFormation == null)
        {
            Debug.Assert(false, "Chose a formation that doesn't exist for this subunit!");
            return;
        } 

        // If we're already in the formation, or it's invalid, do nothing
        if (_formation == chooseFormation || !CanBeInFormation(chooseFormation))
        {
            return;
        }

        _formation = chooseFormation;
        _formation.SetForLine(GetAdjustedEndGoalLine(_endGoalLine, Entities.Count), Entities.Count);
        MarkFormationDirty();
    }

    private bool CanBeInFormation(Formation formation)
    {
        bool invalid = (!formation.CanUseFormationDuringCharge && State == SubUnitState.Charging) ||
                       (!formation.CanUseFormationInCombat && State == SubUnitState.Combat) ||
                       (formation.MinimumDisciplineToSustainFormation >= _parentUnit.Discipline);
        return !invalid;
    }

    public bool IsBeingCharged()
    {
        foreach(Faction faction in Faction.AllFactions)
        {
            if (faction.GetChargeAllocations(this).Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public void Spawn(int numberOfEntities, LineSegment frontLine)
    {
        _endGoalLine = frontLine;

        _coordinator = GetComponent<Coordinator>();

        ChangeIntoFormation("Default");
        _formation.SetForLine(GetAdjustedEndGoalLine(frontLine, numberOfEntities), numberOfEntities);

        List<FormationSpot> formationSpots = _formation.Spots;

        _entities.Clear();
        for (int i = 0; i < numberOfEntities; i++)
        {
            GameObject entityGameObject = Instantiate(EntityPrefab, GameObject.FindWithTag("Entity Collection").transform, false);
            entityGameObject.transform.position = formationSpots[i].position;
            entityGameObject.transform.rotation = Quaternion.LookRotation(formationSpots[i].direction, Vector3.up); ;

            Entity entity = entityGameObject.GetComponent<Entity>();
            AddEntity(entity);
        }

        this.transform.position = _formation.Centre;

        _healthPoints = _parentUnit.UnitStats.BaseHealthPointsPerSubUnit;
    }

    public void AddEntity(Entity ent)
    {
        _formationDirty = true;

        // Todo, when an entity is added, we should enable it's selection highlight if applicable

        ent.ClearAnimState();
        ent._parentSubUnit = this;
        _entities.Add(ent);
    }

    public void RemoveEntity(Entity ent)
    {
        _formationDirty = true;

        ent.EnableSelectionHighlight(false);

        ent.ClearAnimState();
        ent._parentSubUnit = null;
        _entities.Remove(ent);
    }

    public void SetRun(bool enabled)
    {
        _isRunning = enabled;
    }

    public void SetSprint(bool enabled)
    {
        _isSprinting = enabled;
    }

    private void TryPerformMeleeAttack(SubUnit target)
    {
        target._parentUnit.SetInCombatInterrupt();

        _attackCooldown -= Time.fixedDeltaTime;
        while (_attackCooldown < 0f)
        {
            _attackCooldown += 60f / _parentUnit.AttackSpeed; // Add attack delay (attacks per minute, 60 seconds)
            AttemptAttack(target, false);
        }
    }

    private void AttemptAttack(SubUnit target, bool isMissileAttack)
    {
        
        float CombatHitChance = isMissileAttack ? _parentUnit.MissileHitChance : _parentUnit.HitChance;
        float CombatCriticalHitChance = isMissileAttack ? _parentUnit.MissileCriticalHitChance : _parentUnit.CriticalHitChance;


        float hitChance = Random.Range(float.Epsilon, 100f);
        if(hitChance > CombatHitChance)
        {
            if(_parentUnit.UnitSelected)
            {
                combatLog.Log(_parentUnit + "Hit failed: " + hitChance.ToString("F0") + "/" + CombatHitChance.ToString("F0"));
            }
            return;
        }

        float critChance = Random.Range(float.Epsilon, 100f);
        bool isCrit = critChance <= CombatCriticalHitChance;
        if(_parentUnit.UnitSelected)
        {
            combatLog.Log(_parentUnit + (isCrit ? "<color=red>Critical Hit </color>" : "<color=orange>Non-critical Hit </color>") + critChance.ToString("F0") + "/" + CombatCriticalHitChance.ToString("F0"));
        }

        target.AttemptDefend(this, isCrit, isMissileAttack);
        // TODO add Melee attack to pushing threshold.
        // TODO add Missile attack to suppression threshold. Units with higher discipline will fire in shorter volleys, doing more hits at once, therefore doing more suppression. Defender should look at Attacker Discipline level directly.

    }
      
    private void AttemptDefend(SubUnit attacker, bool isCrit, bool isMissileDefence)
    {
        float combatDefenceChance = isMissileDefence ? _parentUnit.MissileDefenceChance : _parentUnit.DefenceChance;
        float defenceChance = Random.Range(float.Epsilon, 100f);
        if(defenceChance <= combatDefenceChance)
        {
            if(_parentUnit.UnitSelected)
            {
                combatLog.Log(_parentUnit + " <color=blue>Hit Blocked: </color>" + defenceChance.ToString("F0") + "/" + combatDefenceChance.ToString("F0"));
            }
            return;
        }

        float combatConstitution = _parentUnit.Consitution;
        if(isCrit)
        {
            // Crit Succeeds, do we kill or wound the soldier?
            float killChance = Random.Range(float.Epsilon, 100f);
            if(killChance > combatConstitution)
            {
                if(_parentUnit.UnitSelected)
                {
                    combatLog.Log(_parentUnit + " <color=red>Critical Hit Sustained. Soldier Killed: </color>" + killChance.ToString("F0") + "/" + combatConstitution.ToString("F0"));
                }
                this.KillBy(attacker);
            }
            else
            {
                if(_parentUnit.UnitSelected)
                {
                    combatLog.Log(_parentUnit + " <color=orange>Critical Hit Resisted. Wound Added: </color>" + killChance.ToString("F0") + "/" + combatConstitution.ToString("F0"));
                }
                this.WoundBy(attacker);
            }

            // We've crit, no need to check further
            return;
        }

        // Crit fails, check to see if it wounds
        float constitutionChance = Random.Range(float.Epsilon, 100f);
        if(constitutionChance > combatConstitution)
        {
            if(_parentUnit.UnitSelected)
            {
                combatLog.Log(_parentUnit + " <color=orange>Non-Critical Hit Sustained (soldier wounded): </color>" + constitutionChance.ToString("F0") + "/" + combatConstitution.ToString("F0"));
            }
            this.WoundBy(attacker);
        }
        else
        {
            if(_parentUnit.UnitSelected)
            {
                combatLog.Log(_parentUnit + " <color=blue>Non-Critical Hit Resisted: </color>" + constitutionChance.ToString("F0") + "/" + combatConstitution.ToString("F0"));
            }
        }
    }

    private void WoundBy(SubUnit attacker)
    {
        _parentUnit.Wounds++;
        _parentUnit.Wounds = Mathf.Clamp(_parentUnit.Wounds, 0, _parentUnit.HealthPoints);
    }

    private void KillBy(SubUnit attacker)
    {
        if (_healthPoints <= 0)
        {
            // This can happen with high attack speeds, as the unit will attack multiple times a tick
            // So just guard against it
            return;
        }

        _healthPoints--;

        float woundsPercentage = _parentUnit.Wounds / (float)_parentUnit.GetUnitHealth();
        float chanceWeKilledAWoundedGuy = Random.Range(float.Epsilon, 1f);
        if (chanceWeKilledAWoundedGuy <= woundsPercentage)
        {
            _parentUnit.Wounds--;
        }

        Mathf.Clamp(_parentUnit.Wounds, 0, _parentUnit.GetUnitHealth());

        if ((_healthPoints % _parentUnit.UnitStats.HealthPointsPerEntity) == 0)
        {
            // todo, we'll change this to have some more proper logic
            Entities[0].Kill();
        }

        if (_healthPoints <= 0)
        {
            DestroyMyself();
        }
    }

    public void ReceiveSingleMissileAttackFrom(SubUnit attacker)
    {
        attacker.AttemptAttack(this, true);
    }

    // Returns whether or not we destroyed ourself by giving all entities away
    private bool GiftHealthPointsTo(SubUnit recipient, int healthPointsToGift = int.MaxValue)
    {
        healthPointsToGift = Mathf.Min(healthPointsToGift, _healthPoints);

        // Give them our healthpoints
        recipient._healthPoints += healthPointsToGift;

        // Now increase our targets healthpoints, transferring entities where required
        // We don't do a normal divide, because we need to account for edge cases (i.e imagine that we gift 1 hp 4 times over; we need to transfer an entity)
        for (int i = 0; i < healthPointsToGift; i++)
        {
            _healthPoints--;
            if (_healthPoints % _parentUnit.UnitStats.HealthPointsPerEntity == 0)
            {
                Entity e = Entities[0];
                RemoveEntity(e);
                recipient.AddEntity(e);
            }
        }

        if (Entities.Count == 0)
        {
            DestroyMyself();
            return true;
        }

        return false;
    }

    private void DestroyMyself()
    {
        _grid.Remove(this);
        _parentUnit.RemoveSubUnit(this);
        Destroy(gameObject);
    }

    public void DestroySubUnit()
    {
        _grid.Remove(this);
        Destroy(gameObject);
    }

    public void SetCombatState(bool enabled)
    {
        foreach (Entity entity in _entities)
        {
            if(enabled)
            {
                if(State == SubUnitState.Combat)
                {
                    entity.AttackAnimSpeed = Mathf.InverseLerp(0f, 100f, _parentUnit.Morale) + 0.5f;
                    entity.AttackAnimOffset = Mathf.InverseLerp(100f, 0f, _parentUnit.Discipline);
                    entity.SetAnimState(Entity.AnimState.Attack);
                }
                else
                {
                    entity.SetAnimState(Entity.AnimState.Combat);
                }
            }
            else
            {
                entity.SetAnimState(Entity.AnimState.Idle);
            }
        }
    }

    private void TryPerformProjectileAttack()
    {
        if (SimProjectilePrefab == null)
        {
            // can't missile attack
            return;
        }

        _reloadCooldown -= Time.fixedDeltaTime;
        _reloadCooldown = Mathf.Clamp(_reloadCooldown, 0f, _parentUnit.ReloadTime);

        // Only attempt shooting if we are allowed
        bool allowedToShoot = _parentUnit.ShootingBehaviour == Unit.UnitShootingBehaviour.FireAtWill ||
                              (_parentUnit.ShootingBehaviour == Unit.UnitShootingBehaviour.ChargePrecursor && State == SubUnitState.Charging);
        if (!allowedToShoot)
        {
            return;
        }

        if (State == SubUnitState.Combat)
        {
            return;
        }

        if(_parentUnit.UnitAmmo == 0 || _reloadCooldown > 0f)
        {
            return;
        }

        // If we're charging, then always fire at whoever we're charging towards
        SubUnit targetSubUnit = null;
        if (State == SubUnitState.Charging)
        {
            List<SubUnit> subUnitsInShootRange = _grid.GetEntsInRange(GetPosition(), _parentUnit.ShootRange);

            List<SubUnit> nearbyHostileSubUnits = new List<SubUnit>();
            foreach (SubUnit candidate in subUnitsInShootRange)
            {
                if (_parentUnit.FriendlyFaction.IsHostile(candidate._parentUnit.FriendlyFaction))
                {
                    nearbyHostileSubUnits.Add(candidate);
                }
            }

            if (nearbyHostileSubUnits.Count > 0)
            {
                int rnd = Random.Range(0, nearbyHostileSubUnits.Count);
                targetSubUnit = nearbyHostileSubUnits[rnd];
            }
        }
        else // Otherwise pick a random enemy subunit in range. 
        // Matt: We should use the new Power Level ratings to pick enemy subunits to attack, with a priority on high melee/missile power level units with low missile defence power.
        {
            List<SubUnit> subUnitsInShootRange = _grid.GetEntsInRange(GetPosition(), _parentUnit.ShootRange);

            List<SubUnit> nearbyHostileSubUnits = new List<SubUnit>();
            foreach (SubUnit candidate in subUnitsInShootRange)
            {
                if (_parentUnit.FriendlyFaction.IsHostile(candidate._parentUnit.FriendlyFaction))
                {
                    bool inCombat = candidate._parentUnit.UnitState == Unit.UnitStateMachine.Combat;
                    if(!inCombat)
                    {
                        nearbyHostileSubUnits.Add(candidate);
                    }
                }
            }

            if (nearbyHostileSubUnits.Count > 0)
            {
                int rnd = Random.Range(0, nearbyHostileSubUnits.Count);
                targetSubUnit = nearbyHostileSubUnits[rnd];
            }
        }

        if (targetSubUnit)
        {
            PerformProjectileAttack(targetSubUnit);
        }
    }

    // Fuck me, what a mess
    private float GetProjectileSpawnDelay()
    {
        if (SimProjectilePrefab == null)
        {
            // can't missile attack
            return 0.0f;
        }

        else
        {
            Entity ent = _entities[0];

            var throwAnim = ent._prototype.clipList.Find(clip => clip.name.Contains("IdleThrowEvent"));

            if(throwAnim != null)
            {
                float animTime = ent._crowdAnimator.GetAnimationTime(throwAnim);
                return animTime;
            }
            else
            {
                return 0.0f;
            }
        }
    }

    private void PerformProjectileAttack(SubUnit targetSubUnit)
    {
        _reloadCooldown += _parentUnit.ReloadTime;

        // We'll need to think a bit about how to handle ammo; i.e a subunit dies/routs, will the parent unit lose that proportion of ammo?
        _parentUnit.UnitAmmo -= 1;

        float missileIntervalMax = _parentUnit.MissileAttackInterval;

        // Spawn a sim projectile and fire it at our target
        GameObject simProjectile = Instantiate(SimProjectilePrefab, GetPosition(), Quaternion.identity) as GameObject;
        simProjectile.GetComponent<SimProjectile>().Initialize(this, targetSubUnit, _projectileSpawnDelay, missileIntervalMax);


        foreach (Entity entity in _entities)
        {
            // Start the attack at a random time during our missile attack interval
            float time = Random.Range(0f, missileIntervalMax);
            StartCoroutine(entity.StartProjectileAttack(targetSubUnit, time));
        }
    }

    public void MarkFormationDirty()
    {
        _formationDirty = true;
    }

    public void SetEndGoalLine(LineSegment lineSegment)
    {
        if (_endGoalLine != lineSegment)
        {
            _endGoalLine = lineSegment;
            MarkFormationDirty();
            CleanFormation();
        }
    }

    public Vector3 GetMovementGoalPosition()
    {
        return _formation.Centre;
    }

    public void SetSubUnitFormationWidth(SubUnitFormationWidth subUnitFormationWidth)
    {
        if (_subUnitFormationWidth == subUnitFormationWidth)
        {
            return;
        }

        _subUnitFormationWidth = subUnitFormationWidth;
        MarkFormationDirty();
    }

    public LineSegment GetAdjustedEndGoalLine(SubUnitFormationWidth subUnitFormationWidthOverride = SubUnitFormationWidth.Invalid)
    {
        return GetAdjustedEndGoalLine(_endGoalLine, Entities.Count, subUnitFormationWidthOverride);
    }

    public LineSegment GetAdjustedEndGoalLine(LineSegment endGoalLine, int numberOfEntities, SubUnitFormationWidth subUnitFormationWidthOverride = SubUnitFormationWidth.Invalid)
    {
        float frontLineLength = 1.0f;

        SubUnitFormationWidth widthOption = subUnitFormationWidthOverride != SubUnitFormationWidth.Invalid ? subUnitFormationWidthOverride : _subUnitFormationWidth;
        switch(widthOption)
        {
            case SubUnitFormationWidth.Square:
                float sqrtEntityCount = Mathf.Sqrt(numberOfEntities);
                frontLineLength = _formation._horizontalSpacing * sqrtEntityCount - (_formation._horizontalSpacing * 0.25f); // eugh magic number
                break;
            case SubUnitFormationWidth.StretchedGrid:
                // Grid square width
                float gridSquareSize = GridManager.s_instance.GridSquareSize;
                frontLineLength = gridSquareSize;// - _formation._horizontalSpacing; // but cut off a formation space (half from either side)
                break;
            case SubUnitFormationWidth.StretchedGoal:
                // End goal line length
                frontLineLength = endGoalLine.GetLength();// - _formation._horizontalSpacing; // but cut off a formation space (half from either side)
                break;
            default:
                // Uh oh
                Assert.IsTrue(false);
                break;
        }

        Vector3 midPoint = endGoalLine.GetMidpoint();
        Vector3 halfDir = endGoalLine.GetDirection() * frontLineLength * 0.5f;
        return new LineSegment
        {
            firstPos = midPoint - halfDir,
            lastPos = midPoint + halfDir
        };
    }

    private void CleanFormation()
    {
        if (!_formationDirty)
        {
            return;
        }

        _formation.SetForLine(GetAdjustedEndGoalLine(), _entities.Count);
        _formationDirty = false;    

        _coordinator.ResetFormation();
    }

    private void FixedUpdate()
    {
        // If we can't be in the formation as circumstances have changed, then go back into default
        if (!CanBeInFormation(_formation))
        {
            ChangeIntoFormation("Default");
        }
        
        // If we cannot run while in this formation, disable running.
        if (!_formation.CanRunWhileInFormation && _isRunning)
        {
            SetRun(false); 
        }

        UpdateMovementSpeed();
        SlowDownToAvoidOtherSubUnits();

        UpdateTasks();

        // We might've destroyed ourselves, for example via a merge
        // So early out...
        if (Entities.Count == 0)
        {
            return;
        }

        CleanFormation();
        UpdatePositionAndVelocity();

        TryPerformProjectileAttack();

        CurrentGridSquare = GridManager.s_instance.GetGridSquareCoordinatesForPosition(GetPosition());
    }

    private void UpdateMovementSpeed()
    {
        if(_isSprinting)
        {
            _currentMovementSpeed = _parentUnit.ChargeSpeed;
            return;
        }
        
        _currentMovementSpeed = _isRunning ? _parentUnit.RunSpeed : _parentUnit.WalkSpeed;
    }

    private void SlowDownToAvoidOtherSubUnits()
    {
        // If we're moving to the same place as another unit, and there's a unit in front of us, slow down
        // Todo; if we are significantly faster than the unit in front of us, move around them instead
        // Potential todo: limit ourselves to their speed or less, instead of just slowing down an arbritrary amount. If multiplier is 0.0f, of course, then we just stop moving
        float distanceThresholdForSpeedReduction = GridManager.s_instance.GridSquareSize;

        float currentMaxMovementSpeed = _currentMovementSpeed;

        List<SubUnit> nearbySubUnits = _grid.GetEntsInRange(Position, distanceThresholdForSpeedReduction);
        foreach (SubUnit nearby in nearbySubUnits)
        {
            if (nearby == this)
            {
                continue;
            }

            // Only consider slowing down if they are also moving
            if (nearby.State != SubUnitState.Moving)
            {
                continue;
            }

            // let's figure out if we're moving in the same direction
            if (Velocity == Vector3.zero ||
                Vector3.Angle(nearby.Velocity, this.Velocity) > 120.0f)
            {
                continue;
            }

            // let's determine if we are behind them. We're behind them if our velocity is facing towards them
            Vector3 toThem = nearby.Position - Position;
            if (Vector3.Angle(Velocity, toThem) > 45.0f)
            {
                continue;
            }

            // Finally, if we're here, all checks have passed, so reduce our speed to match theirs
            float speedProportionifOurUnit = 0.8f; // Go a little slower, so if we're a too close we'll give some distance
            float speedProportionifOtherUnit = 0.01f;

            float speedProportion = nearby._parentUnit == this._parentUnit ? speedProportionifOurUnit : speedProportionifOtherUnit;
            currentMaxMovementSpeed = Mathf.Min(currentMaxMovementSpeed, nearby._currentMovementSpeed * speedProportion);
        }

        _currentMovementSpeed = currentMaxMovementSpeed;
    }

    private struct DistanceInfo
    {
        public SubUnit subUnit;
        public float distanceSqr;
    }

    private void UpdateTasks()
    {
        if (_taskQueue.Count == 0)
        {
            State = SubUnitState.Idle;
            return;
        }

        // update our current task
        if (!_taskQueue[0].IsComplete)
        {
            _taskQueue[0].Update();
        }
        else // or pop it if it's complete
        {
            _taskQueue[0].OnComplete();
            _taskQueue.RemoveAt(0);

            // If there's still tasks left, apply it
            if (_taskQueue.Count > 0)
            {
                _taskQueue[0].Apply();
            }
        }
    }

    public void QueueTask(Task task)
    {
        task._subUnit = this;
        _taskQueue.Add(task);

        // we need to immediately apply if it's the only task
        if (_taskQueue.Count == 1)
        {
            _taskQueue[0].Apply();
        }
    }

    public void ClearQueuedTasks()
    {
        foreach (Task task in _taskQueue)
        {
            task.Cancel();
        }
    }

    public void EnableSelectionHighlight(bool enable)
    {
        foreach (Entity e in _entities)
        {
            e.EnableSelectionHighlight(enable);
        }
    }

    private void UpdatePositionAndVelocity()
    {
        _oldPosition = transform.position;

        // Set our position to be closer to our target by our current movement speed
        Vector3 toDestination = _formation.Centre - transform.position;
        float currentSpeedThisTick = _currentMovementSpeed * Time.fixedDeltaTime;
        if (toDestination.sqrMagnitude <= currentSpeedThisTick * currentSpeedThisTick) // We'll be there this tick, just teleport directly
        {
            transform.position = _formation.Centre;
        }
        else // Move by our current movement speed towards the destination
        {
            transform.position += toDestination.normalized * _currentMovementSpeed * Time.fixedDeltaTime;
        }

        _velocity = (transform.position - _oldPosition) / Time.fixedDeltaTime;

        _grid.Move(this);
    }
}
