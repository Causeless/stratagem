using UnityEngine;

public class UnitStateManager : MonoBehaviour
{
    private Unit _unit;
    
    // Start is called before the first frame update
    void Start()
    {
        _unit = GetComponent<Unit>();
    }

    void FixedUpdate()
    {
        UpdateUnitState();
        UnitStateEffects();
    }

    //How does a unit transition between states?

    private void UpdateUnitState()
    {       
        _unit.HostileInChargeRange = _unit.HostileUnitInRange(_unit.ChargeRange);

        if(!_unit.HostileInChargeRange && !_unit._commitment.IsInChargeCommitment)
        {
            _unit.UnitState = Unit.UnitStateMachine.Default;
            return;
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Default && _unit.HostileInChargeRange || _unit.UnitState == Unit.UnitStateMachine.Default && _unit._commitment.IsInChargeCommitment == true)
        {
            _unit.UnitState = Unit.UnitStateMachine.Ready;
            return;
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Ready)
        {
            if(_unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Charge && _unit._commitment.IsInChargeCommitment == true)
            {
                _unit.UnitState = Unit.UnitStateMachine.Charging;
                return;
            }

            if(_unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Hold && _unit._commitment.IsInChargeCommitment == true)
            {
                _unit.UnitState = Unit.UnitStateMachine.Braced;
                return;
            }

            if(_unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Fallback && _unit._commitment.IsInChargeCommitment == true)
            {
                _unit.UnitState = Unit.UnitStateMachine.Retreat;
                return;
            }       
        }

        if(_unit._commitment.IsInChargeCommitment == false)
        {
            _unit.UnitState = Unit.UnitStateMachine.Ready;
            return;
        }

        if (_unit.IsAnySubUnitInState(SubUnit.SubUnitState.Combat)) 
        {
            if(_unit._commitment.Breakoff)
            {
                _unit.UnitState = Unit.UnitStateMachine.Disengage;
                return;
            }

            _unit.UnitState = Unit.UnitStateMachine.Combat;
            return;
        }

        if(_unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Charge && _unit._commitment.IsInChargeCommitment == true)
        {
            if(_unit._commitment.ChargeHalted == true)
            {
                _unit.UnitState = Unit.UnitStateMachine.Braced; 
                return;
            }
        }

        if(_unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Charge && _unit._commitment.IsInChargeCommitment == true)
        {
            if(_unit._commitment.ChargeHalted == false)
            {
                _unit.UnitState = Unit.UnitStateMachine.Charging; 
                return;
            }
        }
    }

    //What happens when a unit is in a state?
    private void UnitStateEffects()
    {
        if(_unit.UnitState == Unit.UnitStateMachine.Default)
        {
            _unit.SetCombatReadyState(false);
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Ready)
        {
            _unit._commitment.CreateChargeCommitment(); //Creates first charge commitment and assigns frontline.
            _unit.SetCombatReadyState(true);
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Braced)
        {
            _unit.SetCombatReadyState(true);
            _unit.SetRun(true);
            foreach(SubUnit subUnit in _unit.SubUnits)
            {
                if(subUnit.TaskQueue.Count < 1)
                {
                    subUnit.QueueTask(new SubUnit.CombatTask());
                }                
            }   

            if(_unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Charge)
            {
                _unit.ChargeBehaviour = Unit.UnitChargeBehaviour.Hold;
            }
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Charging)
        {
            _unit.SetCombatReadyState(true);
            _unit.SetSprint(true);
            foreach(SubUnit subUnit in _unit.SubUnits)
            {
                subUnit.State = SubUnit.SubUnitState.Charging;
                if(subUnit.TaskQueue.Count < 1)
                {
                    subUnit.QueueTask(new SubUnit.CombatTask());
                }
            }    
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Combat)
        {
            _unit.SetCombatReadyState(true);          
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Disengage)
        {
            bool isDisengaging = false;
            _unit.SetCombatReadyState(true);  
            if(_unit.ChargeBehaviour == Unit.UnitChargeBehaviour.Charge) //TODO: Check if unit should chase. 
            {
                _unit.ChargeBehaviour = Unit.UnitChargeBehaviour.Hold;
            }
            if(!isDisengaging)
            {
                foreach(SubUnit subUnit in _unit.SubUnits)
                {
                    _unit.ClearQueuedCommands();
                    subUnit.State = SubUnit.SubUnitState.Idle;
                }  
            }
            
            _unit._commitment.ChargeHalted = true;
  
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Retreat)
        {
            _unit.SetCombatReadyState(true);  
            _unit.SetSprint(true); 
            _unit.ChargeBehaviour = Unit.UnitChargeBehaviour.Fallback;
        }

        if(_unit.UnitState == Unit.UnitStateMachine.Rout)
        {
            _unit.SetCombatReadyState(false);
            _unit.SetSprint(true);
            _unit.ChargeBehaviour = Unit.UnitChargeBehaviour.Fallback;
            _unit.ShootingBehaviour = Unit.UnitShootingBehaviour.HoldFire;
        }
    }
}
