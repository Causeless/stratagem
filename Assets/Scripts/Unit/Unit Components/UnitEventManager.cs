using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class UnitEventManager : MonoBehaviour
{
    private Unit _unit;
    public float BaseMoraleMultiplier = 1f;
    public float MoraleMultiplier;
    public float Casualties = 0f;
    public bool BattleStarted = false; 
    
    // A list of modifiable stats that user can target when creating Unit Stat Modifications
    public enum ModifiableStats 
    {
        HealthPoints,
        Wounds,
        UnitAmmo,
        ReloadTime,
        MissileAttackInterval,
        Morale,
        Discipline,
        Fatigue,
        WeaknessToCavalry,
        AttackSpeed,
        HitChance,
        MissileHitChance,
        DefenceChance,
        MissileDefenceChance,
        CriticalHitChance,
        MissileCriticalHitChance,
        Consitution,
        WalkSpeed,
        RunSpeed,
        ChargeSpeed,
        SprintTime, 
        ChargeRange,
        CommitmentRange,
        MoraleEffectMultiplier,
        TotalCasualties,
        RecentCasualties,
    }

    //I'm worried about these enums becoming obsolete if the referenced data ends up changing elsewhere (states, event functions being renamed/removed ect).
    public enum UnitEventTriggers 
    {
        BattleStart, 
        DefaultState, ReadyState, BracedState, ChargeState, CombatState, DisengageState, RetreatState, RoutState, 
        Aggresion0, Aggression1, Aggression2, Aggression3, Aggression4, Aggression5,
        InChargeCommitment, ChargeHalted, UnitFlanking, UnitFlanked, UnitinReserve, UnitReacting,
        
    }    

    public Dictionary<ModifiableStats, float> currentStats = new Dictionary<ModifiableStats, float>();
    public Dictionary<ModifiableStats, float> baseStats = new Dictionary<ModifiableStats, float>();
    public Dictionary<UnitEventTriggers, bool> unitEventDict = new Dictionary<UnitEventTriggers, bool>();

    private void Awake()
    {
        _unit = GetComponent<Unit>();
        MoraleMultiplier = BaseMoraleMultiplier;

        // Initialize dictionary with current and base unit stats
        SetBaseStats();
        SetCurrentStats();
        SetEventTriggerEnums();
        BattleStarted = true;
        SetEventTrigger(UnitEventTriggers.BattleStart, true);
    }
    
    private void SetBaseStats()
    {
        baseStats = new Dictionary<ModifiableStats, float>
        {
            { ModifiableStats.HealthPoints, _unit.UnitStats.BaseHealthPoints },
            { ModifiableStats.Wounds, _unit.Wounds },
            { ModifiableStats.UnitAmmo, _unit.UnitStats.BaseUnitAmmo },
            { ModifiableStats.ReloadTime, _unit.UnitStats.BaseReloadTime },
            { ModifiableStats.MissileAttackInterval, _unit.UnitStats.BaseMissileAttackInterval },
            { ModifiableStats.Morale, _unit.UnitStats.BaseMorale },
            { ModifiableStats.Discipline, _unit.UnitStats.BaseDiscipline },
            { ModifiableStats.Fatigue, _unit.UnitStats.BaseFatigue },
            { ModifiableStats.WeaknessToCavalry, _unit.BaseWeaknessToCavalry },
            { ModifiableStats.AttackSpeed, _unit.UnitStats.BaseAttackSpeed },
            { ModifiableStats.HitChance, _unit.UnitStats.BaseHitChance },
            { ModifiableStats.MissileHitChance, _unit.UnitStats.BaseMissileHitChance },
            { ModifiableStats.DefenceChance, _unit.UnitStats.BaseDefenceChance },
            { ModifiableStats.MissileDefenceChance, _unit.UnitStats.BaseMissileDefenceChance },
            { ModifiableStats.CriticalHitChance, _unit.UnitStats.BaseCriticalHitChance },
            { ModifiableStats.MissileCriticalHitChance, _unit.UnitStats.BaseMissileCriticalHitChance },
            { ModifiableStats.Consitution, _unit.UnitStats.BaseConsitution },
            { ModifiableStats.WalkSpeed, _unit.UnitStats.BaseWalkSpeed },
            { ModifiableStats.RunSpeed, _unit.UnitStats.BaseRunSpeed },
            { ModifiableStats.ChargeSpeed, _unit.UnitStats.BaseChargeSpeed },
            { ModifiableStats.SprintTime, _unit.UnitStats.BaseSprintTime },
            { ModifiableStats.ChargeRange, _unit.UnitStats.ChargeRange },
            { ModifiableStats.CommitmentRange, _unit.UnitStats.CommitmentRange },
            { ModifiableStats.MoraleEffectMultiplier, BaseMoraleMultiplier },
        };

        //Debug.Log("Base Stats Set");
    }

    private void SetCurrentStats()
    {
            currentStats = new Dictionary<ModifiableStats, float>
        {
            { ModifiableStats.HealthPoints, _unit.HealthPoints },
            { ModifiableStats.Wounds, _unit.Wounds },
            { ModifiableStats.UnitAmmo, _unit.UnitAmmo },
            { ModifiableStats.ReloadTime, _unit.ReloadTime },
            { ModifiableStats.MissileAttackInterval, _unit.MissileAttackInterval },
            { ModifiableStats.Morale, _unit.Morale },
            { ModifiableStats.Discipline, _unit.Discipline },
            { ModifiableStats.Fatigue, _unit.Fatigue },
            { ModifiableStats.WeaknessToCavalry, _unit.BaseWeaknessToCavalry },
            { ModifiableStats.AttackSpeed, _unit.AttackSpeed },
            { ModifiableStats.HitChance, _unit.HitChance },
            { ModifiableStats.MissileHitChance, _unit.MissileHitChance },
            { ModifiableStats.DefenceChance, _unit.DefenceChance },
            { ModifiableStats.MissileDefenceChance, _unit.MissileDefenceChance },
            { ModifiableStats.CriticalHitChance, _unit.CriticalHitChance },
            { ModifiableStats.MissileCriticalHitChance, _unit.MissileCriticalHitChance },
            { ModifiableStats.Consitution, _unit.Consitution },
            { ModifiableStats.WalkSpeed, _unit.WalkSpeed },
            { ModifiableStats.RunSpeed, _unit.RunSpeed },
            { ModifiableStats.ChargeSpeed, _unit.ChargeSpeed },
            { ModifiableStats.SprintTime, _unit.SprintTime },
            { ModifiableStats.ChargeRange, _unit.UnitStats.ChargeRange },
            { ModifiableStats.CommitmentRange, _unit.UnitStats.CommitmentRange },
            { ModifiableStats.MoraleEffectMultiplier, MoraleMultiplier },
            { ModifiableStats.TotalCasualties, Casualties },
        };

        //Debug.Log("Current Stats Set");
    }    

    private void SetEventTriggerEnums()
    {
        unitEventDict = new Dictionary<UnitEventTriggers, bool>
        {
            { UnitEventTriggers.BattleStart, BattleStarted },
            { UnitEventTriggers.DefaultState, UnitinState(Unit.UnitStateMachine.Default) },
            { UnitEventTriggers.ReadyState, UnitinState(Unit.UnitStateMachine.Ready) },
            { UnitEventTriggers.ChargeState, UnitinState(Unit.UnitStateMachine.Charging) },
            { UnitEventTriggers.CombatState, UnitinState(Unit.UnitStateMachine.Combat) },
            { UnitEventTriggers.BracedState, UnitinState(Unit.UnitStateMachine.Braced) },
            { UnitEventTriggers.DisengageState, UnitinState(Unit.UnitStateMachine.Disengage) },
            { UnitEventTriggers.RetreatState, UnitinState(Unit.UnitStateMachine.Retreat) },
            { UnitEventTriggers.RoutState, UnitinState(Unit.UnitStateMachine.Rout) },
        };
    }

    public float GetBaseStatValue(ModifiableStats stat)
    {
        return baseStats.ContainsKey(stat) ? baseStats[stat] : 0f;
    } 

    public float GetCurrentStatValue(ModifiableStats stat)
    {
        return currentStats.ContainsKey(stat) ? currentStats[stat] : 0f;
    }

public void ApplyStatModifier(ModifiableStats stat, float modifierValue)
{
    if (!currentStats.ContainsKey(stat))
    {
        Debug.LogError($"Stat {stat} not found in currentStats dictionary!");
        return;
    }

    switch (stat)
    {
        case ModifiableStats.HealthPoints:
            _unit.HealthPoints = _unit.UnitStats.BaseHealthPoints + Mathf.RoundToInt(modifierValue);
            break;
        case ModifiableStats.Wounds:
            _unit.Wounds = _unit.Wounds + Mathf.RoundToInt(modifierValue);
            break;
        case ModifiableStats.Morale:
            _unit.Morale = _unit.UnitStats.BaseMorale + modifierValue;
            break;
        case ModifiableStats.Discipline:
            _unit.Discipline = _unit.UnitStats.BaseDiscipline + modifierValue;
            break;   
        case ModifiableStats.Fatigue:
            _unit.Fatigue = _unit.UnitStats.BaseFatigue + modifierValue;
            break;  
        case ModifiableStats.AttackSpeed:
            _unit.AttackSpeed = _unit.UnitStats.BaseAttackSpeed + modifierValue;
            break;              
        case ModifiableStats.HitChance:
            _unit.HitChance = _unit.UnitStats.BaseHitChance + modifierValue;
            break;
        case ModifiableStats.MissileHitChance:
            _unit.MissileHitChance = _unit.UnitStats.BaseMissileHitChance + modifierValue;
            break;
        case ModifiableStats.DefenceChance:
            _unit.DefenceChance = _unit.UnitStats.BaseDefenceChance + modifierValue;
            break;
        case ModifiableStats.MissileDefenceChance:
            _unit.MissileDefenceChance = _unit.UnitStats.BaseMissileDefenceChance + modifierValue;
            break;
        case ModifiableStats.CriticalHitChance:
            _unit.CriticalHitChance = _unit.UnitStats.BaseCriticalHitChance + modifierValue;
            break;
        case ModifiableStats.MissileCriticalHitChance:
            _unit.MissileCriticalHitChance = _unit.UnitStats.BaseMissileCriticalHitChance + modifierValue;
            break;
        case ModifiableStats.Consitution:
            _unit.Consitution = _unit.UnitStats.BaseConsitution + modifierValue;
            break;
        case ModifiableStats.WalkSpeed:
            _unit.WalkSpeed = _unit.UnitStats.BaseWalkSpeed + modifierValue;
            break;
        case ModifiableStats.RunSpeed:
            _unit.RunSpeed = _unit.UnitStats.BaseRunSpeed + modifierValue;
            break;
        case ModifiableStats.ChargeSpeed:
            _unit.ChargeSpeed = _unit.UnitStats.BaseChargeSpeed + modifierValue;
            break;
        case ModifiableStats.SprintTime:
            _unit.SprintTime = _unit.UnitStats.BaseSprintTime + modifierValue;
            break; 
        case ModifiableStats.ChargeRange:
            _unit.ChargeRange = _unit.UnitStats.ChargeRange + modifierValue;
            break;      
        case ModifiableStats.CommitmentRange:
            _unit.ChargeCommitmentRange = _unit.UnitStats.CommitmentRange + modifierValue;
            break;  
        case ModifiableStats.MoraleEffectMultiplier:
            MoraleMultiplier = BaseMoraleMultiplier + modifierValue;
            break;                                                                                                
        default:
            Debug.LogWarning($"Stat {stat} mod not found in application list.");
            return;
    }

    // Update the dictionary
    currentStats[stat] = GetCurrentStatValue(stat);
}    

    public void SetEventTrigger(UnitEventTriggers trigger, bool isTrue)
    {
        if(unitEventDict.ContainsKey(trigger))
        {
            unitEventDict[trigger] = isTrue;
            return;
        }

        Debug.LogError("Searching for an event trigger (" +(trigger)+ ") that does not exist!");
    }

    public bool ReturnEventBool(UnitEventTriggers trigger)
    {
        return unitEventDict.ContainsKey(trigger) ? unitEventDict[trigger] : false;
    }

    public bool UnitinState(Unit.UnitStateMachine unitState)
    {
        if(_unit.UnitState == unitState)
        {
            return true;
        }
        
        return false;
    }

    public bool RoutCheck()
    {
        if(_unit.Morale < 1 && _unit.Discipline < 1)
        {
            return true;
        }

        return false;
    }


    public bool HostileUnitInRange(float range)
    {
        foreach(Faction faction in Faction.AllFactions)
        {
            //Only check spatial grid for enemy factions
            if(_unit.FriendlyFaction.IsHostile(faction))
            {
                // If any enemy units in range, return true
                List<Unit> nearbyUnits = faction.FactionUnitGrid.GetEntsInRange(_unit.UnitPosition, range);
                if(nearbyUnits.Count > 0)
                {
                    return true;
                }
            }
        }

        return false;
    }
    
    public bool AlliedUnitInRange(float range)
    {
        foreach(Faction faction in Faction.AllFactions)
        {
            //Only check spatial grid for enemy factions
            if(_unit.FriendlyFaction.IsAllied(faction))
            {
                // Find all units in range - if in range, bool is true, change unit state!
                List<Unit> nearbyUnits = faction.FactionUnitGrid.GetEntsInRange(_unit.UnitPosition, range);
                if(nearbyUnits.Count > 1)
                {
                    return true;
                }
            }
        }

        return false;
    }    

    
}
