using System.Collections.Generic;
using UnityEngine;

public class UnitMergingManager : MonoBehaviour
{
    private Unit _unit;
    // Start is called before the first frame update
    void Start()
    {
        _unit.GetComponent<Unit>();
    }

    void FixedUpdate()
    {
        MergeWeakSubunits();       
    }

    // Lower means it's more important to merge from

    private float MergePrecedence(SubUnit subUnitDonator, SubUnit subUnitToMergeInto)
    {
        // The lower our health, the more important we are to merge
        float healthMultiplier = 0.1f;
        float precedence = subUnitDonator._healthPoints * healthMultiplier;

        // The closer we are, the more important we are to merge
        float distanceMultipler = 2.0f;
        precedence += (subUnitDonator.GetPosition() - subUnitToMergeInto.GetPosition()).magnitude * distanceMultipler;

        return precedence;
    }

    private bool IsValidMergeTarget(SubUnit subUnit)
    {
        return subUnit._healthPoints < _unit.UnitStats.MinimumHealthPointsPerSubUnit;
    }

    private bool IsValidMergeDonator(SubUnit subUnit)
    {
        return subUnit.State != SubUnit.SubUnitState.Combat && subUnit.State != SubUnit.SubUnitState.Charging;
    }

    // We'll probably need to improve this, but for now the lowest health subunit has the closest valid subunit just donate half their troops
    private void MergeWeakSubunits()
    {
        // If we've only got 1 subunit, we can't merge
        if (_unit.SubUnits.Count <= 1)
        {
            return;
        }

        SubUnit lowestHealth = null;
        int minHeath = _unit.UnitStats.BaseHealthPointsPerSubUnit;

        foreach (SubUnit subUnit in _unit.SubUnits)
        {
            // If we already have a merge task, wait until that's complete
            SubUnit.MergeIntoTask task = subUnit.TaskQueue.Count == 0 ? null : subUnit.TaskQueue[0] as SubUnit.MergeIntoTask;
            if (task != null)
            {
                return;
            }

            // Find minimum health subunit
            if (IsValidMergeTarget(subUnit) && subUnit._healthPoints < minHeath)
            {
                minHeath = subUnit._healthPoints;
                lowestHealth = subUnit;
            }
        }

        if (lowestHealth == null)
        {
            return;
        }

        // Find a nearby valid subunit to merge into us 
        List<SubUnit> potentialMergers = new List<SubUnit>(_unit.SubUnits);
        potentialMergers.Sort((subUnit1, subUnit2) => MergePrecedence(subUnit1, lowestHealth).CompareTo(MergePrecedence(subUnit2, lowestHealth)));

        // Pick the first valid one, and ask them to merge into us
        foreach (SubUnit mergerSubUnit in potentialMergers)
        {
            if (mergerSubUnit == lowestHealth || !IsValidMergeDonator(mergerSubUnit))
            {
                continue;
            }

            // Distribute healthpoints so we and them will have the same amount
            int averageHealthPoints = (mergerSubUnit._healthPoints + lowestHealth._healthPoints) / 2;
            int healthPointsToGive = averageHealthPoints - lowestHealth._healthPoints;

            // Unless that puts us below the minimum threshold, in which case, merge into them entirely
            if (mergerSubUnit._healthPoints - healthPointsToGive < _unit.UnitStats.MinimumHealthPointsPerSubUnit)
            {
                healthPointsToGive = mergerSubUnit._healthPoints;
            }

            mergerSubUnit.ClearQueuedTasks();
            mergerSubUnit.QueueTask(new SubUnit.MergeIntoTask(lowestHealth, healthPointsToGive));
            mergerSubUnit.QueueTask(new SubUnit.PathMovementTask(mergerSubUnit._endGoalLine));

            return;
        }
    }


}
