using System.Collections.Generic;
using UnityEngine;

public class UnitAggression : MonoBehaviour
{
    private Unit _unit;

    public Unit.UnitAggressionLevel targetAggressionLevel;
    public Unit.UnitAggressionLevel currentAggressionLevel;
    
    // Start is called before the first frame update
    void Start()
    {
        _unit = GetComponent<Unit>();
        targetAggressionLevel = _unit.Aggression;
        currentAggressionLevel = _unit.Aggression;
    }

    [Header("Aggression Levels")]
    public List<AggressionLevelConfig> aggressionLevelConfigs = new List<AggressionLevelConfig>();
    private Dictionary<AggressionLevelConfig, int> aggressionID = new Dictionary<AggressionLevelConfig, int>();

    //Use morale thresholds here, if morale is between thresholds then maintain as active, else move towards central levels.  
}
