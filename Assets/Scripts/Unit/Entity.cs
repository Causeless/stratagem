﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using GPUInstancer.CrowdAnimations;
using System;
using Random = UnityEngine.Random;

public class Entity : MonoBehaviour, ICachedPosition
{
    public NavMeshAgent _navMeshAgent;
    public Animator _modelAnimator;
    public GPUICrowdPrefab _crowdAnimator;
    public GPUICrowdPrototype _prototype;
    public float _rotationSpeed = 5.0f;
    public float _rotationThreshold = 0.5f;

    public Vector3 _desiredPosition;
    public Vector3 _finalDestination;

    [HideInInspector]
    public Quaternion _lookRotation;

    private Quaternion _currentRotationTarget;

    public SubUnit _parentSubUnit;

    public enum AnimState
    {
        Idle,
        Combat,
        Throw,
        Attack,
        ShieldFront,
        ShieldUp,
    }

    public AnimState _animState;
    private AnimState _oldAnimState;
    private bool _animStateTrigger = false;
    public float AttackAnimOffset = 1f;
    public float AttackAnimSpeed = 1f;
    public string _animOverride = "";
    private bool _formationAnim = false;
    public bool _throwAnim = false;
    private float _animTimer = 1f;
    private float _currentAnimTime = 0f;


    // For spatial partition
    private Entity_SpatialPartitionGrid _grid;
    private Vector3 _oldPosition;

    private Vector3? _missileAttackTargetPosition = null;
    private Vector3? _missileAttackTargetVelocity = null;
    private AnimationClip _attack;

    // Use this for initialization
    void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _modelAnimator = GetComponent<Animator>();
        _grid = GameObject.FindWithTag("SpatialPartitionGrid").GetComponent<Entity_SpatialPartitionGrid>();
        _lookRotation = transform.rotation;
        _oldPosition = GetPosition();
        _crowdAnimator = GetComponent<GPUICrowdPrefab>();

        _prototype = (GPUICrowdPrototype)_crowdAnimator.prefabPrototype;

        Debug.Assert(_navMeshAgent != null, "Nav mesh agent missing for entity!");
        Debug.Assert(_modelAnimator != null, "Model animator missing for agent!");
        Debug.Assert(_grid != null, "Entity Spatial Partition Grid missing!");
    }

    private void Start()
    {
        _navMeshAgent.updateRotation = false;
        _grid.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        float rotationSpeed = 2.0f;
        transform.rotation = Quaternion.Lerp(transform.rotation, _currentRotationTarget, rotationSpeed * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        float speed = _navMeshAgent.velocity.magnitude;
        bool moving = speed > 0.1f;

        if (!moving &&
            transform.rotation != _lookRotation &&
            (_navMeshAgent.pathEndPosition - _navMeshAgent.nextPosition).sqrMagnitude <= _rotationThreshold * _rotationThreshold)
        {
            _currentRotationTarget = _lookRotation;
        }
        else if (moving)
        {
            _currentRotationTarget = Quaternion.LookRotation(_navMeshAgent.velocity);
        }

        _grid.Move(this);
        _oldPosition = _navMeshAgent.nextPosition;

        if(_throwAnim)
        {
            _currentAnimTime += Time.deltaTime;
        }
        else
        {
            _currentAnimTime = 0f;
        }

        if(_animTimer < _currentAnimTime)
        {
            _throwAnim = false;
        }

        if(_animOverride != "")
        {
            foreach (AnimState overrideAnimState in System.Enum.GetValues(typeof(AnimState)))
            {
                if(overrideAnimState.ToString() == _animOverride)
                {
                    _formationAnim = true;
                    SetAnimState(overrideAnimState);
                }
            }
        }
        else
        {
            _formationAnim = false;
        }         

        StartLoopingAnim(speed);
    }

    public void SetAnimState(AnimState state)
    {
        if(_throwAnim)
        {
            _animState = AnimState.Throw;
            AnimationClip throwClip = _prototype.clipList.Find(clip => clip.name.Contains("IdleThrow"));
            if(throwClip != null)
            {
                _animTimer = throwClip.length;
            }
            return;
        }

        if(_formationAnim)
        {
            if(state.ToString() == "_animOverride")
            {
                return;
            }
        }

        _animState = state;

        if(_oldAnimState != _animState)
        {
            _animStateTrigger = false;
            _oldAnimState = _animState;
        }
    }

    public void StartLoopingAnim(float entSpeed)
    {
        bool moving = entSpeed > 0.1f;

        //ShieldUp Animations
        if(_animState == AnimState.ShieldUp)
        {
            if(moving)
            {
                if(entSpeed > 2f)
                {
                    PlayAnimation("TestudoUpWalk", 0.65f);
                }
                else
                {
                    PlayAnimation("TestudoUpWalk", 1.25f);
                }
            }
            else
            {
                PlayAnimation("ShieldUpIdle", 1f);
            }  

            return;
        } 

        //ShieldFront Animations
        if(_animState == AnimState.ShieldFront)
        {
            if(moving)
            {
                if(entSpeed > 2f)
                {
                    PlayAnimation("TestudoFrontWalk", 0.65f);
                }
                else
                {
                    PlayAnimation("TestudoFrontWalk", 1.25f);
                }
            }
            else
            {
                PlayAnimation("TestudoFront", 1f);
            }

            return;            
        }        

        //Throwing Animations
        if(_animState == AnimState.Throw)
        {
            if(moving)
            {
                if(entSpeed > 2f)
                {
                    PlayAnimation("RunThrow", 1f);
                }
                else
                {
                    PlayAnimation("WalkThrow", 1f);
                }
            }
            else
            {
                PlayAnimation("IdleThrowEvent", 1f);
            }  

            return;   
        }        
        
        //For all Idle Animations
        if(_animState == AnimState.Idle)
        {
            if(moving)
            {
                if(entSpeed > 2f)
                {
                    PlayAnimation("Run", 0.65f);
                }
                else
                {
                    PlayAnimation("Walk", 0.65f);
                }

            }
            else
            {
                PlayAnimation("Idle", 1f);
            }

            return;  
        }

        //Combat Ready Animations
        if(_animState == AnimState.Combat)
        {  
            if(moving)
            {
                if(entSpeed > 2f)
                {
                    PlayAnimation("ChargeRun", 0.65f);
                }
                else
                {
                    PlayAnimation("ChargeWalk", 0.65f);
                }

            }
            else
            {
                PlayAnimation("CR_Idle", 1f);
            }   

            return;           
        }

        if(_animState == AnimState.Attack && !_animStateTrigger)
        {
            var attack = _prototype.clipList.Find(clip => clip.name.Contains("Attack"));
            float min = 1 - (0.5f * AttackAnimOffset);
            float max = 1 + (0.5f * AttackAnimOffset);
            float animOffset = 1 + Random.Range(min, max);
            _crowdAnimator.StartAnimation(attack, animOffset, AttackAnimSpeed, 0.5f);
        }

        _animStateTrigger = true;
    }

    public void PlayAnimation(string state, float speed)
    {
        var _clip = _prototype.clipList.Find(clip => clip.name.Contains(state));
        if(_clip == null)
        {
            //Debug.LogError(state + " is not a valid animation name for entity " + this.name); //TODO: Re-enable this once we have new models and animations set up with same naming conventions!
            return;
        }
        _crowdAnimator.StartAnimation(_clip, -1, speed, 0.5f);
    }

    public void Kill()
    {
        _parentSubUnit.RemoveEntity(this);
        _grid.Remove(this);

        PlayAnimation("Die", 0.3f);
        PlayAnimation("Die", 0.3f);
    }

    public void DestroyEntity()
    {
        _grid.Remove(this);
        Destroy(gameObject);
    }    

    public void Throw()
    {
        float entSpeed = _navMeshAgent.velocity.magnitude;
        bool moving = entSpeed > 0.1f;
        
        if(moving)
        {
            if(entSpeed > 2f)
            {
                PlayAnimation("RunThrow", 1f);
            }
            else
            {
                PlayAnimation("WalkThrow", 1f);
            }
        }
        else
        {
            PlayAnimation("IdleThrowEvent", 1f);
        }    
    }    



    public Vector3 GetPosition()
    {
        return _navMeshAgent.nextPosition;
    }

    public Vector3 GetOldPosition()
    {
        return _oldPosition;
    }

    public void Animate(float speed)
    {
        _modelAnimator.SetFloat("Speed", speed);
    }

    public IEnumerator StartProjectileAttack(SubUnit targetSubUnit, float timeDelay)
    {
        // God, I hope unity automatically cancels coroutines on an object when it's destroyed
        yield return new WaitForSeconds(timeDelay);

        // Just in case they died in the delay between visual/sim missile attack stuff
        if (targetSubUnit == null || targetSubUnit.Entities.Count == 0)
        {
            yield break;
        }

        // Target a random entity within the targeted subunit
        Entity randomEntity = targetSubUnit.Entities[Random.Range(0, targetSubUnit.Entities.Count)];
        _missileAttackTargetPosition = randomEntity.GetPosition();
        _missileAttackTargetVelocity = randomEntity.GetComponent<NavMeshAgent>().velocity;

        // start the throw animation
        SetAnimState(AnimState.Throw);
        _throwAnim = true;
    }

    public void EnableSelectionHighlight(bool enable)
    {
        foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>(true))
        {
            sprite.enabled = enable;
        }
    }

    public void AnimEventTriggerAutonomousProjectiles()
    {
        if (!_missileAttackTargetPosition.HasValue || !_missileAttackTargetVelocity.HasValue)
        {
            Debug.Assert(false, "Entity projectile throw anim event hit without a target entity! Please ensure that the projectile attack delay is greater than the animation timing.");
            return;
        }

        if (_parentSubUnit == null)
        {
            // We're not in a subunit for whatever reason, so just stop
            return;
        }

        // Later, spawn this exactly on the javelin location
        GameObject projectilePrefab = _parentSubUnit.SimProjectilePrefab.GetComponent<SimProjectile>().ProjectilePrefab;
        GameObject projectile = Instantiate(projectilePrefab, GetPosition(), Quaternion.identity) as GameObject;
        projectile.GetComponent<Projectile>().Initialize(_missileAttackTargetPosition.Value, _missileAttackTargetVelocity.Value);

        _missileAttackTargetPosition = null;
        _missileAttackTargetVelocity = null;
    }

    public void ClearAnimState()
    {
        for (int i = 0; i < _modelAnimator.parameterCount; i++)
        {
            var parameter = _modelAnimator.parameters[i];
            if (parameter.type == AnimatorControllerParameterType.Bool)
            {
                _modelAnimator.SetBool(parameter.name, parameter.defaultBool);
            }
        }
    }

    public void SetAnimation(string animation, bool active)
    {
        _modelAnimator.SetBool(animation, active);
    }
}
