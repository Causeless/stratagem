using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class UnitPowerLevels : MonoBehaviour
{

    [HideInInspector]
    public Unit _unit;

    private void Start()
    {
        _unit = GetComponent<Unit>();
    }

    
    [Header("Multipliers")]

    [Header("Attack Multipliers")]
    public float HealthPointsAttackPowerMult = 1f;
    public float HitChancePowerMult = 1f;
    public float CriticalHitChancePowerMult = 1f;
    public float HealthPointsMissileAttackPowerMult = 1f;
    public float MissileHitChancePowerMult = 1f;
    public float MissileCriticalHitChancePowerMult = 1f;    
    public float MissileRangeReference = 180f; //Distance at which all other unit "Shoot Range" are compared against.

    [Header("Sustain Multipliers")]
    public float HealthPointsSustainPowerMult = 1f;
    public float DefenceChancePowerMult = 1f;
    public float ConsitutionPowerMult = 1f;     
    public float HealthPointsSustainMissilePowerMult = 1f;
    public float MissileDefenceChancePowerMult = 1f;      
    public float ConsitutionMissilePowerMult = 1f;   

    [Header("Psych Multipliers")]
    public float PsychPowerMult = 1f;    

    [Header("Unit Type Versus")]
    public float BonusVersusMeleeCav = 1f; 
    public float BonusVersusMissileCav = 1f;
    public float BonusVersusMeleeInf = 1f;
    public float BonusVersusMissileInf = 1f;

    [Header("Variables")]
    [Header("Attack Variables")]       
    [ReadOnly]
    public float HealthPointsAttackPower;
    private float AttackSpeedPower;    
    [ReadOnly]
    public float HitChancePower;
    [ReadOnly]
    public float CriticalHitChancePower;
    [ReadOnly]
    public float HealthPointsMissileAttackPower;
    private float AmmoPower;
    private float ReloadTimePower;
    [ReadOnly]
    public float MissileRangePower; 
    [ReadOnly]
    public float MissileHitChancePower;    
    [ReadOnly]
    public float MissileCriticalHitChancePower;

    [Header("Sustain Variables")] 
    [ReadOnly]
    public float HealthPointsSustainPower;
    [ReadOnly]
    public float DefenceChancePower;
    [ReadOnly]
    public float ConsitutionPower;      
    [ReadOnly]
    public float HealthPointsSustainMissilePower;    
    [ReadOnly]
    public float MissileDefenceChancePower;
    [ReadOnly]
    public float ConsitutionMissilePower;  

    [Header("Psych Variables")]       
    [ReadOnly]
    public float DisciplinePower;
    [ReadOnly]
    public float MoralePower;  

    //To create the unit powerlevels, we assess the likelihood of various scenarios, and attribute values to the outcomes.
    public float CalculateMeleeAttackPower()
    {   
        //Manpower to use for attacking
        HealthPointsAttackPower = (float)_unit.HealthPoints * HealthPointsAttackPowerMult;

        //Chance to hit enemy in a minute
        AttackSpeedPower = _unit.AttackSpeed / 60f;
        HitChancePower = (_unit.HitChance * AttackSpeedPower) * HitChancePowerMult;

        //Hit and Crit enemy in a minute.
        CriticalHitChancePower = ((_unit.CriticalHitChance * (_unit.HitChance / 100f)) * AttackSpeedPower) * CriticalHitChancePowerMult;


        return (HitChancePower + CriticalHitChancePower) * HealthPointsAttackPower;
    }

    public float CalculateMeleeDefencePower()
    {   
        //Manpower to use for defending
        HealthPointsSustainPower = (float)_unit.HealthPoints * HealthPointsSustainPowerMult;

        //Chance to block an attack
        DefenceChancePower = _unit.DefenceChance * DefenceChancePowerMult;

        //Chance to fail a block, but const the damage.
        float _failDefenceChance = (_unit.DefenceChance / 100f - 1f) * -1f;
        ConsitutionPower = (_unit.Consitution * _failDefenceChance) * ConsitutionPowerMult;

        return (DefenceChancePower + ConsitutionPower) * HealthPointsSustainPower;
    }    

    public float CalculateMissileAttackPower() 
    //We should calculate power vs enemy units to determine which units to automatically shoot at? 
    //Automatically shoot at units with low missile defence and highest missile/melee threat against this unit?
    {
        if(_unit.UnitAmmo == 0)
        {
            return 0f;
        }

        if(_unit.ShootingBehaviour == Unit.UnitShootingBehaviour.HoldFire)
        {
            return 0f;
        }

        //Manpower to use for attacking
        HealthPointsMissileAttackPower = (float)_unit.HealthPoints * HealthPointsMissileAttackPowerMult;

        //Chance to hit enemy in a minute
        var t = _unit.ReloadTime / 60f; 
        ReloadTimePower = Mathf.Lerp(1, 0, t);

        MissileRangePower = _unit.ShootRange / MissileRangeReference;

        AmmoPower = _unit.UnitAmmo / _unit.SubUnits.Count;
        MissileHitChancePower = (((_unit.HitChance * AmmoPower) * MissileRangePower) * ReloadTimePower) * MissileHitChancePowerMult;

        //Hit and Crit enemy in a minute.
        MissileCriticalHitChancePower = ((((_unit.MissileCriticalHitChance * (_unit.MissileHitChance / 100f)) * AmmoPower) * MissileRangePower) * ReloadTimePower) * MissileCriticalHitChancePowerMult;


        return (MissileHitChancePower + MissileCriticalHitChancePower) * HealthPointsMissileAttackPower;
    }

    public float CalculateMissileDefencePower() //When calculating missile power against a unit, reduce it by target units missile defense power.
    {
        //Manpower to use for defending
        HealthPointsSustainMissilePower = (float)_unit.HealthPoints * HealthPointsSustainMissilePowerMult;

        //Chance to block an attack
        MissileDefenceChancePower = _unit.MissileDefenceChance * MissileDefenceChancePowerMult;

        //Chance to fail a block, but const the damage.
        float _failMissileDefenceChance = (_unit.MissileDefenceChance / 100f - 1f) * -1f;
        ConsitutionMissilePower = (_unit.Consitution * _failMissileDefenceChance) * ConsitutionMissilePowerMult;

        return (MissileDefenceChancePower + ConsitutionMissilePower) * HealthPointsSustainMissilePower;
    }

    public float CalculatePsychPower()
    {
        var l = _unit.Morale / 50f;
        var h = (_unit.Morale -50f) / 50f;
        float _moralePowerHigh = Mathf.Lerp(50f, 0f, h);
        float _moralePowerLow = Mathf.Lerp(0f, 50f, l);
        DisciplinePower = _unit.Discipline / 100f;
        MoralePower = _moralePowerLow + _moralePowerHigh; //MoralePower should be higher the closer it is to 50.
        //Could do this in parabolic curve with 1 and 100 morale being max/min and 50 being the vertex. 
        //y = a(0-50)2 + 0, where y is set by how steep we want the curve to be and a being the coefficient.  

        return (MoralePower * DisciplinePower) * PsychPowerMult;
    }
}
