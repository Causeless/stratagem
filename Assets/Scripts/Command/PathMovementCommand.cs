﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using LinearAlgebra;
using UnityEngine.Assertions;

public partial class Unit // So we can access unit's private members
{
    public class PathMovementCommand : Command
    {
        private List<LineSegment> _destinationEdges;
        private List<SubUnit.Task> _pathMovementTasks;
        private LineSegment _frontLine;

        public PathMovementCommand(LineSegment frontLine)
        {
            _frontLine = frontLine;
            _pathMovementTasks = new List<SubUnit.Task>();
        }

        private class SubUnitPathHelper
        {
            PathMovementCommand _parent;
            int _subUnitId;
            public SubUnitPathHelper(PathMovementCommand parent, int subUnitId)
            {
                _parent = parent;
                _subUnitId = subUnitId;
            }

            public void OnSubUnitPathComplete(Path path)
            {
                ABPath abPath = path as ABPath;

                var waypoints = SubUnit.PathMovementTask.GenerateWaypointsForPath(abPath, _parent._unit.SubUnits[_subUnitId].GetPosition(), true, _parent._destinationEdges[_subUnitId]);

                SubUnit.FollowPathTask pathToEnd = new SubUnit.FollowPathTask(waypoints);
                _parent._pathMovementTasks.Add(pathToEnd);

                _parent._unit.SubUnits[_subUnitId].QueueTask(pathToEnd);
            }
        }

        public void OnCommanderPathComplete(Path path)
        {
            ABPath abPath = path as ABPath;
            if (path.error)
            {
                // Command failed, no valid path
                IsComplete = true;
                return;
            }

            var waypoints = SubUnit.PathMovementTask.GenerateWaypointsForPath(abPath, _unit.GetPrimarySubUnit().GetPosition(), true, _destinationEdges[0], _unit.SubUnitRallyDistance, _unit.SubUnitIndependentMovementDistance);

            // Every subunit paths to the rally point, and then starts following the commander's path
            // Before then pathing to their final destination
            for (int i = 0; i < _unit.SubUnits.Count; i++)
            {
                SubUnit subUnit = _unit.SubUnits[i];

                if (waypoints.Count > 0)
                {
                    SubUnit.PathMovementTask pathToRally = new SubUnit.PathMovementTask(waypoints[0]);
                    SubUnit.FollowPathTask followCommanderPath = new SubUnit.FollowPathTask(waypoints);

                    Vector3 lastWaypoint = waypoints[waypoints.Count - 1].GetMidpoint();

                    subUnit.QueueTask(pathToRally);
                    subUnit.QueueTask(followCommanderPath);

                    // Start an async pass to calculate path from end of commander path to our final destination
                    SubUnitPathHelper helper = new SubUnitPathHelper(this, i);
                    ABPath pathToEnd = ABPath.Construct(lastWaypoint, _destinationEdges[i].GetMidpoint(), helper.OnSubUnitPathComplete);
                    SubUnit.PathMovementTask.ApplyMovementPathSettings(pathToEnd);
                    AstarPath.StartPath(pathToEnd);
                }
                else
                {
                    SubUnit.PathMovementTask pathToEnd = new SubUnit.PathMovementTask(_destinationEdges[i]);
                    _pathMovementTasks.Add(pathToEnd);

                    subUnit.QueueTask(pathToEnd);
                }
            }
        }

        public override void Apply()
        {
            // We need to set our subunit's position tags to moving so we don't avoid each other before the movement formally begins
            AstarPath.active.AddWorkItem(
                new AstarWorkItem(() => {
                    var gg = AstarPath.active.data.gridGraph;

                    foreach (SubUnit subUnit in _unit.SubUnits)
                    {
                        var node = AstarPath.active.GetNearest(subUnit.Position).node;
                        node.Tag = 0;
                    }
                },
                force => { return true; })
            );

            StartCalculatingPath();
        }

        private void StartCalculatingPath()
        {
            // There must be an AstarPath instance in the scene
            if (AstarPath.active == null)
            {
                Assert.IsTrue(false);
                IsComplete = true;
                return;
            }

            var unitFormation = _unit.GetComponent<UnitFormation>();
            var unitLines = unitFormation.GetForLine(_frontLine, _unit.SubUnits);
            _destinationEdges = unitLines.segments;

            if (_destinationEdges.Count < _unit.SubUnits.Count)
            {
                Assert.IsTrue(false);
                IsComplete = true;
                return;
            }

            _frontLine = unitLines.endFrontLine;

            ABPath path = ABPath.Construct(_unit.GetPrimarySubUnit().GetPosition(), unitLines.endFrontLine.GetMidpoint(), OnCommanderPathComplete);
            SubUnit.PathMovementTask.ApplyMovementPathSettings(path);

            AstarPath.StartPath(path);
        }

        public override void Update()
        {
            Debug.DrawRay(_frontLine.GetMidpoint(), Vector3.up, Color.cyan);

            bool allTasksComplete = true;
            foreach (SubUnit.Task task in _pathMovementTasks)
            {
                if (!task.IsComplete)
                {
                    allTasksComplete = false;
                    break;
                }
            }

            IsComplete = allTasksComplete && _pathMovementTasks.Count >= _unit.SubUnits.Count;
        }
    }
}