using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Unit // So we can access unit's private members
{
    public abstract class Command
    {
        public Unit _unit;

        public bool IsComplete { get; protected set; } = false;

        public abstract void Apply();
        public virtual void Update() { }
    }
}
