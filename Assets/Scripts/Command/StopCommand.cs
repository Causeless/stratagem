﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Unit // So we can access unit's private members
{
    public class StopCommand : Command
    {
        public override void Apply()
        {
            foreach (SubUnit subUnit in _unit.SubUnits)
            {
                subUnit.QueueTask(new SubUnit.StopTask());
            }
            IsComplete = true;
        }
    }
}