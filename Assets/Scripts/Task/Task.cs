﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SubUnit // So we can access sub-unit's private members
{
    public abstract class Task
    {
        public SubUnit _subUnit;
        protected List<Task> _subTasks = new List<Task>();

        public bool IsComplete { get; protected set; } = false;

        public virtual void Cancel() { IsComplete = true; }
        public abstract void Apply();
        public virtual void UpdateImpl() { }
        public virtual void OnComplete() { }

        // Called after our subtasks have finished updating
        public virtual void PostUpdate() { }

        // TODO: Fully implement this to be useful when I gain the fucking energy

        // Some subtasks don't apply immediately; for example, a task to path somewhere may take several ticks to complete
        // This complicates things when we do things like repathing, as typically what we would do is just delete our current PathMovementTask and then apply a new one
        // However this causes a visible pause, which is especially bad when doing things like charging: the subunit will stop as it recalculates, then move again
        // As such, we have this special-case function, which can be called to ask "hey, are you actually ready to apply to the unit yet?"
        public virtual bool IsReady() { return true; }

        public void Update()
        {
            // Update our task
            UpdateImpl();

            while (_subTasks.Count > 0)
            {
                // update our current subtask
                if (!_subTasks[0].IsComplete)
                {
                    _subTasks[0].Update();
                    if (!_subTasks[0].IsComplete)
                    {
                        // Wait until next tick until our next update
                        break;
                    }
                }
                else // or pop it if it's complete
                {
                    _subTasks[0].OnComplete();
                    _subTasks.RemoveAt(0);

                    // If there's still subtasks left, apply it
                    if (_subTasks.Count > 0)
                    {
                        _subTasks[0].Apply();
                    }
                }
            }

            PostUpdate();
        }

        // Immediately cancels and clears all queued subtasks
        protected void ClearSubTasks()
        {
            foreach (Task subTask in _subTasks)
            {
                subTask.Cancel();
            }
        }

        protected void QueueSubTask(Task subTask)
        {
            subTask._subUnit = _subUnit;
            _subTasks.Add(subTask);

            if (_subTasks.Count == 1)
            {
                _subTasks[0].Apply();
            }
        }

        protected bool AllSubTasksComplete()
        {
            return _subTasks.Count == 0;
        }
    }
}
