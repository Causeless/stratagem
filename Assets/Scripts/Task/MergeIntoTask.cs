using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LinearAlgebra;

public partial class SubUnit // So we can access unit's private members
{
    public class MergeIntoTask : Task
    {
        private SubUnit _mergeInto = null;
        private int _numberOfHealthPoints = 0;
        Vector2Int _targetCurrentSquare = GridManager.InvalidGridSquare;
            
        public MergeIntoTask(SubUnit unit, int numberOfHealthPoints)
        {
            _mergeInto = unit;
            _numberOfHealthPoints = numberOfHealthPoints;
        }
        
        public override void Apply()
        {

        }

        public override void UpdateImpl()
        {
            // If our target was destroyed, cancel
            if (_mergeInto._healthPoints == 0)
            {
                Cancel();
                return;
            }

            // Check to see if our target has moved; if so, we repath
            Vector2Int targetGridSquare = GridManager.s_instance.GetGridSquareCoordinatesForPosition(_mergeInto.GetPosition());
            if (targetGridSquare != _targetCurrentSquare)
            {
                // Target subunit has moved, so repath
                _targetCurrentSquare = targetGridSquare;
                PathToTarget(_mergeInto);
            }
        }

        public override void PostUpdate()
        {
            IsComplete = AllSubTasksComplete();
        }

        // TODO: stop duplicating this shit everywhere
        private void PathToTarget(SubUnit target)
        {
            ClearSubTasks();

            Vector3 ourPosition = _subUnit.GetPosition();
            Vector3 theirPosition = target.GetPosition();

            Vector3 toThem = (theirPosition - ourPosition).normalized;

            Vector3 toSide = Vector3.Cross(toThem, Vector3.up).normalized;
            toSide *= GridManager.s_instance.GridSquareSize * 0.5f;

            Vector3 targetPosition = theirPosition - (toThem * GridManager.s_instance.GridSquareSize * 0.5f);

            // Don't go precisely on the edge of the grid square, give some tolerance
            float buffer = 1f;
            Vector3 tolerance =  toThem * buffer;

            // TODO: we don't want to align exactly on the grid edge; instead we want to align along grid "corridors"
            // But that's for future me

            LineSegment targetLine = new LineSegment
            {
                firstPos = GridManager.s_instance.AliasToGridCorner(targetPosition + toSide) - tolerance,
                lastPos =  GridManager.s_instance.AliasToGridCorner(targetPosition - toSide) - tolerance
            };

            QueueSubTask(new PathMovementTask(targetLine));
        }

        public override void OnComplete()
        {
            // We're at our destination, donate our HP to them
            _subUnit.GiftHealthPointsTo(_mergeInto, _numberOfHealthPoints);
        }
    }
}