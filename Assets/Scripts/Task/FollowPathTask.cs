﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using LinearAlgebra;
using UnityEngine.Assertions;

public partial class SubUnit // So we can access subUnit's private members
{
    public class FollowPathTask : Task
    {
        List<LineSegment> _waypoints;
        public FollowPathTask(List<LineSegment> waypoints)
        {
            _waypoints = waypoints;
        }

        public bool CheckPathBlocked()
        {
            return false; // :( for now

#pragma warning disable 0162
            int pathBlockAheadThreshold = 5;
            int nodesAhead = -1;

            GraphNode lastNode = null;
            foreach (RawMovementTask rawMovementTask in _subTasks)
            {
                GraphNode currentNode = AstarPath.active.GetNearest(rawMovementTask.Line.GetMidpoint()).node;
                if (currentNode != lastNode)
                {
                    nodesAhead++;
                    if (nodesAhead > pathBlockAheadThreshold)
                    {
                        return false;
                    }

                    if (currentNode.Tag == GridManager.nodeOccupiedByCombatUnitTag)
                    {
                        return true;
                    }
                }
            }

            return false;
#pragma warning restore 0162
        }

        public override void Apply()
        {
            foreach (LineSegment waypoint in _waypoints)
            {
                QueueSubTask(new RawMovementTask(waypoint));
            }
        }

        public override void UpdateImpl()
        {
            IsComplete = AllSubTasksComplete();

            if (_subTasks.Count > 0)
            {
                // debug rendering
                for (int i = 1; i < _subTasks.Count; i++)
                {
                    LineSegment prevLine = (_subTasks[i] as RawMovementTask).Line;
                    LineSegment thisLine = (_subTasks[i - 1] as RawMovementTask).Line;
                    //Debug.DrawLine(thisLine.firstPos, thisLine.lastPos, Color.yellow);
                    //Debug.DrawRay(thisLine.GetMidpoint(), -thisLine.GetNormalVector()*0.5f, Color.blue);
                    Debug.DrawLine(prevLine.GetMidpoint(), thisLine.GetMidpoint(), Color.red);
                }
                LineSegment lastLine = (_subTasks[_subTasks.Count - 1] as RawMovementTask).Line;
                Debug.DrawLine(lastLine.firstPos, lastLine.lastPos, Color.yellow);
                Debug.DrawRay(lastLine.GetMidpoint(), -lastLine.GetNormalVector()*0.5f, Color.blue);
            }
        }
    }
}