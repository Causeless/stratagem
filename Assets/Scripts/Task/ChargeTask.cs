﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LinearAlgebra;

public partial class SubUnit // So we can access unit's private members
{
    public class ChargeTask : Task
    {
        SubUnit _currentlyChargingTowards = null;

        public SubUnit CurrentlyChargingTowards
        {
            get { return _currentlyChargingTowards; }
        }

        Vector2Int _targetCurrentSquare = GridManager.InvalidGridSquare;
        Vector2Int _ourCurrentSquare = GridManager.InvalidGridSquare;

        public ChargeTask()
        {

        }

        // If we have multiple charge tasks against one subunit, we may be forced to choose another target
        // Maybe not necessarily? We'll repath soon enough anyways, right? (after 1 grid square)
        public void RechooseChargeTarget()
        {
            ClearChargeAllocation();
            _currentlyChargingTowards = null;
        }

        public override void Apply()
        {
            _subUnit.State = SubUnit.SubUnitState.Charging;
        }

        public override void UpdateImpl()
        {
            // Recheck the target subunit whenever we enter a new grid square
            Vector2Int ourGridSquare = GridManager.s_instance.GetGridSquareCoordinatesForPosition(_subUnit.GetPosition());
            if (ourGridSquare != _ourCurrentSquare || _currentlyChargingTowards == null)
            {
                _ourCurrentSquare = ourGridSquare;
                SubUnit chargeTarget = _subUnit._chargeTarget;
                if (chargeTarget != _currentlyChargingTowards)
                {
                    ClearChargeAllocation();
                    _currentlyChargingTowards = chargeTarget;
                    _subUnit._chargeTarget = chargeTarget;
                    AddChargeAllocation();
                }
                
                // If no valid subunit found
                if (_currentlyChargingTowards == null)
                {
                    return;
                }
            }

            // Check to see if our target has moved; if so, we repath
            Vector2Int targetGridSquare = GridManager.s_instance.GetGridSquareCoordinatesForPosition(_currentlyChargingTowards.GetPosition());
            if (targetGridSquare != _targetCurrentSquare)
            {
                // Target subunit has moved, so repath
                _targetCurrentSquare = targetGridSquare;
                PathToTarget(_currentlyChargingTowards);
            }
        }

        public override void PostUpdate()
        {
            if (_currentlyChargingTowards != null)
            {
                // If we're within x grid square distance of our target, we're complete
                float tolerance = 1.25f * GridManager.s_instance.GridSquareSize;
                float distanceSqrToTarget = (_subUnit.GetPosition() - _currentlyChargingTowards.GetPosition()).sqrMagnitude;
                IsComplete = distanceSqrToTarget < tolerance * tolerance;
            }
            else
            {
                Cancel();
            }
        }

        public override void OnComplete()
        {
            ClearChargeAllocation();
            _subUnit.State = SubUnit.SubUnitState.Idle;
        }

        private void ClearChargeAllocation()
        {
            if (_currentlyChargingTowards == null)
            {
                return;
            }

            var allocation = _subUnit._parentUnit.FriendlyFaction.GetChargeAllocations(_currentlyChargingTowards);
            allocation.Remove(this);
        }

        private void AddChargeAllocation()
        {
            if (_currentlyChargingTowards == null)
            {
                return;
            }

            var allocation = _subUnit._parentUnit.FriendlyFaction.GetChargeAllocations(_currentlyChargingTowards);
            allocation.Add(this);
        }

        private void PathToTarget(SubUnit target)
        {
            ClearSubTasks();

            Vector3 ourPosition = _subUnit.GetPosition();
            Vector3 theirPosition = target.GetPosition();

            Vector3 toThem = (theirPosition - ourPosition).normalized;

            Vector3 toSide = Vector3.Cross(toThem, Vector3.up);
            toSide *= GridManager.s_instance.GridSquareSize * 0.5f;

            Vector3 targetPosition = theirPosition - (toThem * GridManager.s_instance.GridSquareSize * 0.75f);

            LineSegment targetLine = new LineSegment
            {
                firstPos = GridManager.s_instance.AliasToGridCorner(targetPosition + toSide),
                lastPos =  GridManager.s_instance.AliasToGridCorner(targetPosition - toSide)
            };

            QueueSubTask(new PathMovementTask(targetLine));
        }
    }
}