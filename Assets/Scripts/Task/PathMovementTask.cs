﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using LinearAlgebra;
using UnityEngine.Assertions;

public partial class SubUnit // So we can access subUnit's private members
{
    public class PathMovementTask : Task
    {
        private ABPath _path;

        // Todo, generalize this...
        private bool _isReady = false;

        public LineSegment Line { get; private set; }

        public PathMovementTask(LineSegment line)
        {
            Line = line;
        }

        public static void ApplyMovementPathSettings(ABPath path)
        {
            path.nnConstraint = NNConstraint.None;
            path.enabledTags &= ~GridManager.nodeOccupiedByIdleUnitTag;
            path.tagPenalties = GridManager.tagPenalties;
        }

        public static List<LineSegment> GenerateWaypointsForPath(ABPath path, Vector3 pathStartPosition, bool smoothPath, LineSegment finalLine, float fromDistance = 0.0f, float untilDistance = 0.0f)
        {
            List<LineSegment> waypoints = new List<LineSegment>();
            if (!path.IsDone() || path.error)
            {
                return waypoints;
            }

            if (smoothPath)
            {
                HackSimpleSmoothModifier modifier = new HackSimpleSmoothModifier();
                modifier.strength = 0.75f;
                modifier.Apply(path);
            }

            float desiredWidth = finalLine.GetLength();

            int firstPathPointIndex = 0;
            float distance = 0.0f;
            // Calculate the first waypoint we need to generate towards
            for (int i = 0; i < path.vectorPath.Count - 1; i++)
            {
                distance += (path.vectorPath[i] - path.vectorPath[i+1]).magnitude;
                if (distance >= fromDistance)
                {
                    firstPathPointIndex = i;
                    break;
                }
            }

            int finalPathPointIndex = path.vectorPath.Count;
            if (untilDistance != 0.0f && path.vectorPath.Count > 1)
            {
                finalPathPointIndex = 0;
                float totalLength = 0.0f;
                // Calculate the last waypoint we need to generate towards
                for (int i = path.vectorPath.Count - 1; i > 0; i--)
                {
                    totalLength += (path.vectorPath[i] - path.vectorPath[i - 1]).magnitude;
                    if (totalLength > untilDistance)
                    {
                        finalPathPointIndex = i;
                        break;
                    }
                }
            }

            // ignore any corners that are very close to us, because A* grid likes to path behind us initially...
            float thresholdSqr = GridManager.s_instance.GridSquareSize * 0.5f;
            thresholdSqr *= thresholdSqr;

            // ignore the last corner as last corner is on line we will add directly (finalPathPointIndex - 1)
            for (int i = firstPathPointIndex; i < finalPathPointIndex - 1; i++)
            {
                if ((pathStartPosition - path.vectorPath[i]).sqrMagnitude < thresholdSqr)
                {
                    continue;
                }

                LineSegment intermediateLine = GetWaypointForPosition(path.vectorPath[i], desiredWidth, path.vectorPath[i + 1]);
                waypoints.Add(intermediateLine);
            }

            if (untilDistance == 0.0f)
            {
                // And we need to add the final point, of course
                waypoints.Add(finalLine);
            }

            return waypoints;
        }

        public static LineSegment GetWaypointForPosition(Vector3 pos, float desiredWidth, Vector3 nextCorner)
        {
            Vector3 dirToNextWaypoint = (nextCorner - pos);
            Vector3 toSide = Vector3.Cross(dirToNextWaypoint, Vector3.up).normalized; // todo, think of a better solution than this... need a notepad
            Vector3 toSideWithWidth = toSide * desiredWidth * 0.5f; // half the width to either side, as pos is ideally the middle

            LineSegment newLine = new LineSegment
            {
                firstPos = pos + toSideWithWidth,
                lastPos = pos - toSideWithWidth
            };

            return newLine;
        }

        public void OnPathComplete(Path path)
        {
            ABPath abPath = path as ABPath;

            _subTasks.Clear();

            var waypoints = GenerateWaypointsForPath(abPath, _subUnit.GetPosition(), true, Line);
            QueueSubTask(new FollowPathTask(waypoints));

            _path = abPath;
            _isReady = true;
        }

        public override void Apply()
        {
            CreateNewPath();
        }

        public void CreateNewPath()
        {
            Vector3 endPos = Line.GetMidpoint();

            // There must be an AstarPath instance in the scene
            if (AstarPath.active == null)
            {
                Assert.IsTrue(false);
                IsComplete = true;
                return;
            }

            // Optimization; if we're in the same grid square as our target, then no need to try to path
            // Turns out constant pathing becomes awfully slow even with a path length of zero
            Vector3 frontLinePosition = _subUnit._coordinator.GetFrontLine().GetMidpoint();
            Vector2Int ourPosition = GridManager.s_instance.GetGridSquareCoordinatesForPosition(frontLinePosition);
            Vector2Int destination = GridManager.s_instance.GetGridSquareCoordinatesForPosition(endPos);
            if (ourPosition == destination)
            {
                QueueSubTask(new RawMovementTask(Line));
                _isReady = true;
                return;
            }

            ABPath path = ABPath.Construct(_subUnit.GetPosition(), endPos, OnPathComplete);
            ApplyMovementPathSettings(path);
            AstarPath.StartPath(path);

            // no no no no no no
            // We really, really need to change this shit :/
            path.BlockUntilCalculated();
        }

        public override bool IsReady()
        {
            return _isReady;
        }
        
        public override void UpdateImpl()
        {
            // We want to check to see if we need to generate a new path;
            // If our path has suddenly become blocked by something, we repath

            // Note! We need to properly do this with the pre-generated-pathed stuff in PathMovementCommand too, not just here
            // TODO TODO TODO
            if (!IsComplete && _subTasks.Count > 0)
            {
                FollowPathTask followPathTask = _subTasks[0] as FollowPathTask;
                if (followPathTask != null && followPathTask.CheckPathBlocked())
                {
                    _isReady = false;
                    CreateNewPath();
                }
            }
        }

        public override void PostUpdate()
        {
            IsComplete = _isReady && AllSubTasksComplete();
        }
    }
}