﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SubUnit // So we can access unit's private members
{
    public class FormationTask : Task
    {
        // Todo, this'll need changing. We'll need to find a good way of setting Formation here as you cannot instantiate MonoBehaviours with New

        public Formation Formation { get; private set; }

        public FormationTask(SubUnit subUnit, Formation formation)
        {
            _subUnit = subUnit;
            Formation = formation;
        }

        public override void Apply()
        {
            if (Formation.Line == null)
            {
                Formation.SetForLine(_subUnit._formation.Line, _subUnit.Entities.Count);
            }

            _subUnit._formation = Formation;
            _subUnit.MarkFormationDirty();

            IsComplete = true;
        }
    }
}