using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LinearAlgebra;

public partial class SubUnit // So we can access unit's private members
{
    public class CombatTask : Task
    {
        private SubUnit _currentCombatTarget = null;
            
        public override void Apply()
        {
            if (_currentCombatTarget != null)
            {
                _subUnit.State = SubUnit.SubUnitState.Combat;
            }
        }

        public override void UpdateImpl()
        {
            if(_subUnit._parentUnit.UnitState == Unit.UnitStateMachine.Disengage)
            {
                IsComplete = true;
                return;          
            }

            if(_subUnit._taskQueue.Count > 1)
            {
                return;
            }
            
            _currentCombatTarget = PickCombatTarget();
            if (_currentCombatTarget == null)
            {
                IsComplete = true;
                return;
            }

            ManuoevreTowardsEnemy(_currentCombatTarget);

            if (IsInCombatRange(_currentCombatTarget))
            {
                _subUnit.TryPerformMeleeAttack(_currentCombatTarget);
                _subUnit.State = SubUnit.SubUnitState.Combat;
            }
        }

        // Get a score for attacking a subunit (lower is better)
        private float GetCombatScore(SubUnit potentialTarget)
        {
            float score = 0.0f;

            // Distance cost
            score += (_subUnit.GetPosition() - potentialTarget.GetPosition()).magnitude;

            // Hysterisis; if the subunit is different from our current target, avoid switching
            float switchCost = 8.0f;
            score += _currentCombatTarget != potentialTarget ? switchCost : 0.0f;

            return score;
        }

        private SubUnit PickCombatTarget()
        {
            float radiusTolerance = 1.15f; // radius modifier allows us to tweak how far from the frontline the units will move to engage. Higher frontline tolerance means this should be increased.
            
            if(_subUnit._chargeTarget != null)
            {
                radiusTolerance += _subUnit._combatDistance * 2f; //Used for pushing mechanic, subunits that are further from the enemy still need to target them.
            }
 
            float clampedRadius = Mathf.Clamp(radiusTolerance, 1.1f, 100f);
            float checkRadius = clampedRadius * GridManager.s_instance.GridSquareSize; 
            List<SubUnit> nearbySubUnits = _subUnit._grid.GetEntsInRange(_subUnit.GetPosition(), checkRadius);
            nearbySubUnits.Sort((nearby1, nearby2) => GetCombatScore(nearby1).CompareTo(GetCombatScore(nearby2)));
            
            foreach (SubUnit potentialTarget in nearbySubUnits)
            {
                // If not an enemy, ignore
                if (!_subUnit._parentUnit.FriendlyFaction.IsHostile(potentialTarget._parentUnit.FriendlyFaction))
                {
                    continue;
                }

                _subUnit._chargeTarget = _currentCombatTarget;
                return potentialTarget;
            }

            _subUnit._chargeTarget = null;
            return null;
        }

        private void ManuoevreTowardsEnemy(SubUnit target)
        {
            ClearSubTasks(); //need to clear existing movement tasks here.

            Vector3 ourPosition = _subUnit.GetPosition();
            Vector3 theirPosition = target.GetPosition();

            Vector3 toThem = (theirPosition - ourPosition).normalized;

            Vector3 toSide = Vector3.Cross(toThem, Vector3.up).normalized;
            toSide *= GridManager.s_instance.GridSquareSize * 0.5f;

            Vector3 targetPosition = theirPosition - (toThem * GridManager.s_instance.GridSquareSize * 0.5f);

            // Don't go precisely on the edge of the grid square, give some tolerance
            float buffer = 1.15f + _subUnit._combatDistance;
            Vector3 tolerance =  toThem * buffer;

            // TODO: we don't want to align exactly on the grid edge; instead we want to align along grid "corridors"
            // But that's for future me

            LineSegment targetLine = new LineSegment
            {
                firstPos = GridManager.s_instance.AliasToGridCorner(targetPosition + toSide) - tolerance,
                lastPos =  GridManager.s_instance.AliasToGridCorner(targetPosition - toSide) - tolerance
            };

            QueueSubTask(new PathMovementTask(targetLine));
        }

        // If we're within 1 grid square distance, we can hit them
        private bool IsInCombatRange(SubUnit target)
        {
            float gridSquareSizeSqr = (GridManager.s_instance.GridSquareSize * GridManager.s_instance.GridSquareSize);
            float distanceSqr = (target.GetPosition() - _subUnit.GetPosition()).sqrMagnitude;
            return distanceSqr < gridSquareSizeSqr;
        }

        public override void OnComplete()
        {
            _subUnit.State = SubUnit.SubUnitState.Idle;
        }
    }
}