﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SubUnit // So we can access unit's private members
{
    public class RawMovementTask : Task
    {
        public LinearAlgebra.LineSegment Line { get; private set; }

        public RawMovementTask(LinearAlgebra.LineSegment line)
        {
            Line = line;
        }

        public override void Apply()
        {
            _subUnit.SetEndGoalLine(Line);
        }

        public override void UpdateImpl()
        {
            // Hmm, we really need a nicer way. Some sort of state machine
            // Tasks shouldn't set states directly, most likely
            // Right now we're using "tasks" as ad-hoc state machines, but really we should have state machines that create and pass tasks down
            if (_subUnit.State != SubUnitState.Charging && _subUnit.State != SubUnitState.Combat)
            {
                _subUnit.State = SubUnitState.Moving;
            }

            IsComplete = IsDistanceToDestinationWithinThreshold(_subUnit, _subUnit.GetMovementGoalPosition());
        }

        public static bool IsDistanceToDestinationWithinThreshold(SubUnit subUnit, Vector3 destination)
        {
            Vector3 toDestination = destination - subUnit.GetPosition();

            float currentSpeedThisTick = subUnit.CurrentMovementSpeed * Time.fixedDeltaTime;
            // Whether we'll be there soon
            currentSpeedThisTick *= 5.0f;
            return toDestination.sqrMagnitude <= currentSpeedThisTick * currentSpeedThisTick;
        }
    }
}