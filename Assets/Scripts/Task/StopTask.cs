﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SubUnit // So we can access unit's private members
{
    public class StopTask : Task
    {
        // Todo, find a better solution that handles the rotation different between current and end position, preferably without needing to explicitly check coordinator (i.e be generic)
        // Perhaps would make sense to have the stop command call a virtual function on the coordinator
        public override void Apply()
        {
            Formation formation = _subUnit._formation;
            Vector3 finalPosition = formation.Centre;
            LinearAlgebra.LineSegment currentLine = formation.Line;

            // TODO: Make it set the line a little ahead of where we are, so we don't suddenly stop and walk back. Make this depend on a Unit average velocity
            LinearAlgebra.LineSegment newLine = new LinearAlgebra.LineSegment
            {
                firstPos = _subUnit.Position + (currentLine.firstPos - finalPosition),
                lastPos = _subUnit.Position + (currentLine.lastPos - finalPosition)
            };

            _subUnit._formation.SetForLine(newLine, _subUnit.Entities.Count);
            _subUnit.MarkFormationDirty();

            IsComplete = true;
        }
    }
}